﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Anton.AppreoWebservice.ViewModels
{
    public class CreateTodo
    {
        [Required] public int TodoType { get; set; }
        [Required] public string Description { get; set; }
        [Required] public int EmployeeId { get; set; }
        public DateTime? DueDate { get; set; }
        public string InternalMemo { get; set; }
        public int? RelationId { get; set; }
    }
}
