﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Anton.AppreoWebservice.ViewModels
{
    public class DocumentTableInfo
    {
        public string TableName { get; set; }
        public string FkParent { get; set; }
    }
}
