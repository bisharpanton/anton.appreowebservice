﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Anton.AppreoWebservice.ViewModels
{
    public class RejectTodoInfo
    {
        public int RejectedBy { get; set; }
        public DateTime DateRejected { get; set; }
    }
}
