﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Anton.AppreoWebservice.ViewModels
{
    public class SetTodoDoneInfo
    {
        public int DoneBy { get; set; }
        public DateTime DateDone { get; set; }
    }
}
