﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Anton.AppreoWebservice.ViewModels
{
    public class CreateProjectTime
    {
        [Required] public DateTime Date { get; set; }
        [Required] public int Employee { get; set; }
        public int? Order { get; set; }
        public int? Joborder { get; set; }
        public int? JoborderDetailWorkactivity { get; set; }
        [Required] public int Workactivity { get; set; }
        public string Memo { get; set; }
        [Required] public double TimeEmployeeInHours { get; set; }
        public int? OverTime { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public bool Closed { get; set; }
    }
}
