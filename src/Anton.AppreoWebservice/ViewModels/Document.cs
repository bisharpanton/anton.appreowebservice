﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Anton.AppreoWebservice.ViewModels
{
    public class Document
    {
        public int DocumentId { get; set; }
        public DateTime DateChanged { get; set; }
        public string TableName { get; set; }
        public int RecordId { get; set; }
        public string FileName { get; set; }
        public string DocumentInBase64String { get; set; }
        public bool IsDeleted { get; set; }
    }
}
