﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Anton.AppreoWebservice.Models;

namespace Anton.AppreoWebservice.ViewModels
{
    public class Declaration
    {
        public int FK_EMPLOYEE { get; set; }
        public string FK_ORDER { get; set; }
        public string FK_JOBORDER { get; set; }
        public DeclarationType DECLARATIONTYPE { get; set; }
        public string DESCRIPTION { get; set; }
        public double QUANTITY { get; set; }
        public double AMOUNT { get; set; }
        public double TOTALAMOUNT { get; set; }
        public DateTime DATE { get; set; }
        public string STATE { get; set; }
        public int? PURCHASEINVOICEID { get; set; }
    }
}
