﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Anton.AppreoWebservice.Models;

namespace Anton.AppreoWebservice.ViewModels
{
    public class CreateSafetyReport
    {
        public Incidentmelding Incidentmelding { get; set; }
        public int? SafetyReportSource { get; set; }
        public int? Reporter { get; set; }
        public string Description { get; set; }
        public int? SafetyReportKind { get; set; }
        public DateTime? DateSafetyReport { get; set; }
        public string Memo { get; set; }
        public IntercompanyRelation IntercompanyRelation { get; set; }
        public int? RelationId { get; set; }
        public string Cause { get; set; }
        public string Measures { get; set; }
        public string LongTermMeasures { get; set; }

        public List<int> EmployeeIds { get; set; }
        public List<int> IntercompanyRelationIds { get; set; }
        public List<int> SubcontractorIds { get; set; }
        public List<int> ObservationRoundIds { get; set; }
        public List<int> ImprovementIds { get; set; }
    }
}
