﻿using System;
using System.Collections.Concurrent;
using System.Configuration;
using Microsoft.Extensions.Options;
using Ridder.Client.SDK.Extensions;

namespace Anton.AppreoWebservice.Framework
{
    public class CachedSdkSessionProvider : ISdkSessionProvider
    {
        private readonly ConcurrentDictionary<string, SdkSession> _sessions = new ConcurrentDictionary<string, SdkSession>();
        private readonly SdkCompaniesOptions _options;

        public CachedSdkSessionProvider(IOptions<SdkCompaniesOptions> options)
        {
            _options = options?.Value ?? throw new ArgumentNullException(nameof(options));
        }

        public SdkSession Get(string company)
        {
            var sdk = _sessions.GetOrAdd(company, CreateSdkSession);
            return sdk;
        }

        protected SdkSession CreateSdkSession(string company)
        {
            if (!_options.Companies.TryGetValue(company, out var sdkOptions))
            {
                return null; //Company '{company}' not found in settings
                //throw new ConfigurationErrorsException($"Company '{company}' not found in settings");
            }

            var config = new SdkConfiguration() {
                CompanyName = sdkOptions.CompanyName,
                UserName = sdkOptions.UserName,
                Password = sdkOptions.Password,
                Permission = sdkOptions.Permission,
                RidderIQClientPath = sdkOptions.RidderIQClientPath
            };

            return new SdkSession(config);
        }
    }
}
