﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Anton.AppreoWebservice.Framework
{
    public class PagedResponse<T>
    {
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public int? Page { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public int? PageSize { get; set; }

        public int TotalResults { get; set; }

        public IEnumerable<T> Results { get; set; }

        public PagedResponse<T> ViewModelsToPagedResponse(IEnumerable<T> models, int totalResults, PagingOptions pagingOptions = null)
        {
            return new PagedResponse<T>
            {
                Page = pagingOptions?.Page,
                PageSize = pagingOptions?.PageSize,
                TotalResults = totalResults,
                Results = models
            };
        }
    }
}
