﻿using System;
using System.Configuration;
using Microsoft.Extensions.Options;
using Ridder.Client.SDK.Extensions;

namespace Anton.AppreoWebservice.Framework
{
    public class SdkSessionProvider : ISdkSessionProvider
    {
        private readonly SdkCompaniesOptions _options;

        public SdkSessionProvider(IOptions<SdkCompaniesOptions> options)
        {
            _options = options?.Value ?? throw new ArgumentNullException(nameof(options));
        }

        public SdkSession Get(string company)
        {
            return CreateSdkSession(company);
        }

        protected SdkSession CreateSdkSession(string company)
        {
            if (!_options.Companies.TryGetValue(company, out var sdkOptions))
            {
                throw new ConfigurationErrorsException($"Company '{company}' not found in settings");
            }

            var config = new SdkConfiguration() {
                CompanyName = sdkOptions.CompanyName,
                UserName = sdkOptions.UserName,
                Password = sdkOptions.Password,
                Permission = sdkOptions.Permission,
                RidderIQClientPath = sdkOptions.RidderIQClientPath
            };

            return new SdkSession(config);
        }
    }
}