﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace Anton.AppreoWebservice.Framework
{
    public class PagingOptions
    {
        public const int DefaultPageSize = 100;
        public const int MaxPageSize = int.MaxValue;

        [FromQuery]
        [Range(0, int.MaxValue, ErrorMessage = "Invalid page.")]
        public int Page { get; set; } = 0;

        [FromQuery]
        [Range(1, MaxPageSize, ErrorMessage = "Limit must be greater than 0 and less than 1000.")]
        public int PageSize { get; set; } = DefaultPageSize;
    }
}
