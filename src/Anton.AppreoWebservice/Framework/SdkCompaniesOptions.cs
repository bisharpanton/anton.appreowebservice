﻿using System.Collections.Generic;
using Ridder.AspNetCore.ClientSdk;

namespace Anton.AppreoWebservice.Framework
{
    public class SdkCompaniesOptions
    {
        public IDictionary<string, ClientSdkOptions> Companies { get; set; } = new Dictionary<string, ClientSdkOptions>();
    }
}