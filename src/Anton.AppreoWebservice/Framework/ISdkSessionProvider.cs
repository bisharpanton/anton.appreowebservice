﻿using Ridder.Client.SDK.Extensions;

namespace Anton.AppreoWebservice.Framework
{
    public interface ISdkSessionProvider
    {
        SdkSession Get(string company);
    }
}