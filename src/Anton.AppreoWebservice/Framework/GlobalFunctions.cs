﻿using System.Linq;
using Microsoft.EntityFrameworkCore.Internal;
using Ridder.Client.SDK.Extensions;

namespace Anton.AppreoWebservice.Framework
{
    public static class GlobalFunctions
    {

        public static string DeleteRecordsOfDetailTable(SdkSession sdk, string tableName, string filter)
        {
            var rs = sdk.GetRecordsetColumns(tableName, "", filter);

            if(rs.RecordCount == 0)
            {
                return string.Empty;
            }

            rs.MoveFirst();
            rs.UpdateWhenMoveRecord = false;
            
            while(!rs.EOF)
            {
                rs.Delete();
                rs.MoveNext();
            }

            rs.MoveFirst();
            var updateResult = rs.Update2();

            if(updateResult.ToList().Any(x => x.HasError))
            {
                return $"Verwijderen {tableName} is mislukt, oorzaak: {updateResult.First(x => x.HasError).GetResult()}";
            }

            return string.Empty;
        }
    }
}
