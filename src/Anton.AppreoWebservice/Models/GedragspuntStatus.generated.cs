namespace Anton.AppreoWebservice.Models
{
    using System;
    using Ridder.Client.SDK.Extensions;
    using System.ComponentModel;
    
    
    public enum GedragspuntStatus : int
    {
        
        /// <summary>
        /// Openstaand
        /// </summary>
        [Description("Openstaand")]
        Openstaand = 1,
        
        /// <summary>
        /// Afgehandeld
        /// </summary>
        [Description("Afgehandeld")]
        Afgehandeld = 2,
    }
}
