﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Anton.AppreoWebservice.Models
{
    public class JobOrderWorkflowStates
    {
        public static readonly Guid New = new Guid("d82582b2-cf61-47d2-a0d0-d44b62329e53");
        public static readonly Guid ReleasedForProduction = new Guid("a2aa83fb-8158-4665-bcd4-7655eb7b6f67");
        public static readonly Guid Planned = new Guid("b3d747fe-1bb9-4b78-b95d-032cab4085a3");
        public static readonly Guid Done = new Guid("eceab401-932a-4cff-aa0f-e6b162eb87fb");
    }
}
