﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Ridder.Client.SDK.Extensions;

namespace Anton.AppreoWebservice.Models
{
    public partial class Joborder
    {
        public string WORKFLOWSTATEDESCRIPTION => GetWorkflowStateDescription(FK_WORKFLOWSTATE.Value, PLANNINGPENDINGOF);
        
        [Ignore] public string FOREMAN => Order.NAMEFOREMAN;
        [Ignore] public string CONTACTPERSON => Order.NAMECONTACT;
        [Ignore] public int ORDERNUMBER => Order.ORDERNUMBER;

        [ForeignKey("FK_ORDER"), Include]
        [JsonIgnore]
        public Order Order { get; set; }


        private string GetWorkflowStateDescription(Guid fkWorkflowstate, PlanningPendingOf planningPendingOf)
        {
            if (fkWorkflowstate == JobOrderWorkflowStates.Done)
            {
                return "Done";
            }
            else if (planningPendingOf == PlanningPendingOf.In_afwachting_van_factuur)
            {
                return "In afwachting van factuur";
            }
            else if (planningPendingOf == PlanningPendingOf.In_afwachting_van_inkoop)
            {
                return "In afwachting van inkoop";
            }
            else if (planningPendingOf == PlanningPendingOf.In_afwachting_van_planactie)
            {
                return "In afwachting van planactie";
            }
            else if (fkWorkflowstate == JobOrderWorkflowStates.New)
            {
                return "New";
            }
            else if (fkWorkflowstate == JobOrderWorkflowStates.ReleasedForProduction)
            {
                return "ReleasedForProduction";
            }
            else if (fkWorkflowstate == JobOrderWorkflowStates.Planned)
            {
                return "Planned";
            }

            return "";
        }
    }
}
