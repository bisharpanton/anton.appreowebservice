//  <auto-generated />
namespace Anton.AppreoWebservice.Models
{
    using System;
    using Ridder.Client.SDK.Extensions;
    
    
    ///  <summary>
    ///  	 Table: U_SAFETYREPORTIMPROVEMENTS
    ///  </summary>
    ///  <remarks>
    /// 	Available columns (for debugging, not necessarily available in this class):
    ///  	CREATOR, DATECHANGED, DATECREATED, EXTERNALKEY, FK_IMPROVEMENTS, FK_SAFETYREPORT, FK_WORKFLOWSTATE, PK_U_SAFETYREPORTIMPROVEMENTS, RECORDLINK, USERCHANGED
    ///  
    ///  	Filter used: FK_TABLEINFO = '4718026d-8d96-4849-9f9b-a1b1aa75d6fa'
    ///  	Included columns: FK_SAFETYREPORT, FK_IMPROVEMENTS
    ///  	Excluded columns: 
    ///  </remarks>
    [Table("U_SAFETYREPORTIMPROVEMENTS")]
    public partial class SafetyReportImprovement
    {
        
        private System.Nullable<int> _FK_IMPROVEMENTS;
        
        private System.Nullable<int> _FK_SAFETYREPORT;
        
        private int _id;
        
        /// <summary>
        /// 	Verbetervoorstel
        /// </summary>
        /// <remarks>
        /// 	DataType: Integer
        /// 	Size: 4
        /// 	Table: U_SAFETYREPORTIMPROVEMENTS
        /// </remarks>
        [Column("FK_IMPROVEMENTS")]
        public virtual System.Nullable<int> FK_IMPROVEMENTS
        {
            get
            {
                return this._FK_IMPROVEMENTS;
            }
            set
            {
                this._FK_IMPROVEMENTS = value;
            }
        }
        
        /// <summary>
        /// 	Veiligheidsmelding
        /// </summary>
        /// <remarks>
        /// 	DataType: Integer
        /// 	Size: 4
        /// 	Table: U_SAFETYREPORTIMPROVEMENTS
        /// </remarks>
        [Column("FK_SAFETYREPORT")]
        public virtual System.Nullable<int> FK_SAFETYREPORT
        {
            get
            {
                return this._FK_SAFETYREPORT;
            }
            set
            {
                this._FK_SAFETYREPORT = value;
            }
        }
        
        /// <summary>
        /// 	Veiligheidsmelding verbetervoorstellen id
        /// </summary>
        /// <remarks>
        /// 	DataType: Integer
        /// 	Size: 4
        /// </remarks>
        [PrimaryKey("PK_U_SAFETYREPORTIMPROVEMENTS")]
        public virtual int Id
        {
            get
            {
                return this._id;
            }
            set
            {
                this._id = value;
            }
        }
    }
}
