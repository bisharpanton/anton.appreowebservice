//  <auto-generated />
namespace Anton.AppreoWebservice.Models
{
    using System;
    using Ridder.Client.SDK.Extensions;
    
    
    ///  <summary>
    ///  	 Table: R_DOCUMENT
    ///  </summary>
    ///  <remarks>
    /// 	Available columns (for debugging, not necessarily available in this class):
    ///  	CREATOR, DATEAPPROVAL, DATECHANGED, DATECREATED, DATERECEPTION, DATEREVISION, DESCRIPTION, DOCUMENTDATA, DOCUMENTHASHCODE, DOCUMENTLOCATION
    ///  	DUMMYREFRESHDATECHANGED, EXTENSION, EXTERNALKEY, EXTERNALNUMBER, FK_AUTHOR, FK_R_DOCUMENT, FK_WORKFLOWSTATE, FORMAT, GELDIGHEIDSDUUR, MEMO
    ///  	NAME, ORIGINALLOCATION, PK_R_DOCUMENT, PLAINTEXT_MEMO, RECORDLINK, REVISION, SHAREPOINTID, STORAGESYSTEM, STORAGETYPE, USERCHANGED
    ///  	VIEW, WORKINSTRUCTION
    ///  
    ///  	Filter used: FK_TABLEINFO = 'ca9d5d63-291c-4908-b5d8-af06c705595b'
    ///  	Included columns: CREATOR, DATECREATED, DATECHANGED, DESCRIPTION, STORAGESYSTEM, DOCUMENTDATA, DOCUMENTLOCATION, EXTENSION
    ///  	Excluded columns: 
    ///  </remarks>
    [Table("R_DOCUMENT")]
    public partial class iQDocument
    {
        
        private string _CREATOR;
        
        private System.Nullable<System.DateTime> _DATECHANGED;
        
        private System.Nullable<System.DateTime> _DATECREATED;
        
        private string _DESCRIPTION;
        
        private byte[] _DOCUMENTDATA;
        
        private string _DOCUMENTLOCATION;
        
        private string _EXTENSION;
        
        private int _id;
        
        private string _STORAGESYSTEM;
        
        /// <summary>
        /// 	Created by
        /// </summary>
        /// <remarks>
        /// 	DataType: String
        /// 	Size: 50
        /// </remarks>
        [Column("CREATOR")]
        public virtual string CREATOR
        {
            get
            {
                if (string.IsNullOrEmpty(this._CREATOR))
                {
                    return "";
                }
                return this._CREATOR;
            }
            set
            {
                this._CREATOR = value;
            }
        }
        
        /// <summary>
        /// 	Date changed
        /// </summary>
        /// <remarks>
        /// 	DataType: DateTime
        /// 	Size: 8
        /// </remarks>
        [Column("DATECHANGED")]
        public virtual System.Nullable<System.DateTime> DATECHANGED
        {
            get
            {
                return this._DATECHANGED;
            }
            set
            {
                this._DATECHANGED = value;
            }
        }
        
        /// <summary>
        /// 	Date created
        /// </summary>
        /// <remarks>
        /// 	DataType: DateTime
        /// 	Size: 8
        /// </remarks>
        [Column("DATECREATED")]
        public virtual System.Nullable<System.DateTime> DATECREATED
        {
            get
            {
                return this._DATECREATED;
            }
            set
            {
                this._DATECREATED = value;
            }
        }
        
        /// <summary>
        /// 	Omschrijving
        /// </summary>
        /// <remarks>
        /// 	DataType: String
        /// 	Size: 255
        /// </remarks>
        [Column("DESCRIPTION")]
        public virtual string DESCRIPTION
        {
            get
            {
                if (string.IsNullOrEmpty(this._DESCRIPTION))
                {
                    return "";
                }
                return this._DESCRIPTION;
            }
            set
            {
                this._DESCRIPTION = value;
            }
        }
        
        /// <summary>
        /// 	Documentgegevens
        /// </summary>
        /// <remarks>
        /// 	DataType: Blob
        /// 	Size: 0
        /// </remarks>
        [Column("DOCUMENTDATA")]
        public virtual byte[] DOCUMENTDATA
        {
            get
            {
                return this._DOCUMENTDATA;
            }
            set
            {
                this._DOCUMENTDATA = value;
            }
        }
        
        /// <summary>
        /// 	Documentlocatie
        /// </summary>
        /// <remarks>
        /// 	DataType: String
        /// 	Size: 1024
        /// </remarks>
        [Column("DOCUMENTLOCATION")]
        public virtual string DOCUMENTLOCATION
        {
            get
            {
                if (string.IsNullOrEmpty(this._DOCUMENTLOCATION))
                {
                    return "";
                }
                return this._DOCUMENTLOCATION;
            }
            set
            {
                this._DOCUMENTLOCATION = value;
            }
        }
        
        /// <summary>
        /// 	Extension
        /// </summary>
        /// <remarks>
        /// 	DataType: String
        /// 	Size: 255
        /// </remarks>
        [Column("EXTENSION")]
        public virtual string EXTENSION
        {
            get
            {
                if (string.IsNullOrEmpty(this._EXTENSION))
                {
                    return "";
                }
                return this._EXTENSION;
            }
            set
            {
                this._EXTENSION = value;
            }
        }
        
        /// <summary>
        /// 	Documents id
        /// </summary>
        /// <remarks>
        /// 	DataType: Integer
        /// 	Size: 4
        /// </remarks>
        [PrimaryKey("PK_R_DOCUMENT")]
        public virtual int Id
        {
            get
            {
                return this._id;
            }
            set
            {
                this._id = value;
            }
        }
        
        /// <summary>
        /// 	Opslagsysteem
        /// </summary>
        /// <remarks>
        /// 	DataType: String
        /// 	Size: 50
        /// </remarks>
        [Column("STORAGESYSTEM")]
        public virtual string STORAGESYSTEM
        {
            get
            {
                if (string.IsNullOrEmpty(this._STORAGESYSTEM))
                {
                    return "";
                }
                return this._STORAGESYSTEM;
            }
            set
            {
                this._STORAGESYSTEM = value;
            }
        }
    }
}
