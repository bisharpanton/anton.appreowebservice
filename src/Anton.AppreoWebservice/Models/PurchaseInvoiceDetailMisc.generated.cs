//  <auto-generated />
namespace Anton.AppreoWebservice.Models
{
    using System;
    using Ridder.Client.SDK.Extensions;
    
    
    ///  <summary>
    ///  	 Table: R_PURCHASEINVOICEDETAILMISC
    ///  </summary>
    ///  <remarks>
    /// 	Available columns (for debugging, not necessarily available in this class):
    ///  	CREATOR, DATECHANGED, DATECREATED, DESCRIPTION, DIRECTTOORDER, DISCOUNTAMOUNT, DISCOUNTPERCENTAGE, EXTERNALKEY, FK_COSTCENTER, FK_COSTUNIT
    ///  	FK_GENERALLEDGERACCOUNT, FK_GOODSRECEIPTDETAILMISC, FK_JOBORDER, FK_MISC, FK_ORDER, FK_PURCHASEINVOICE, FK_PURCHASEORDERDETAILMISC, FK_PURCHASEUNIT, FK_VATCODE, FK_WORKFLOWSTATE
    ///  	GROSSPURCHASEPRICE, MEMO, NETPURCHASEPRICE, PK_R_PURCHASEINVOICEDETAILMISC, PLAINTEXT_MEMO, QUANTITY, RECORDLINK, SUPPLIERCODE, TOTALAMOUNT, USERCHANGED
    ///  	VATAMOUNT
    ///  
    ///  	Filter used: FK_TABLEINFO = 'cdf23944-4652-4bbd-b3dc-cc20315ae469'
    ///  	Included columns: CREATOR, DATECREATED, DATECHANGED, FK_PURCHASEINVOICE, DESCRIPTION, QUANTITY, FK_ORDER, FK_JOBORDER, GROSSPURCHASEPRICE, NETPURCHASEPRICE, TOTALAMOUNT
    ///  	Excluded columns: 
    ///  </remarks>
    [Table("R_PURCHASEINVOICEDETAILMISC")]
    public partial class PurchaseInvoiceDetailMisc
    {
        
        private string _CREATOR;
        
        private System.Nullable<System.DateTime> _DATECHANGED;
        
        private System.Nullable<System.DateTime> _DATECREATED;
        
        private string _DESCRIPTION;
        
        private System.Nullable<int> _FK_JOBORDER;
        
        private System.Nullable<int> _FK_ORDER;
        
        private int _FK_PURCHASEINVOICE;
        
        private System.Nullable<double> _GROSSPURCHASEPRICE;
        
        private System.Nullable<double> _NETPURCHASEPRICE;
        
        private int _id;
        
        private double _QUANTITY;
        
        private System.Nullable<double> _TOTALAMOUNT;
        
        /// <summary>
        /// 	Created by
        /// </summary>
        /// <remarks>
        /// 	DataType: String
        /// 	Size: 50
        /// </remarks>
        [Column("CREATOR")]
        public virtual string CREATOR
        {
            get
            {
                if (string.IsNullOrEmpty(this._CREATOR))
                {
                    return "";
                }
                return this._CREATOR;
            }
            set
            {
                this._CREATOR = value;
            }
        }
        
        /// <summary>
        /// 	Date changed
        /// </summary>
        /// <remarks>
        /// 	DataType: DateTime
        /// 	Size: 8
        /// </remarks>
        [Column("DATECHANGED")]
        public virtual System.Nullable<System.DateTime> DATECHANGED
        {
            get
            {
                return this._DATECHANGED;
            }
            set
            {
                this._DATECHANGED = value;
            }
        }
        
        /// <summary>
        /// 	Date created
        /// </summary>
        /// <remarks>
        /// 	DataType: DateTime
        /// 	Size: 8
        /// </remarks>
        [Column("DATECREATED")]
        public virtual System.Nullable<System.DateTime> DATECREATED
        {
            get
            {
                return this._DATECREATED;
            }
            set
            {
                this._DATECREATED = value;
            }
        }
        
        /// <summary>
        /// 	Description
        /// </summary>
        /// <remarks>
        /// 	DataType: String
        /// 	Size: 80
        /// </remarks>
        [Column("DESCRIPTION")]
        public virtual string DESCRIPTION
        {
            get
            {
                if (string.IsNullOrEmpty(this._DESCRIPTION))
                {
                    return "";
                }
                return this._DESCRIPTION;
            }
            set
            {
                this._DESCRIPTION = value;
            }
        }
        
        /// <summary>
        /// 	Job order
        /// </summary>
        /// <remarks>
        /// 	DataType: Integer
        /// 	Size: 4
        /// 	Table: R_PURCHASEINVOICEDETAILMISC
        /// </remarks>
        [Column("FK_JOBORDER")]
        public virtual System.Nullable<int> FK_JOBORDER
        {
            get
            {
                return this._FK_JOBORDER;
            }
            set
            {
                this._FK_JOBORDER = value;
            }
        }
        
        /// <summary>
        /// 	Order
        /// </summary>
        /// <remarks>
        /// 	DataType: Integer
        /// 	Size: 4
        /// 	Table: R_PURCHASEINVOICEDETAILMISC
        /// </remarks>
        [Column("FK_ORDER")]
        public virtual System.Nullable<int> FK_ORDER
        {
            get
            {
                return this._FK_ORDER;
            }
            set
            {
                this._FK_ORDER = value;
            }
        }
        
        /// <summary>
        /// 	Purchase invoice
        /// </summary>
        /// <remarks>
        /// 	DataType: Integer
        /// 	Size: 4
        /// 	Table: R_PURCHASEINVOICEDETAILMISC
        /// </remarks>
        [Column("FK_PURCHASEINVOICE")]
        public virtual int FK_PURCHASEINVOICE
        {
            get
            {
                return this._FK_PURCHASEINVOICE;
            }
            set
            {
                this._FK_PURCHASEINVOICE = value;
            }
        }
        
        /// <summary>
        /// 	Gross purchase price
        /// </summary>
        /// <remarks>
        /// 	DataType: Money
        /// 	Size: 8
        /// </remarks>
        [Column("GROSSPURCHASEPRICE")]
        public virtual System.Nullable<double> GROSSPURCHASEPRICE
        {
            get
            {
                return this._GROSSPURCHASEPRICE;
            }
            set
            {
                this._GROSSPURCHASEPRICE = value;
            }
        }
        
        /// <summary>
        /// 	Net purchase price
        /// </summary>
        /// <remarks>
        /// 	DataType: Money
        /// 	Size: 8
        /// </remarks>
        [Column("NETPURCHASEPRICE")]
        public virtual System.Nullable<double> NETPURCHASEPRICE
        {
            get
            {
                return this._NETPURCHASEPRICE;
            }
            set
            {
                this._NETPURCHASEPRICE = value;
            }
        }
        
        /// <summary>
        /// 	Purchase invoice detail miscellaneous id
        /// </summary>
        /// <remarks>
        /// 	DataType: Integer
        /// 	Size: 4
        /// </remarks>
        [PrimaryKey("PK_R_PURCHASEINVOICEDETAILMISC")]
        public virtual int Id
        {
            get
            {
                return this._id;
            }
            set
            {
                this._id = value;
            }
        }
        
        /// <summary>
        /// 	Quantity
        /// </summary>
        /// <remarks>
        /// 	DataType: Float
        /// 	Size: 8
        /// </remarks>
        [Column("QUANTITY")]
        public virtual double QUANTITY
        {
            get
            {
                return this._QUANTITY;
            }
            set
            {
                this._QUANTITY = value;
            }
        }
        
        /// <summary>
        /// 	Total amount
        /// </summary>
        /// <remarks>
        /// 	DataType: Money
        /// 	Size: 8
        /// </remarks>
        [Column("TOTALAMOUNT")]
        public virtual System.Nullable<double> TOTALAMOUNT
        {
            get
            {
                return this._TOTALAMOUNT;
            }
            set
            {
                this._TOTALAMOUNT = value;
            }
        }
    }
}
