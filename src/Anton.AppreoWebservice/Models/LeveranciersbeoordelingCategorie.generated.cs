namespace Anton.AppreoWebservice.Models
{
    using System;
    using Ridder.Client.SDK.Extensions;
    using System.ComponentModel;
    
    
    public enum LeveranciersbeoordelingCategorie : int
    {
        
        /// <summary>
        /// Kwaliteit
        /// </summary>
        [Description("Kwaliteit")]
        Kwaliteit = 1,
        
        /// <summary>
        /// Veiligheid
        /// </summary>
        [Description("Veiligheid")]
        Veiligheid = 2,
        
        /// <summary>
        /// Milieu
        /// </summary>
        [Description("Milieu")]
        Milieu = 3,
        
        /// <summary>
        /// Financieel
        /// </summary>
        [Description("Financieel")]
        Financieel = 4,
        
        /// <summary>
        /// Tot slot
        /// </summary>
        [Description("Tot slot")]
        Tot_slot = 5,
        
        /// <summary>
        /// Algemeen
        /// </summary>
        [Description("Algemeen")]
        Algemeen = 6,
    }
}
