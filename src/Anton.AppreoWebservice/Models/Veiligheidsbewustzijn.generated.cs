namespace Anton.AppreoWebservice.Models
{
    using System;
    using Ridder.Client.SDK.Extensions;
    using System.ComponentModel;
    
    
    public enum Veiligheidsbewustzijn : int
    {
        
        /// <summary>
        ///  
        /// </summary>
        [Description(" ")]
        N_ = 1,
        
        /// <summary>
        /// Onbewust onveilig
        /// </summary>
        [Description("Onbewust onveilig")]
        Onbewust_onveilig = 2,
        
        /// <summary>
        /// Bewust onveilig
        /// </summary>
        [Description("Bewust onveilig")]
        Bewust_onveilig = 3,
        
        /// <summary>
        /// Bewust veilig
        /// </summary>
        [Description("Bewust veilig")]
        Bewust_veilig = 4,
        
        /// <summary>
        /// Onbewust veilig
        /// </summary>
        [Description("Onbewust veilig")]
        Onbewust_veilig = 5,
    }
}
