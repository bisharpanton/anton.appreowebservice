//  <auto-generated />
namespace Anton.AppreoWebservice.Models
{
    using System;
    using Ridder.Client.SDK.Extensions;
    
    
    ///  <summary>
    ///  	 Table: R_PURCHASEINVOICE
    ///  </summary>
    ///  <remarks>
    /// 	Available columns (for debugging, not necessarily available in this class):
    ///  	ADDTONEXTPAYMENTBATCH, AKKOORDVANUITIC, AMOUNTGACCOUNT, AMOUNTPAYMENTDISCOUNT, ANTONBOOKDATE, BEDRAGBETAALD, BLOCKPAYMENT, BOOKDATE, BOOKINCURRENCY, CREATOR
    ///  	DATEACCORD, DATEBLOCKED, DATECHANGED, DATECREATED, DATEPAID, DATUMBETALING, DAYSOUTSTANDING, DAYSOVERDUE, DECLARATIONTYPE, EXCHANGERATE
    ///  	EXCHANGERATEDATE, EXPECTEDPAYDATE, EXTERNALINVOICENUMBER, EXTERNALKEY, EXTERNFACTUURNUMMERALAANWEZIG, FK_BLOCKEDBY, FK_CREATEDBY, FK_CURRENCY, FK_DAYBOOK, FK_INCOTERM
    ///  	FK_INVOICEAUDITOR, FK_JOURNALENTRY, FK_ORIGINALPURCHASEORDER, FK_PAYMENTBATCH, FK_PAYMENTMETHOD, FK_PAYMENTTERM, FK_R_PURCHASEINVOICE, FK_SUPPLIER, FK_VATCOMPANYGROUP, FK_VATPAYMENTDISCOUNT
    ///  	FK_WKACHANGEDBY, FK_WORKFLOWSTATE, IGNOREBLOCKADELEDGER, INCOTERMSPLACE, INTEGRATIONSTATE, INVOICEAMOUNTEXVAT, INVOICEDATE, INVOICEVATAMOUNT, ISJOURNALIZED, ISPAID
    ///  	MEMO, OUTSTANDINGAMOUNT, PAYABLE, PAYABLEDATE, PAYABLEDATEPAYMENTDISCOUNT, PAYMENTREFERENCE, PK_R_PURCHASEINVOICE, PLAINTEXT_MEMO, PURCHASEINVOICERECEIPTSTATE, RECORDLINK
    ///  	U_BETAALD, UNATTENDEDPROCESSEDBYIC, USERCHANGED, VATAMOUNTPAYMENTDISCOUNT, WKA, WKADATECHANGED, WKASTATE
    ///  
    ///  	Filter used: FK_TABLEINFO = '043cbf55-edf8-4d56-bcd6-a5f384c72a5c'
    ///  	Included columns: CREATOR, DATECREATED, DATECHANGED, FK_SUPPLIER, EXTERNALINVOICENUMBER, INVOICEDATE, DECLARATIONTYPE, FK_WORKFLOWSTATE
    ///  	Excluded columns: 
    ///  </remarks>
    [Table("R_PURCHASEINVOICE")]
    public partial class PurchaseInvoice
    {
        
        private string _CREATOR;
        
        private System.Nullable<System.DateTime> _DATECHANGED;
        
        private System.Nullable<System.DateTime> _DATECREATED;
        
        private DeclarationType _DECLARATIONTYPE;
        
        private string _EXTERNALINVOICENUMBER;
        
        private int _FK_SUPPLIER;
        
        private System.Nullable<System.Guid> _FK_WORKFLOWSTATE;
        
        private System.DateTime _INVOICEDATE;
        
        private int _id;
        
        /// <summary>
        /// 	Created by
        /// </summary>
        /// <remarks>
        /// 	DataType: String
        /// 	Size: 50
        /// </remarks>
        [Column("CREATOR")]
        public virtual string CREATOR
        {
            get
            {
                if (string.IsNullOrEmpty(this._CREATOR))
                {
                    return "";
                }
                return this._CREATOR;
            }
            set
            {
                this._CREATOR = value;
            }
        }
        
        /// <summary>
        /// 	Date changed
        /// </summary>
        /// <remarks>
        /// 	DataType: DateTime
        /// 	Size: 8
        /// </remarks>
        [Column("DATECHANGED")]
        public virtual System.Nullable<System.DateTime> DATECHANGED
        {
            get
            {
                return this._DATECHANGED;
            }
            set
            {
                this._DATECHANGED = value;
            }
        }
        
        /// <summary>
        /// 	Date created
        /// </summary>
        /// <remarks>
        /// 	DataType: DateTime
        /// 	Size: 8
        /// </remarks>
        [Column("DATECREATED")]
        public virtual System.Nullable<System.DateTime> DATECREATED
        {
            get
            {
                return this._DATECREATED;
            }
            set
            {
                this._DATECREATED = value;
            }
        }
        
        /// <summary>
        /// 	Declaratie type
        /// </summary>
        /// <remarks>
        /// 	DataType: Integer
        /// 	Size: 4
        /// </remarks>
        [Column("DECLARATIONTYPE")]
        public virtual DeclarationType DECLARATIONTYPE
        {
            get
            {
                return this._DECLARATIONTYPE;
            }
            set
            {
                this._DECLARATIONTYPE = value;
            }
        }
        
        /// <summary>
        /// 	External invoice number
        /// </summary>
        /// <remarks>
        /// 	DataType: String
        /// 	Size: 50
        /// </remarks>
        [Column("EXTERNALINVOICENUMBER")]
        public virtual string EXTERNALINVOICENUMBER
        {
            get
            {
                if (string.IsNullOrEmpty(this._EXTERNALINVOICENUMBER))
                {
                    return "";
                }
                return this._EXTERNALINVOICENUMBER;
            }
            set
            {
                this._EXTERNALINVOICENUMBER = value;
            }
        }
        
        /// <summary>
        /// 	Supplier
        /// </summary>
        /// <remarks>
        /// 	DataType: Integer
        /// 	Size: 4
        /// 	Table: R_PURCHASEINVOICE
        /// </remarks>
        [Column("FK_SUPPLIER")]
        public virtual int FK_SUPPLIER
        {
            get
            {
                return this._FK_SUPPLIER;
            }
            set
            {
                this._FK_SUPPLIER = value;
            }
        }
        
        /// <summary>
        /// 	Workflow state
        /// </summary>
        /// <remarks>
        /// 	DataType: Guid
        /// 	Size: 4
        /// 	Table: R_PURCHASEINVOICE
        /// </remarks>
        [Column("FK_WORKFLOWSTATE")]
        public virtual System.Nullable<System.Guid> FK_WORKFLOWSTATE
        {
            get
            {
                return this._FK_WORKFLOWSTATE;
            }
            set
            {
                this._FK_WORKFLOWSTATE = value;
            }
        }
        
        /// <summary>
        /// 	Invoice date
        /// </summary>
        /// <remarks>
        /// 	DataType: DateTime
        /// 	Size: 8
        /// </remarks>
        [Column("INVOICEDATE")]
        public virtual System.DateTime INVOICEDATE
        {
            get
            {
                return this._INVOICEDATE;
            }
            set
            {
                this._INVOICEDATE = value;
            }
        }
        
        /// <summary>
        /// 	Purchace invoice id
        /// </summary>
        /// <remarks>
        /// 	DataType: Integer
        /// 	Size: 4
        /// </remarks>
        [PrimaryKey("PK_R_PURCHASEINVOICE")]
        public virtual int Id
        {
            get
            {
                return this._id;
            }
            set
            {
                this._id = value;
            }
        }
    }
}
