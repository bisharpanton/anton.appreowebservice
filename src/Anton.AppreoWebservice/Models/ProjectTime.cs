﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Anton.AppreoWebservice.Models
{
    public partial class ProjectTime
    {
        public double TIMEEMPLOYEEINHOURS => TimeSpan.FromTicks(TIMEEMPLOYEE).TotalHours;
    }
}
