﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Ridder.Client.SDK.Extensions;

namespace Anton.AppreoWebservice.Models
{
    public partial class JoborderDetailWorkactivity
    {
        [ForeignKey("FK_JOBORDER"), Include]
        public Joborder Joborder { get; set; }

        public double TOTALOPERATIONTIMEINHOURS => TimeSpan.FromTicks(TOTALOPERATIONTIME.Value).TotalHours;

        public string WORKFLOWSTATEDESCRIPTION => GetWorkflowStateDescription(FK_WORKFLOWSTATE.Value, Joborder.PLANNINGPENDINGOF);

        private string GetWorkflowStateDescription(Guid fkWorkflowstate, PlanningPendingOf planningPendingOf)
        {
            if (fkWorkflowstate == JoborderDetailWorkactivityWorkflowStates.Done)
            {
                return "Done";
            }
            else if (planningPendingOf == PlanningPendingOf.In_afwachting_van_factuur)
            {
                return "In afwachting van factuur";
            }
            else if (planningPendingOf == PlanningPendingOf.In_afwachting_van_inkoop)
            {
                return "In afwachting van inkoop";
            }
            else if (planningPendingOf == PlanningPendingOf.In_afwachting_van_planactie)
            {
                return "In afwachting van planactie";
            }
            else if (fkWorkflowstate == JoborderDetailWorkactivityWorkflowStates.New)
            {
                return "New";
            }
            else if (fkWorkflowstate == JoborderDetailWorkactivityWorkflowStates.ReleasedForProduction)
            {
                return "ReleasedForProduction";
            }

            return "";
        }

    }
}
