﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Anton.AppreoWebservice.Models
{
    public enum WorkActivityType
    {
        Start_working_time = 1,
        End_working_time = 2,
        Continue_last_job = 3,
        Part_time_absence = 4,
        Break = 5,
        Illness = 6,
        Time_off_in_lieu = 7,
        Discontinuity = 8,
        Bank_holiday = 9,
        Holiday = 10,
        Travelling_hours = 11,
        Paid_absence = 12,
        Illegal_absence = 13,
        Unpayed_absence = 14,
        Indirect_hours = 15,
        Direct_hours = 16
    }
}
