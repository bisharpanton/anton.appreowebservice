﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Anton.AppreoWebservice.Models
{
    public class OrderWorkflowstates
    {
        public static readonly Guid New = new Guid("6205d9d2-72d7-49df-8b4b-6e5c1c3d4927");
        public static readonly Guid InProgress = new Guid("e83694f3-b6a8-4c2f-8c65-20107edf22bc");
        public static readonly Guid DoneByProjectmanager = new Guid("eb0e5af0-52b1-4d0e-b203-4a4fb2f089bf");
        public static readonly Guid FinancialDone = new Guid("b2cb1324-a085-4823-8cc2-e0c372edfd87");
    }
}
