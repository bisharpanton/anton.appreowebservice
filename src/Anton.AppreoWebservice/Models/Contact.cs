﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Ridder.Client.SDK.Extensions;

namespace Anton.AppreoWebservice.Models
{
    public partial class Contact
    {
        [Ignore] public string NAME => (Person.NAMEPREFIX == "")
            ? $"{Person.FIRSTNAME} {Person.LASTNAME}"
            : $"{Person.FIRSTNAME} {Person.NAMEPREFIX} {Person.LASTNAME}";

        [ForeignKey("FK_PERSON"), Include]
        [JsonIgnore]
        public Person Person { get; set; }
    }
}
