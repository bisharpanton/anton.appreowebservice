namespace Anton.AppreoWebservice.Models
{
    using System;
    using Ridder.Client.SDK.Extensions;
    using System.ComponentModel;
    
    
    public enum Incidentmelding : int
    {
        
        /// <summary>
        /// -
        /// </summary>
        [Description("-")]
        N_ = 1,
        
        /// <summary>
        /// Ongeval met verzuim
        /// </summary>
        [Description("Ongeval met verzuim")]
        Ongeval_met_verzuim = 2,
        
        /// <summary>
        /// Ongeval met aangepast werk
        /// </summary>
        [Description("Ongeval met aangepast werk")]
        Ongeval_met_aangepast_werk = 3,
        
        /// <summary>
        /// Ongeval zonder verzuim (vanaf EHBO)
        /// </summary>
        [Description("Ongeval zonder verzuim (vanaf EHBO)")]
        Ongeval_zonder_verzuim__vanaf_EHBO_ = 4,
        
        /// <summary>
        /// Bijna-ongeval
        /// </summary>
        [Description("Bijna-ongeval")]
        Bijna_ongeval = 5,
        
        /// <summary>
        /// Onveilige situatie en handeling
        /// </summary>
        [Description("Onveilige situatie en handeling")]
        Onveilige_situatie_en_handeling = 6,
        
        /// <summary>
        /// Overige VGM-incidenten
        /// </summary>
        [Description("Overige VGM-incidenten")]
        Overige_VGM_incidenten = 7,
        
        /// <summary>
        /// Positieve melding
        /// </summary>
        [Description("Positieve melding")]
        Positieve_melding = 8,
    }
}
