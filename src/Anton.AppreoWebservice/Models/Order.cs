﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Ridder.Client.SDK.Extensions;

namespace Anton.AppreoWebservice.Models
{
    public partial class Order
    {
        [Ignore] public int? DESTINATIONADDRESS_ADDRESID => RelationDeliveryAddress?.FK_ADDRESS;

        [ForeignKey("FK_DESTINATIONADDRESS"), Include]
        [JsonIgnore]
        public RelationDeliveryAddress RelationDeliveryAddress { get; set; }

        [Ignore] public string NAMEFOREMAN => fkUitvoerder?.NAME;
        [Ignore] public string NAMECONTACT => fkContact?.NAME;

        [ForeignKey("FK_UITVOERDER"), Include]
        [JsonIgnore]
        public Contact fkUitvoerder { get; set; }

        [ForeignKey("FK_CONTACT"), Include]
        [JsonIgnore]
        public Contact fkContact { get; set; }


        public string WORKFLOWSTATEDESCRIPTION => GetWorkflowStateDescription(FK_WORKFLOWSTATE.Value);

        private string GetWorkflowStateDescription(Guid fkWorkflowstate)
        {
            if (fkWorkflowstate == OrderWorkflowstates.New)
            {
                return "Nieuw";
            }
            else if (fkWorkflowstate == OrderWorkflowstates.InProgress)
            {
                return "Onderhanden";
            }
            else if (fkWorkflowstate == OrderWorkflowstates.DoneByProjectmanager)
            {
                return "Afgesloten";
            }
            else if (fkWorkflowstate == OrderWorkflowstates.FinancialDone)
            {
                return "Financieel gereed";
            }

            return "";
        }
    }
}
