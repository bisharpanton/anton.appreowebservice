﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Anton.AppreoWebservice.Models
{
    public partial class Todo
    {
        public string WORKFLOWSTATEDESCRIPTION => GetWorkflowStateDescription(FK_WORKFLOWSTATE.Value);

        private string GetWorkflowStateDescription(Guid fkWorkflowstate)
        {
            var wfStateOpen = new Guid("82100d7d-d342-4fe4-922c-be7002fe8844");
            var wfStateDone = new Guid("43ea659f-14bb-473a-869b-819c63ef43ac");
            var wfStateRejected = new Guid("a27c3dfc-1b4e-4d88-bf5d-3a684c50c9e8");
            var wfstateAssigned = new Guid("077168fc-54b0-4189-a892-7b83882a0dfe");
            var wfStateWaitOnReaction = new Guid("027fc371-6ec6-4e0a-bab9-8a27cd3e009d");
            

            if (fkWorkflowstate == wfStateOpen)
            {
                return "Open";
            }
            else if (fkWorkflowstate == wfStateDone)
            {
                return "Done";
            }
            else if (fkWorkflowstate == wfStateRejected)
            {
                return "Rejected";
            }
            else if (fkWorkflowstate == wfstateAssigned)
            {
                return "Toegewezen";
            }
            else if (fkWorkflowstate == wfStateWaitOnReaction)
            {
                return "Wacht op reactie";
            }

            return "";
        }
    }
}
