﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Anton.AppreoWebservice.Models
{
    public partial class WorkplaceInspection
    {
        private static readonly Guid _wfStateClosed = new Guid("b600eb9b-3ea9-45c7-aee5-18fbb1fdc49a");
        public bool ISDONE => FK_WORKFLOWSTATE == _wfStateClosed;

        public List<int> EmployeeIds { get; set; }
        public List<int> IntercompanyRelationIds { get; set; }
        public List<int> SubcontractorIds { get; set; }
        public List<int> ObservationRoundIds { get; set; }
        public List<int> ImprovementIds { get; set; }
    }
}
