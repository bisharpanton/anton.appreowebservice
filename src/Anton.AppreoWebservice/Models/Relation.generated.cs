//  <auto-generated />
namespace Anton.AppreoWebservice.Models
{
    using System;
    using Ridder.Client.SDK.Extensions;
    
    
    ///  <summary>
    ///  	 Table: R_RELATION
    ///  </summary>
    ///  <remarks>
    /// 	Available columns (for debugging, not necessarily available in this class):
    ///  	APPROVALREQUIRED, BANKACCOUNTNUMBER, BANKCITY, BANKNAME, BIC, BICGACCOUNT, BISHARPACCOUNTVIEWLASTEXPORTDATE, BISHARPAPIKEY, BISHARPCOMPANYCOLOR, BISHARPCOMPANYNUMBER
    ///  	BISHARPDIGITALDATASIGNAGELICENSEKEY, BISHARPDIGITALDATASIGNAGETOKEN, BISHARPENKAPIURL, BISHARPENKLASTIMPORTDATE, BISHARPENKPASSWORD, BISHARPENKRQSVERSION, BISHARPENKUSERNAME, BISHARPLOCATIONACCOUNTVIEW, BISHARPMULTIVERSCONNECTIONSTRING, BISHARPMULTIVERSLASTEXPORTDATE
    ///  	BISHARPRIDDERADMINISTRATIONNAME, BISHARPRIDDERDATABASENAME, BISHARPRIDDERLASTEXPORTDATE, BISHARPRSSFEEDID, BLOCKED, CCPURCHASEORDER, CCSALESINVOICE, CHECKDATEVATNUMBER, COCCITY, COCDATE
    ///  	COCNUMBER, CODE, CREATOR, CREDITSAFEID, CUSTOMERSATISFACTION, DATECHANGED, DATECREATED, DATECUSTOMER, DATEPROSPECT, DEFAULTSALESMARKUP
    ///  	EANCODE, EMAIL, EMAILPAYMENTREMINDER, EXACTONLINEACCESSTOKEN, EXACTONLINEFILTERDIVISIONS, EXACTONLINEREFRESHTOKEN, EXACTONLINEREFRESHTOKENDATE, EXTERNALKEY, FACTUURMAIL, FAX
    ///  	FK_AREA, FK_BEDRIJFSTAK1, FK_BEDRIJFSTAK2, FK_BEDRIJFSTAK3, FK_BISHARPDIGITALDATASIGNAGEUSER, FK_COSTINGPARAMETERS, FK_COSTUNIT, FK_CURRENCY, FK_DEALER, FK_DEFAULTORDER
    ///  	FK_DEFAULTPURCHASELEDGERACCOUNT, FK_INDUSTRY, FK_INDUSTRYCUSTOM, FK_LANGUAGE, FK_LEGALFORM, FK_PARENTCOMPANY, FK_POSTALADDRESS, FK_PRIVATEPERSON, FK_PURCHASEPAYMENTMETHOD, FK_PURCHASEPAYMENTTERM
    ///  	FK_PURCHASESHIPPINGTYPE, FK_R_RELATION, FK_RELATIONFINANCIALSTATE, FK_RELATIONTYPE, FK_REVENUECODE, FK_SALESCREDITLIMITCURRENCY, FK_SALESINVOICERELATION, FK_SALESPAYMENTMETHOD, FK_SALESPAYMENTTERM, FK_SALESPERSON
    ///  	FK_SALESPRICERELATION, FK_SALESSHIPPINGTYPE, FK_SBIMAINACTIVITY, FK_SHIPPINGAGENT, FK_SOURCE, FK_VATCOMPANYGROUP, FK_VISITINGADDRESS, FK_WKATYPE, FK_WORKFLOWSTATE, GIRO
    ///  	GOOGLEACCESSTOKEN, GOOGLEACCESSTOKENDATE, GOOGLEADSACCESSTOKEN, GOOGLEADSACCESSTOKENDATE, GOOGLEADSLASTIMPORTDATE, GOOGLEADSMANAGERCODE, GOOGLEADSREFRESHTOKEN, GOOGLEADSREFRESHTOKENDATE, GOOGLEANALYTICSLASTIMPORTDATE, GOOGLEREFRESHTOKEN
    ///  	GOOGLEREFRESHTOKENDATE, IBAN, IBANGACCOUNT, INSTAGRAMADMINACCESSTOKEN, INTEGRATIONID, INTERCOMPANYRELATION, INTERNALMEMO, IQ_EORINUMBER, LINKEDINACCESSTOKEN, LINKEDINACCESSTOKENDATE
    ///  	LINKEDINREFRESHTOKEN, LINKEDINREFRESHTOKENDATE, MAILING, MAINCONTACT, MAINCONTACTCREDITOR, MAINCONTACTDEBTOR, NAME, NATIONALBANKCODE, NOTAUTOMATICRECEIVEPURCHASEORDER, NUMBEROFEMPLOYEES
    ///  	OPTISIGNSTEAMSAMLTAG, PERCENTAGEGACCOUNT, PHONE1, PHONE2, PK_R_RELATION, PLAINTEXT_EXACTONLINEACCESSTOKEN, PLAINTEXT_EXACTONLINEREFRESHTOKEN, PLAINTEXT_INTERNALMEMO, PLAINTEXT_SNELSTARTONLINECLIENTKEY, PLAINTEXT_WARNING
    ///  	PRIVATEPERSON, PURCHASECREDITLIMIT, PURCHASEDISCOUNT, PURCHASEORDERMAIL, PURCHASESHIPPINGPLACE, PURCHASESMALLORDERHANDLING, PURCHASESMALLORDERLIMIT, PURCHASESMALLORDERSURCHARGEAMOUNT, RECORDLINK, RELOADDATAAVAILABLE
    ///  	SALESCREDITLIMIT, SALESCREDITLIMITSCORE, SALESDISCOUNT, SALESORDERSURCHARGEAMOUNT, SALESSHIPPINGPLACE, SALESSMALLORDERHANDLING, SALESSMALLORDERLIMIT, SALESSTAGE, SEARCHCODE, SHOWWARNING
    ///  	SNELSTARTONLINECLIENTKEY, SNELSTARTONLINELASTEXPORTDATE, SPEEDDIAL, SUPPLIERTYPE, U_VERKOOPSTADIUM, USERCHANGED, VATNUMBER, VCA, VENDOR, WARNING
    ///  	WEBSITE, WKA
    ///  
    ///  	Filter used: FK_TABLEINFO = 'b328fa3c-25e1-4b0a-9c0e-32631b19a160'
    ///  	Included columns: WARNING, CREATOR, FK_VISITINGADDRESS, CODE, DATECREATED, DATECHANGED, EMAIL, MAINCONTACT, NAME, FK_POSTALADDRESS
    ///  	Excluded columns: 
    ///  </remarks>
    [Table("R_RELATION")]
    public partial class Relation
    {
        
        private string _CODE;
        
        private string _CREATOR;
        
        private System.Nullable<System.DateTime> _DATECHANGED;
        
        private System.Nullable<System.DateTime> _DATECREATED;
        
        private string _EMAIL;
        
        private System.Nullable<int> _FK_POSTALADDRESS;
        
        private System.Nullable<int> _FK_VISITINGADDRESS;
        
        private System.Nullable<int> _MAINCONTACT;
        
        private string _NAME;
        
        private int _id;
        
        private string _WARNING;
        
        /// <summary>
        /// 	Code
        /// </summary>
        /// <remarks>
        /// 	DataType: String
        /// 	Size: 10
        /// </remarks>
        [Column("CODE")]
        public virtual string CODE
        {
            get
            {
                if (string.IsNullOrEmpty(this._CODE))
                {
                    return "";
                }
                return this._CODE;
            }
            set
            {
                this._CODE = value;
            }
        }
        
        /// <summary>
        /// 	Created by
        /// </summary>
        /// <remarks>
        /// 	DataType: String
        /// 	Size: 50
        /// </remarks>
        [Column("CREATOR")]
        public virtual string CREATOR
        {
            get
            {
                if (string.IsNullOrEmpty(this._CREATOR))
                {
                    return "";
                }
                return this._CREATOR;
            }
            set
            {
                this._CREATOR = value;
            }
        }
        
        /// <summary>
        /// 	Date changed
        /// </summary>
        /// <remarks>
        /// 	DataType: DateTime
        /// 	Size: 8
        /// </remarks>
        [Column("DATECHANGED")]
        public virtual System.Nullable<System.DateTime> DATECHANGED
        {
            get
            {
                return this._DATECHANGED;
            }
            set
            {
                this._DATECHANGED = value;
            }
        }
        
        /// <summary>
        /// 	Date created
        /// </summary>
        /// <remarks>
        /// 	DataType: DateTime
        /// 	Size: 8
        /// </remarks>
        [Column("DATECREATED")]
        public virtual System.Nullable<System.DateTime> DATECREATED
        {
            get
            {
                return this._DATECREATED;
            }
            set
            {
                this._DATECREATED = value;
            }
        }
        
        /// <summary>
        /// 	Email
        /// </summary>
        /// <remarks>
        /// 	DataType: String
        /// 	Size: 255
        /// </remarks>
        [Column("EMAIL")]
        public virtual string EMAIL
        {
            get
            {
                if (string.IsNullOrEmpty(this._EMAIL))
                {
                    return "";
                }
                return this._EMAIL;
            }
            set
            {
                this._EMAIL = value;
            }
        }
        
        /// <summary>
        /// 	Postal address
        /// </summary>
        /// <remarks>
        /// 	DataType: Integer
        /// 	Size: 4
        /// 	Table: R_RELATION
        /// </remarks>
        [Column("FK_POSTALADDRESS")]
        public virtual System.Nullable<int> FK_POSTALADDRESS
        {
            get
            {
                return this._FK_POSTALADDRESS;
            }
            set
            {
                this._FK_POSTALADDRESS = value;
            }
        }
        
        /// <summary>
        /// 	Visiting address
        /// </summary>
        /// <remarks>
        /// 	DataType: Integer
        /// 	Size: 4
        /// 	Table: R_RELATION
        /// </remarks>
        [Column("FK_VISITINGADDRESS")]
        public virtual System.Nullable<int> FK_VISITINGADDRESS
        {
            get
            {
                return this._FK_VISITINGADDRESS;
            }
            set
            {
                this._FK_VISITINGADDRESS = value;
            }
        }
        
        /// <summary>
        /// 	Main contact
        /// </summary>
        /// <remarks>
        /// 	DataType: Integer
        /// 	Size: 0
        /// 	Table: R_RELATION
        /// </remarks>
        [Column("MAINCONTACT")]
        public virtual System.Nullable<int> MAINCONTACT
        {
            get
            {
                return this._MAINCONTACT;
            }
            set
            {
                this._MAINCONTACT = value;
            }
        }
        
        /// <summary>
        /// 	Name
        /// </summary>
        /// <remarks>
        /// 	DataType: String
        /// 	Size: 60
        /// </remarks>
        [Column("NAME")]
        public virtual string NAME
        {
            get
            {
                if (string.IsNullOrEmpty(this._NAME))
                {
                    return "";
                }
                return this._NAME;
            }
            set
            {
                this._NAME = value;
            }
        }
        
        /// <summary>
        /// 	Business relations id
        /// </summary>
        /// <remarks>
        /// 	DataType: Integer
        /// 	Size: 4
        /// </remarks>
        [PrimaryKey("PK_R_RELATION")]
        public virtual int Id
        {
            get
            {
                return this._id;
            }
            set
            {
                this._id = value;
            }
        }
        
        /// <summary>
        /// 	Warning
        /// </summary>
        /// <remarks>
        /// 	DataType: String
        /// 	Size: 0
        /// </remarks>
        [Column("WARNING")]
        public virtual string WARNING
        {
            get
            {
                if (string.IsNullOrEmpty(this._WARNING))
                {
                    return "";
                }
                return this._WARNING;
            }
            set
            {
                this._WARNING = value;
            }
        }
    }
}
