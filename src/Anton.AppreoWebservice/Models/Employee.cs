﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Ridder.Client.SDK.Extensions;

namespace Anton.AppreoWebservice.Models
{
    public partial class Employee
    {
        [Ignore] public string NAME => (Person.NAMEPREFIX == "")
            ? $"{Person.FIRSTNAME} {Person.LASTNAME}"
            : $"{Person.FIRSTNAME} {Person.NAMEPREFIX} {Person.LASTNAME}";

        [ForeignKey("FK_PERSON"), Include]
        [JsonIgnore]
        public Person Person { get; set; }


        [Ignore] public string POSITION => Position?.DESCRIPTION;

        [ForeignKey("FK_POSITION"), Include]
        [JsonIgnore]
        public Position Position { get; set; }


        [Ignore] public DateTime? DATEOFBIRTH { get; set; }
        [Ignore] public int? FK_ADDRESS { get; set; }
        [Ignore] public string PERSONALMOBILEPHONENUMBER { get; set; }
        [Ignore] public string PERSONALEMERGENCYNUMBER { get; set; }
        [Ignore] public string PERSONALEMAIL { get; set; }
        [Ignore] public string SPEEDDIAL { get; set; }

        [Ignore] public DateTime? DATEPICTURECHANGED { get; set; }


        [Ignore] public double TIMEMONDAY { get; set; }
        [Ignore] public double TIMETUESDAY { get; set; }
        [Ignore] public double TIMEWEDNESDAY { get; set; }
        [Ignore] public double TIMETHURSDAY { get; set; }
        [Ignore] public double TIMEFRIDAY { get; set; }
        [Ignore] public double TIMESATURDAY { get; set; }
        [Ignore] public double TIMESUNDAY { get; set; }

        [Ignore] public bool ACTIVEDIRECTORYACCOUNTLINKED { get; set; }
    }
}
