namespace Anton.AppreoWebservice.Models
{
    using System;
    using Ridder.Client.SDK.Extensions;
    using System.ComponentModel;
    
    
    public enum VGInspectie : int
    {
        
        /// <summary>
        /// -
        /// </summary>
        [Description("-")]
        N_ = 1,
        
        /// <summary>
        /// Onvoldoende
        /// </summary>
        [Description("Onvoldoende")]
        Onvoldoende = 2,
        
        /// <summary>
        /// Voldoende
        /// </summary>
        [Description("Voldoende")]
        Voldoende = 3,
        
        /// <summary>
        /// Goed
        /// </summary>
        [Description("Goed")]
        Goed = 4,
    }
}
