//  <auto-generated />
namespace Anton.AppreoWebservice.Models
{
    using System;
    using Ridder.Client.SDK.Extensions;
    
    
    ///  <summary>
    ///  	 Table: U_SAFETYREPORTSUBCONTRACTOR
    ///  </summary>
    ///  <remarks>
    /// 	Available columns (for debugging, not necessarily available in this class):
    ///  	CREATOR, DATECHANGED, DATECREATED, EXTERNALKEY, FK_RELATION, FK_SAFETYREPORT, FK_WORKFLOWSTATE, PK_U_SAFETYREPORTSUBCONTRACTOR, RECORDLINK, USERCHANGED
    ///  
    ///  	Filter used: FK_TABLEINFO = '8cafa3ad-38cc-47e3-bfd1-e1d018188215'
    ///  	Included columns: FK_SAFETYREPORT, FK_RELATION
    ///  	Excluded columns: 
    ///  </remarks>
    [Table("U_SAFETYREPORTSUBCONTRACTOR")]
    public partial class SafetyReportSubcontractor
    {
        
        private System.Nullable<int> _FK_RELATION;
        
        private System.Nullable<int> _FK_SAFETYREPORT;
        
        private int _id;
        
        /// <summary>
        /// 	Onderaannemer
        /// </summary>
        /// <remarks>
        /// 	DataType: Integer
        /// 	Size: 4
        /// 	Table: U_SAFETYREPORTSUBCONTRACTOR
        /// </remarks>
        [Column("FK_RELATION")]
        public virtual System.Nullable<int> FK_RELATION
        {
            get
            {
                return this._FK_RELATION;
            }
            set
            {
                this._FK_RELATION = value;
            }
        }
        
        /// <summary>
        /// 	Veiligheidsmelding
        /// </summary>
        /// <remarks>
        /// 	DataType: Integer
        /// 	Size: 4
        /// 	Table: U_SAFETYREPORTSUBCONTRACTOR
        /// </remarks>
        [Column("FK_SAFETYREPORT")]
        public virtual System.Nullable<int> FK_SAFETYREPORT
        {
            get
            {
                return this._FK_SAFETYREPORT;
            }
            set
            {
                this._FK_SAFETYREPORT = value;
            }
        }
        
        /// <summary>
        /// 	Veiligheidsmelding onderaannemers id
        /// </summary>
        /// <remarks>
        /// 	DataType: Integer
        /// 	Size: 4
        /// </remarks>
        [PrimaryKey("PK_U_SAFETYREPORTSUBCONTRACTOR")]
        public virtual int Id
        {
            get
            {
                return this._id;
            }
            set
            {
                this._id = value;
            }
        }
    }
}
