namespace Anton.AppreoWebservice.Models
{
    using System;
    using Ridder.Client.SDK.Extensions;
    using System.ComponentModel;
    
    
    public enum PlanSetting : int
    {
        
        /// <summary>
        /// Production
        /// </summary>
        [Description("Production")]
        Production = 1,
        
        /// <summary>
        /// Service
        /// </summary>
        [Description("Service")]
        Service = 2,
        
        /// <summary>
        /// Project
        /// </summary>
        [Description("Project")]
        Project = 3,
    }
}
