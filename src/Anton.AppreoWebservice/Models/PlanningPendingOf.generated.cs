namespace Anton.AppreoWebservice.Models
{
    using System;
    using Ridder.Client.SDK.Extensions;
    using System.ComponentModel;
    
    
    public enum PlanningPendingOf : int
    {
        
        /// <summary>
        /// -
        /// </summary>
        [Description("-")]
        N_ = 1,
        
        /// <summary>
        /// In afwachting van factuur
        /// </summary>
        [Description("In afwachting van factuur")]
        In_afwachting_van_factuur = 2,
        
        /// <summary>
        /// In afwachting van inkoop
        /// </summary>
        [Description("In afwachting van inkoop")]
        In_afwachting_van_inkoop = 3,
        
        /// <summary>
        /// In afwachting van planactie
        /// </summary>
        [Description("In afwachting van planactie")]
        In_afwachting_van_planactie = 4,
    }
}
