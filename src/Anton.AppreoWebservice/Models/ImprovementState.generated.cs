namespace Anton.AppreoWebservice.Models
{
    using System;
    using Ridder.Client.SDK.Extensions;
    using System.ComponentModel;
    
    
    public enum ImprovementState : int
    {
        
        /// <summary>
        /// Nieuw
        /// </summary>
        [Description("Nieuw")]
        Nieuw = 1,
        
        /// <summary>
        /// In behandeling
        /// </summary>
        [Description("In behandeling")]
        In_behandeling = 2,
        
        /// <summary>
        /// Afgerond
        /// </summary>
        [Description("Afgerond")]
        Afgerond = 3,
        
        /// <summary>
        /// Afgekeurd
        /// </summary>
        [Description("Afgekeurd")]
        Afgekeurd = 4,
    }
}
