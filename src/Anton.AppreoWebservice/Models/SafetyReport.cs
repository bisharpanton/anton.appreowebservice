﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.NetworkInformation;
using System.Threading.Tasks;

namespace Anton.AppreoWebservice.Models
{
    public partial class SafetyReport
    {
        private static readonly Guid _wfStateClosed = new Guid("946b3c25-11ea-4d0a-98b9-8894c42e971e");
        public bool ISDONE => FK_WORKFLOWSTATE == _wfStateClosed;

        public List<int> EmployeeIds { get; set; }
        public List<int> IntercompanyRelationIds { get; set; }
        public List<int> SubcontractorIds { get; set; }
        public List<int> ObservationRoundIds { get; set; }
        public List<int> ImprovementIds { get; set; }
    }
}
