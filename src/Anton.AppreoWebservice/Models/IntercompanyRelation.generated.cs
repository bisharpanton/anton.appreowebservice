namespace Anton.AppreoWebservice.Models
{
    using System;
    using Ridder.Client.SDK.Extensions;
    using System.ComponentModel;
    
    
    public enum IntercompanyRelation : int
    {
        
        /// <summary>
        /// N.v.t.
        /// </summary>
        [Description("N.v.t.")]
        N_v_t_ = 1,
        
        /// <summary>
        /// Anton Air Support
        /// </summary>
        [Description("Anton Air Support")]
        Anton_Air_Support = 2,
        
        /// <summary>
        /// Anton Bouw & Betontechniek
        /// </summary>
        [Description("Anton Bouw & Betontechniek")]
        Anton_Bouw___Betontechniek = 3,
        
        /// <summary>
        /// Anton Civiel en Infra
        /// </summary>
        [Description("Anton Civiel en Infra")]
        Anton_Civiel_en_Infra = 4,
        
        /// <summary>
        /// Anton Constructiewerken
        /// </summary>
        [Description("Anton Constructiewerken")]
        Anton_Constructiewerken = 5,
        
        /// <summary>
        /// Anton Groep
        /// </summary>
        [Description("Anton Groep")]
        Anton_Groep = 6,
        
        /// <summary>
        /// Anton Industrial Services
        /// </summary>
        [Description("Anton Industrial Services")]
        Anton_Industrial_Services = 7,
        
        /// <summary>
        /// Anton Rail & Infra
        /// </summary>
        [Description("Anton Rail & Infra")]
        Anton_Rail___Infra = 8,
        
        /// <summary>
        /// Anton Staalbouw
        /// </summary>
        [Description("Anton Staalbouw")]
        Anton_Staalbouw = 9,
        
        /// <summary>
        /// Anton Airport Trolley Solutions
        /// </summary>
        [Description("Anton Airport Trolley Solutions")]
        Anton_Airport_Trolley_Solutions = 10,
        
        /// <summary>
        /// De Leeuw Protection Systems
        /// </summary>
        [Description("De Leeuw Protection Systems")]
        De_Leeuw_Protection_Systems = 11,
        
        /// <summary>
        /// Harder Constructie en Adviesbureau
        /// </summary>
        [Description("Harder Constructie en Adviesbureau")]
        Harder_Constructie_en_Adviesbureau = 12,
        
        /// <summary>
        /// Hoogendijk Bouw
        /// </summary>
        [Description("Hoogendijk Bouw")]
        Hoogendijk_Bouw = 13,
        
        /// <summary>
        /// Loos Betonreparaties
        /// </summary>
        [Description("Loos Betonreparaties")]
        Loos_Betonreparaties = 14,
        
        /// <summary>
        /// Loos Betonvloeren
        /// </summary>
        [Description("Loos Betonvloeren")]
        Loos_Betonvloeren = 15,
        
        /// <summary>
        /// Loos Betonvloeren Zuid
        /// </summary>
        [Description("Loos Betonvloeren Zuid")]
        Loos_Betonvloeren_Zuid = 16,
        
        /// <summary>
        /// Spires
        /// </summary>
        [Description("Spires")]
        Spires = 17,
        
        /// <summary>
        /// Spanberg
        /// </summary>
        [Description("Spanberg")]
        Spanberg = 18,
        
        /// <summary>
        /// Staalis
        /// </summary>
        [Description("Staalis")]
        Staalis = 19,
    }
}
