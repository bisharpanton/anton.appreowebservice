﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Anton.AppreoWebservice.Models
{
    public class JoborderDetailWorkactivityWorkflowStates
    {
        public static readonly Guid New = new Guid("ea528c25-b77e-49c8-8838-3e15acf24d7b");
        public static readonly Guid ReleasedForProduction = new Guid("051e43f6-8d6b-467a-999a-7fb9da1a9625");
        public static readonly Guid Done = new Guid("9d458cbf-07ca-4716-a6f1-b56391f22fa6");
    }
}
