﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Ridder.Client.SDK.Extensions;

namespace Anton.AppreoWebservice.Models
{
    public partial class Tool
    {
        public string ToolKindDescription => ToolKind?.DESCRIPTION ?? "";

        [ForeignKey("FK_GEREEDSCHAPSOORT"), Include]
        [JsonIgnore]
        public ToolKind ToolKind { get; set; }
    }
}
