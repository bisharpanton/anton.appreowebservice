//  <auto-generated />
namespace Anton.AppreoWebservice.Models
{
    using System;
    using Ridder.Client.SDK.Extensions;
    
    
    ///  <summary>
    ///  	 Table: C_DELETEDRECORDS
    ///  </summary>
    ///  <remarks>
    /// 	Available columns (for debugging, not necessarily available in this class):
    ///  	CREATOR, DATECHANGED, DATECREATED, EXTERNALKEY, FK_WORKFLOWSTATE, PK_C_DELETEDRECORDS, RECORDID, RECORDLINK, TABLENAME, USERCHANGED
    ///  
    ///  	Filter used: FK_TABLEINFO = '5a7350c5-b21b-42eb-8f29-ca301ddb959c'
    ///  	Included columns: CREATOR, DATECREATED, TABLENAME, RECORDID
    ///  	Excluded columns: 
    ///  </remarks>
    [Table("C_DELETEDRECORDS")]
    public partial class DeletedRecord
    {
        
        private string _CREATOR;
        
        private System.Nullable<System.DateTime> _DATECREATED;
        
        private int _id;
        
        private System.Nullable<int> _RECORDID;
        
        private string _TABLENAME;
        
        /// <summary>
        /// 	Aangemaakt door
        /// </summary>
        /// <remarks>
        /// 	DataType: String
        /// 	Size: 50
        /// </remarks>
        [Column("CREATOR")]
        public virtual string CREATOR
        {
            get
            {
                if (string.IsNullOrEmpty(this._CREATOR))
                {
                    return "";
                }
                return this._CREATOR;
            }
            set
            {
                this._CREATOR = value;
            }
        }
        
        /// <summary>
        /// 	Datum aangemaakt
        /// </summary>
        /// <remarks>
        /// 	DataType: DateTime
        /// 	Size: 8
        /// </remarks>
        [Column("DATECREATED")]
        public virtual System.Nullable<System.DateTime> DATECREATED
        {
            get
            {
                return this._DATECREATED;
            }
            set
            {
                this._DATECREATED = value;
            }
        }
        
        /// <summary>
        /// 	Verwijderde records id
        /// </summary>
        /// <remarks>
        /// 	DataType: Integer
        /// 	Size: 4
        /// </remarks>
        [PrimaryKey("PK_C_DELETEDRECORDS")]
        public virtual int Id
        {
            get
            {
                return this._id;
            }
            set
            {
                this._id = value;
            }
        }
        
        /// <summary>
        /// 	Record id
        /// </summary>
        /// <remarks>
        /// 	DataType: Integer
        /// 	Size: 4
        /// </remarks>
        [Column("RECORDID")]
        public virtual System.Nullable<int> RECORDID
        {
            get
            {
                return this._RECORDID;
            }
            set
            {
                this._RECORDID = value;
            }
        }
        
        /// <summary>
        /// 	Tabelnaam
        /// </summary>
        /// <remarks>
        /// 	DataType: String
        /// 	Size: 150
        /// </remarks>
        [Column("TABLENAME")]
        public virtual string TABLENAME
        {
            get
            {
                if (string.IsNullOrEmpty(this._TABLENAME))
                {
                    return "";
                }
                return this._TABLENAME;
            }
            set
            {
                this._TABLENAME = value;
            }
        }
    }
}
