﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Anton.AppreoWebservice.Framework;
using Anton.AppreoWebservice.Models;
using Anton.AppreoWebservice.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Ridder.Client.SDK;
using Ridder.Client.SDK.Extensions;

namespace Anton.AppreoWebservice.Controllers
{
    [Route("api")]
    [ApiController]

    public class TodosController : ControllerBase
    {
        private readonly ISdkSessionProvider _sessionProvider;

        public TodosController(ISdkSessionProvider sessionProvider)
        {
            _sessionProvider = sessionProvider;
        }

        // GET: api/Todos
        [HttpGet("{company}/[controller]")]
        public ActionResult<PagedResponse<Todo>> Get(string company, [FromQuery] PagingOptions pagingOptions,
            DateTime? dateChanged = null)
        {
            if (string.IsNullOrEmpty(company))
            {
                return BadRequest("No company specified.");
            }

            var sdk = _sessionProvider.Get(company);
            if (sdk == null)
            {
                return BadRequest($"Company '{company}' not found.");
            }

            var filter = (dateChanged.HasValue)
                ? $"DATECHANGED >= '{dateChanged.Value:yyyyMMdd HH:mm:ss}'"
                : "";

            var todos = sdk.FindPaged<Todo>(pagingOptions.Page + 1, pagingOptions.PageSize, filter).ToList();
            var totalResults =
                sdk.Sdk.CreateRecordset("R_TODO", "PK_R_TODO", filter, "").RecordCount;

            return new PagedResponse<Todo>().ViewModelsToPagedResponse(todos, totalResults, pagingOptions);
        }

        // POST: api/Todos
        [HttpPost("{company}/[controller]")]
        public ActionResult<Todo> Post(string company, [FromBody] CreateTodo createTodo)
        {
            if (string.IsNullOrEmpty(company))
            {
                return BadRequest("No company specified.");
            }

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (createTodo.TodoType == 0 || createTodo.EmployeeId == 0 || string.IsNullOrEmpty(createTodo.Description))
            {
                return BadRequest("Todo type, Employee and description are required.");
            }

            var sdk = _sessionProvider.Get(company);
            if (sdk == null)
            {
                return NotFound($"Company '{company}' not found.");
            }

            var todo = new Todo()
            {
                FK_TODOTYPE = createTodo.TodoType,
                DESCRIPTION = createTodo.Description,
                FK_ASSIGNEDTO = createTodo.EmployeeId,
                DUEDATE = createTodo.DueDate,
                INTERNALMEMO = createTodo.InternalMemo,
                FK_RELATION = !createTodo.RelationId.HasValue || createTodo.RelationId == 0 ? (int?) null : createTodo.RelationId,
            };

            var validateResult = ValidateTodo(sdk, todo);

            if (!validateResult.Result)
            {
                return BadRequest(validateResult.Message);
            }

            sdk.Insert(todo);

            return Ok(todo);
        }

        // PUT: api/Todos
        [HttpPut("{company}/[controller]")]
        public ActionResult Put(string company, [FromBody] Todo todo)
        {
            if (string.IsNullOrEmpty(company))
            {
                return BadRequest("No company specified.");
            }

            if (todo.FK_TODOTYPE == 0 || todo.FK_ASSIGNEDTO == 0 || string.IsNullOrEmpty(todo.DESCRIPTION))
            {
                return BadRequest("Todo type, Employee and description are required.");
            }

            var sdk = _sessionProvider.Get(company);
            if (sdk == null)
            {
                return BadRequest($"Company '{company}' not found.");
            }

            var existingTodo = sdk.Find<Todo>($"PK_R_TODO = {todo.Id}").FirstOrDefault();

            if (existingTodo == null)
            {
                return BadRequest($"No todo found with id {todo.Id}.");
            }
            var validateResult = ValidateTodo(sdk, todo);

            if (!validateResult.Result)
            {
                return BadRequest(validateResult.Message);
            }
            
            //Bij een wijzigings actie mogen deze velden niet gewijzigd worden.
            todo.DATEDONE = existingTodo.DATEDONE;
            todo.FK_DONEBY = existingTodo.FK_DONEBY;

            sdk.Update(todo);

            return Ok(todo);
        }

        // PUT: api/Todos/SetTodoDone/5
        [HttpPut("{company}/[controller]/SetTodoDone/{id}")]
        public ActionResult SetTodoDone(string company, int id,  [FromBody] SetTodoDoneInfo setTodoDoneInfo)
        {
            if (string.IsNullOrEmpty(company))
            {
                return BadRequest("No company specified.");
            }

            var sdk = _sessionProvider.Get(company);
            if (sdk == null)
            {
                return BadRequest($"Company '{company}' not found.");
            }

            var todo = sdk.Find<Todo>($"PK_R_TODO = {id}").FirstOrDefault();

            if (todo == null)
            {
                return BadRequest($"No todo found with id {id}.");
            }

            if (todo.FK_DONEBY.HasValue)
            {
                return Ok(todo);
            }

            if (sdk.Find<Employee>(x => x.Id == setTodoDoneInfo.DoneBy).FirstOrDefault() == null)
            {
                return BadRequest($"No employee found with id {setTodoDoneInfo.DoneBy}.");
            }

            var wfSetToDone = new Guid("d94de796-4b24-44ff-9c66-230d1511d144");
            var wfResult = sdk.Sdk.ExecuteWorkflowEvent("R_TODO", todo.Id, wfSetToDone, null);

            if (wfResult.HasError)
            {
                return BadRequest($"Set to done failed; cause: {wfResult.GetResult()}.");
            }

            //We halen het object opnieuw op, om de velden 'FK_DONEBY' en 'DATEDONE' bij te werken
            var updatedTodo = sdk.Find<Todo>($"PK_R_TODO = {id}").First();

            updatedTodo.FK_DONEBY = setTodoDoneInfo.DoneBy;
            updatedTodo.DATEDONE = setTodoDoneInfo.DateDone;

            sdk.Update(updatedTodo);
            
            return Ok(updatedTodo);
        }

        // PUT: api/Todos/RejectTodo/5
        [HttpPut("{company}/[controller]/RejectTodo/{id}")]
        public ActionResult RejectTodo(string company, int id, [FromBody] RejectTodoInfo rejectTodoInfo)
        {
            if (string.IsNullOrEmpty(company))
            {
                return BadRequest("No company specified.");
            }

            var sdk = _sessionProvider.Get(company);
            if (sdk == null)
            {
                return BadRequest($"Company '{company}' not found.");
            }

            var todo = sdk.Find<Todo>($"PK_R_TODO = {id}").FirstOrDefault();

            if (todo == null)
            {
                return BadRequest($"No todo found with id {id}.");
            }

            if (todo.FK_REJECTEDBY.HasValue)
            {
                return Ok(todo);
            }

            if (sdk.Find<Employee>(x => x.Id == rejectTodoInfo.RejectedBy).FirstOrDefault() == null)
            {
                return BadRequest($"No employee found with id {rejectTodoInfo.RejectedBy}.");
            }

            var wfRejectTodo = new Guid("8df4a1fb-9829-402f-8fbe-f3342da4b9f9");
            var wfResult = sdk.Sdk.ExecuteWorkflowEvent("R_TODO", todo.Id, wfRejectTodo, null);

            if (wfResult.HasError)
            {
                return BadRequest($"Reject todo failed; cause: {wfResult.GetResult()}.");
            }

            //We halen het object opnieuw op, om de velden 'FK_REJECTEDBY' en 'DATEREJECTED' bij te werken
            var updatedTodo = sdk.Find<Todo>($"PK_R_TODO = {id}").First();

            updatedTodo.FK_REJECTEDBY = rejectTodoInfo.RejectedBy;
            updatedTodo.DATEREJECTED = rejectTodoInfo.DateRejected;

            sdk.Update(updatedTodo);
            
            return Ok(updatedTodo);
        }

        // PUT: api/Todos/SetTodoOpen/5
        [HttpPut("{company}/[controller]/SetTodoOpen/{id}")]
        public ActionResult SetTodoOpen(string company, int id)
        {
            if (string.IsNullOrEmpty(company))
            {
                return BadRequest("No company specified.");
            }

            var sdk = _sessionProvider.Get(company);
            if (sdk == null)
            {
                return BadRequest($"Company '{company}' not found.");
            }

            var todo = sdk.Find<Todo>($"PK_R_TODO = {id}").FirstOrDefault();

            if (todo == null)
            {
                return BadRequest($"No todo found with id {id}.");
            }

            if (!todo.FK_DONEBY.HasValue)
            {
                return Ok(todo);
            }

            var wfSetToOpen = new Guid("88e3d5c7-e8ed-49fc-a864-57d47ace2470");
            var wfResult = sdk.Sdk.ExecuteWorkflowEvent("R_TODO", todo.Id, wfSetToOpen, null);

            if (wfResult.HasError)
            {
                return BadRequest($"Set to open failed; cause: {wfResult.GetResult()}.");
            }

            //We halen het object opnieuw op, om de velden 'FK_DONEBY' en 'DATEDONE' zijn bijgewerkt
            var updatedTodo = sdk.Find<Todo>($"PK_R_TODO = {id}").First();

            return Ok(updatedTodo);
        }

        // DELETE api/Todos/5
        [HttpDelete("{company}/[controller]/{id}")]
        public ActionResult Delete(string company, int id)
        {
            if (string.IsNullOrEmpty(company))
            {
                return BadRequest("No company specified.");
            }

            var sdk = _sessionProvider.Get(company);
            if (sdk == null)
            {
                return BadRequest($"Company '{company}' not found.");
            }

            var rsTodo = sdk.GetRecordsetColumns("R_TODO", "",
                $"PK_R_TODO = {id}");

            if (rsTodo.RecordCount == 0)
            {
                return BadRequest($"No todo found with id {id}.");
            }

            rsTodo.UpdateWhenMoveRecord = false;
            rsTodo.MoveFirst();
            rsTodo.Delete();

            var updateResult = rsTodo.Update2();

            if (updateResult.Any(x => x.HasError))
            {
                return BadRequest(
                    $"Deleting todo failed, cause: {updateResult.First(x => x.HasError).GetResult()}");
            }

            return Ok();
        }

        private ValidateResult ValidateTodo(SdkSession sdk, Todo todo)
        {
            var employee = sdk.Find<Employee>(x => x.Id == todo.FK_ASSIGNEDTO).FirstOrDefault();

            if (employee == null)
            {
                return new ValidateResult()
                {
                    Result = false, Message = $"No employee found with id {todo.FK_ASSIGNEDTO}"
                };
            }

            var todoType = sdk.Find<TodoType>(x => x.Id == todo.FK_TODOTYPE).FirstOrDefault();

            if (todoType == null)
            {
                return new ValidateResult()
                {
                    Result = false, Message = $"No todo type found with id {todo.FK_TODOTYPE}"
                };
            }

            return new ValidateResult() {Result = true, Message = ""};
        }
    }
}
