﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using Anton.AppreoWebservice.Framework;
using Anton.AppreoWebservice.Models;
using Anton.AppreoWebservice.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Ridder.Client.SDK.Extensions;

namespace Anton.AppreoWebservice.Controllers
{
    [Route("api")]
    [ApiController]

    public class DeclarationsController : ControllerBase
    {
        private readonly ISdkSessionProvider _sessionProvider;

        public DeclarationsController(ISdkSessionProvider sessionProvider)
        {
            _sessionProvider = sessionProvider;
        }

        // GET: api/Declarations
        [HttpGet("{company}/[controller]")]
        public ActionResult<PagedResponse<Declaration>> Get(string company, [FromQuery] PagingOptions pagingOptions,
            DateTime? dateChanged = null)
        {
            if (string.IsNullOrEmpty(company))
            {
                return BadRequest("No company specified.");
            }

            var sdk = _sessionProvider.Get(company);
            if (sdk == null)
            {
                return BadRequest($"Company '{company}' not found.");
            }

            var relationsFromEmployees = sdk.Find<Employee>("FK_RELATION IS NOT NULL").ToList();

            if(!relationsFromEmployees.Any())
            {
                return new PagedResponse<Declaration>
                {
                    Page = pagingOptions?.Page,
                    PageSize = pagingOptions?.PageSize,
                    TotalResults = 0,
                    Results = new List<Declaration>(),
                };
            }

            var filter = (dateChanged.HasValue)
                ? $"DECLARATIONTYPE <> {(int)DeclarationType.N_v_t_} AND DATECHANGED >= '{dateChanged.Value:yyyyMMdd HH:mm:ss}'"
                : $"DECLARATIONTYPE <> {(int)DeclarationType.N_v_t_}";

            var purchaseInvoices = sdk.FindPaged<PurchaseInvoice>(pagingOptions.Page + 1, pagingOptions.PageSize, filter).ToList();
            var totalResults =
                sdk.Sdk.CreateRecordset("R_PURCHASEINVOICE", "PK_R_PURCHASEINVOICE", filter, "").RecordCount;

            var purchaseInvoiceIds = purchaseInvoices.Select(x => x.Id).ToList();
            var purchaseInvoiceDetailsMisc =
                sdk.Find<PurchaseInvoiceDetailMisc>(x => purchaseInvoiceIds.Contains(x.FK_PURCHASEINVOICE)).ToList();

            //Map purchase invoice info to declaration
            var declarations = MapPurchaseInvoiceInfoToDeclarations(purchaseInvoices, purchaseInvoiceDetailsMisc, relationsFromEmployees);

            return new PagedResponse<Declaration>().ViewModelsToPagedResponse(declarations, totalResults, pagingOptions);
        }

        // POST: api/Declarations
        [HttpPost("{company}/[controller]")]
        public ActionResult<Declaration> Post(string company, [FromBody] Declaration createDeclaration)
        {
            if (string.IsNullOrEmpty(company))
            {
                return BadRequest("No company specified.");
            }

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (createDeclaration.FK_EMPLOYEE == 0)
            {
                return BadRequest("Employee is required to create a declaration.");
            }

            if (Math.Abs(createDeclaration.QUANTITY) < 0.0001)
            {
                return BadRequest("Quantity cannot be zero.");
            }

            var sdk = _sessionProvider.Get(company);
            if (sdk == null)
            {
                return NotFound($"Company '{company}' not found.");
            }

            var employee = sdk.Find<Employee>(x => x.Id == createDeclaration.FK_EMPLOYEE).FirstOrDefault();

            if (employee == null)
            {
                return BadRequest($"No employee found with id {createDeclaration.FK_EMPLOYEE}.");
            }

            var logDetermineSupplier = "";
            var supplierId = DetermineSupplier(sdk, employee, ref logDetermineSupplier);

            if(supplierId == 0)
            {
                return BadRequest("Fout bij bepalen relatie.");
            }

            var validateOrderMessage = ValidateOrder(sdk, createDeclaration.FK_ORDER, createDeclaration.FK_JOBORDER);
            if (!string.IsNullOrEmpty(validateOrderMessage))
            {
                return BadRequest(validateOrderMessage);
            }

            //Bij het aanmaken van de inkoopfactuur vullen we de aangemaakte factuur id in op het object 'createDeclaration'.
            var resultMessage = CreatePurchaseInvoice(sdk, createDeclaration, employee, supplierId, logDetermineSupplier);

            if (!string.IsNullOrEmpty(resultMessage))
            {
                return BadRequest(resultMessage);
            }
            
            return Ok(createDeclaration);
        }

        // PUT: api/Declarations
        [HttpPut("{company}/[controller]")]
        public ActionResult<Declaration> Put(string company, [FromBody] Declaration createDeclaration)
        {
            if (string.IsNullOrEmpty(company))
            {
                return BadRequest("No company specified.");
            }

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (createDeclaration.PURCHASEINVOICEID == 0)
            {
                return BadRequest("Purchase invoice id is required to change a declaration.");
            }

            if (createDeclaration.FK_EMPLOYEE == 0)
            {
                return BadRequest("Employee is required to change a declaration.");
            }

            if (Math.Abs(createDeclaration.QUANTITY) < 0.0001)
            {
                return BadRequest("Quantity cannot be zero.");
            }

            var sdk = _sessionProvider.Get(company);
            if (sdk == null)
            {
                return NotFound($"Company '{company}' not found.");
            }

            var employee = sdk.Find<Employee>(x => x.Id == createDeclaration.FK_EMPLOYEE).FirstOrDefault();

            if (employee == null)
            {
                return BadRequest($"No employee found with id {createDeclaration.FK_EMPLOYEE}.");
            }

            var logDetermineSupplier = "";
            var supplierId = DetermineSupplier(sdk, employee, ref logDetermineSupplier);

            if (supplierId == 0)
            {
                return BadRequest("Fout bij bepalen relatie.");
            }

            var validateOrderMessage = ValidateOrder(sdk, createDeclaration.FK_ORDER, createDeclaration.FK_JOBORDER);
            if (!string.IsNullOrEmpty(validateOrderMessage))
            {
                return BadRequest(validateOrderMessage);
            }

            //Bij het aanmaken van de inkoopfactuur vullen we de aangemaakte factuur id in op het object 'createDeclaration'.
            var resultMessage = ChangePurchaseInvoice(sdk, createDeclaration, employee, supplierId, logDetermineSupplier);

            if (!string.IsNullOrEmpty(resultMessage))
            {
                return BadRequest(resultMessage);
            }

            return Ok(createDeclaration);
        }

        // Delete: api/Declarations
        [HttpDelete("{company}/[controller]")]
        public ActionResult<Declaration> Delete(string company, [FromBody] Declaration createDeclaration)
        {
            if (string.IsNullOrEmpty(company))
            {
                return BadRequest("No company specified.");
            }

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (createDeclaration.PURCHASEINVOICEID == 0)
            {
                return BadRequest("Purchase invoice id is required to delete a declaration.");
            }

            var sdk = _sessionProvider.Get(company);
            if (sdk == null)
            {
                return NotFound($"Company '{company}' not found.");
            }

            var purchaseInvoice = sdk.Find<PurchaseInvoice>(x => x.Id == createDeclaration.PURCHASEINVOICEID).FirstOrDefault();

            if (purchaseInvoice == null)
            {
                return BadRequest($"No Purchase invoice found with id {createDeclaration.PURCHASEINVOICEID}");
            }

            var wfStateNew = new Guid("466c4d1e-21ce-47bb-a2c6-8b96c6ec0d23");
            if (purchaseInvoice.FK_WORKFLOWSTATE != wfStateNew)
            {
                return BadRequest($"Purchase invoice is not new anymore, deleting purchase invoice is not allowed.");
            }

            try
            {
                sdk.Delete(purchaseInvoice);
            }
            catch(Exception e)
            {
                return BadRequest($"Delete purchase invoice failed, cause:{e}");
            }

            return Ok();
        }

        private string CreatePurchaseInvoice(SdkSession sdk, Declaration createDeclaration, Employee employee,
            int supplierId, string logDetermineSupplier)
        {
            var rsPurchaseInvoice = sdk.GetRecordsetColumns("R_PURCHASEINVOICE", "",
                $"PK_R_PURCHASEINVOICE = -1");
            rsPurchaseInvoice.UseDataChanges = true;
            rsPurchaseInvoice.UpdateWhenMoveRecord = false;

            rsPurchaseInvoice.AddNew();
            rsPurchaseInvoice.Fields["FK_SUPPLIER"].Value = supplierId;
            rsPurchaseInvoice.Fields["INVOICEDATE"].Value = createDeclaration.DATE;

            rsPurchaseInvoice.Fields["EXTERNALINVOICENUMBER"].Value = createDeclaration.DESCRIPTION.Length > 25
                ? createDeclaration.DESCRIPTION.Substring(0, 25)
                : createDeclaration.DESCRIPTION;

            rsPurchaseInvoice.Fields["DECLARATIONTYPE"].Value = createDeclaration.DECLARATIONTYPE;

            rsPurchaseInvoice.Fields["FK_INVOICEAUDITOR"].Value = employee.FK_MANAGER ?? (object)DBNull.Value;

            var currentEmployee = GetCurrentEmployee(sdk);
            rsPurchaseInvoice.Fields["FK_CREATEDBY"].Value = currentEmployee == 0
                ? employee.Id
                : currentEmployee;

            if (!string.IsNullOrEmpty(logDetermineSupplier))
            {
                rsPurchaseInvoice.Fields["MEMO"].Value = logDetermineSupplier;
            }

            var updateResult = rsPurchaseInvoice.Update2();

            if (updateResult != null && updateResult.Any(x => x.HasError))
            {
                return $"Aanmaken inkoopfactuur mislukt, oorzaak: {updateResult.First(x => x.HasError).GetResult()}";
            }

            var createdPurchaseInvoiceId = (int)updateResult.First().PrimaryKey;

            //Plaats boekstuknummer voor extern factuurnummer
            var rsPurchaseInvoice2 = sdk.GetRecordsetColumns("R_PURCHASEINVOICE",
                "FK_JOURNALENTRY, EXTERNALINVOICENUMBER, PAYMENTREFERENCE",
                $"PK_R_PURCHASEINVOICE = {createdPurchaseInvoiceId}");
            rsPurchaseInvoice2.MoveFirst();

            var oldExternalInvoiceNumber = rsPurchaseInvoice2.GetField("EXTERNALINVOICENUMBER").Value.ToString();
            var newExternalInvoiceNumber =
                $"{GetJournalEntryNumber(sdk, (int)rsPurchaseInvoice2.GetField("FK_JOURNALENTRY").Value)}-{oldExternalInvoiceNumber}";

            var newExternalInvoiceNumberResult = newExternalInvoiceNumber.Length > 25
                ? newExternalInvoiceNumber.Substring(0, 25)
                : newExternalInvoiceNumber;

            rsPurchaseInvoice2.Fields["EXTERNALINVOICENUMBER"].Value = newExternalInvoiceNumberResult;
            rsPurchaseInvoice2.Fields["PAYMENTREFERENCE"].Value = newExternalInvoiceNumberResult;

            var updateResult2 = rsPurchaseInvoice2.Update2();

            if (updateResult2 != null && updateResult2.Any(x => x.HasError))
            {
                return $"Aanpassen extern factuurnummer mislukt, oorzaak: {updateResult2.First(x => x.HasError).GetResult()}";
            }

            //Aanmaken inkoopfactuurregel divers
            var miscDeclarations = sdk.GetRecordsetColumns("R_CRMSETTINGS", "FK_MISCDECLARATIONS", "")
                .DataTable.AsEnumerable().First().Field<int?>("FK_MISCDECLARATIONS") ?? 0;

            if(miscDeclarations == 0)
            {
                return $"Geen divers declaraties aanwezig in de CRM-instellingen.";
            }

            var rsPurchaseInvoiceDetailMisc = sdk.GetRecordsetColumns("R_PURCHASEINVOICEDETAILMISC", "",
                $"PK_R_PURCHASEINVOICEDETAILMISC = -1");
            rsPurchaseInvoiceDetailMisc.UseDataChanges = true;
            rsPurchaseInvoiceDetailMisc.UpdateWhenMoveRecord = false;

            rsPurchaseInvoiceDetailMisc.AddNew();

            rsPurchaseInvoiceDetailMisc.Fields["FK_PURCHASEINVOICE"].Value = createdPurchaseInvoiceId;
            rsPurchaseInvoiceDetailMisc.Fields["FK_MISC"].Value = miscDeclarations;
            
            rsPurchaseInvoiceDetailMisc.Fields["DESCRIPTION"].Value = createDeclaration.DESCRIPTION.Length > 80
                ? createDeclaration.DESCRIPTION.Substring(0, 80)
                : createDeclaration.DESCRIPTION;

            rsPurchaseInvoiceDetailMisc.Fields["QUANTITY"].Value = createDeclaration.QUANTITY;
            //rsPurchaseInvoiceDetailMisc.Fields["GROSSPURCHASEPRICE"].Value = createDeclaration.AMOUNT;
            rsPurchaseInvoiceDetailMisc.Fields["TOTALAMOUNT"].Value = createDeclaration.TOTALAMOUNT;

            if (!string.IsNullOrEmpty(createDeclaration.FK_ORDER))
            {
                if(!int.TryParse(createDeclaration.FK_ORDER, NumberStyles.Any, CultureInfo.CurrentCulture, out int iOrderId))
                {
                    return $"Order '{createDeclaration.FK_ORDER}' can't be parsed to a integer.";
                }

                if (iOrderId != 0)
                {
                    rsPurchaseInvoiceDetailMisc.Fields["DIRECTTOORDER"].Value = true;
                    rsPurchaseInvoiceDetailMisc.Fields["FK_ORDER"].Value = iOrderId;

                    if(!string.IsNullOrEmpty(createDeclaration.FK_JOBORDER))
                    {
                        if (!int.TryParse(createDeclaration.FK_JOBORDER, NumberStyles.Any, CultureInfo.CurrentCulture, out int iJobOrderId))
                        {
                            return $"Job order '{createDeclaration.FK_JOBORDER}' can't be parsed to a integer.";
                        }

                        rsPurchaseInvoiceDetailMisc.Fields["FK_JOBORDER"].Value = iJobOrderId;
                    }
                }
            }

            var updateResult3 = rsPurchaseInvoiceDetailMisc.Update2();

            if (updateResult3 != null && updateResult3.Any(x => x.HasError))
            {
                return $"Aanmaken inkoopfactuurregel divers mislukt, oorzaak: {updateResult3.First(x => x.HasError).GetResult()}";
            }


            createDeclaration.PURCHASEINVOICEID = createdPurchaseInvoiceId;
            createDeclaration.STATE = "Open";

            return string.Empty; 
        }

        private string ChangePurchaseInvoice(SdkSession sdk, Declaration createDeclaration, Employee employee,
                int supplierId, string logDetermineSupplier)
        {
            var rsPurchaseInvoice = sdk.GetRecordsetColumns("R_PURCHASEINVOICE", "",
                $"PK_R_PURCHASEINVOICE = {createDeclaration.PURCHASEINVOICEID}");

            if(rsPurchaseInvoice.RecordCount == 0)
            {
                return $"No purchase invoice found with id {createDeclaration.PURCHASEINVOICEID}";
            }

            rsPurchaseInvoice.MoveFirst();

            var wfStateNew = new Guid("466c4d1e-21ce-47bb-a2c6-8b96c6ec0d23");
            if((Guid)rsPurchaseInvoice.GetField("FK_WORKFLOWSTATE").Value != wfStateNew)
            {
                return $"Purchase invoice is not new anymore, changing purchase invoice is not allowed.";
            }

            rsPurchaseInvoice.UseDataChanges = true;
            rsPurchaseInvoice.UpdateWhenMoveRecord = false;

            rsPurchaseInvoice.Fields["FK_SUPPLIER"].Value = supplierId;
            rsPurchaseInvoice.Fields["INVOICEDATE"].Value = createDeclaration.DATE;

            rsPurchaseInvoice.Fields["EXTERNALINVOICENUMBER"].Value = createDeclaration.DESCRIPTION.Length > 25
                ? createDeclaration.DESCRIPTION.Substring(0, 25)
                : createDeclaration.DESCRIPTION;

            rsPurchaseInvoice.Fields["DECLARATIONTYPE"].Value = createDeclaration.DECLARATIONTYPE;

            rsPurchaseInvoice.Fields["FK_INVOICEAUDITOR"].Value = employee.FK_MANAGER ?? (object)DBNull.Value;

            var updateResult = rsPurchaseInvoice.Update2();

            if (updateResult != null && updateResult.Any(x => x.HasError))
            {
                return $"Wijzigen inkoopfactuur mislukt, oorzaak: {updateResult.First(x => x.HasError).GetResult()}";
            }

            //Plaats boekstuknummer voor extern factuurnummer
            var rsPurchaseInvoice2 = sdk.GetRecordsetColumns("R_PURCHASEINVOICE",
                "FK_JOURNALENTRY, EXTERNALINVOICENUMBER, PAYMENTREFERENCE",
                $"PK_R_PURCHASEINVOICE = {createDeclaration.PURCHASEINVOICEID}");
            rsPurchaseInvoice2.MoveFirst();

            var newJournalEntryNumber = GetJournalEntryNumber(sdk, (int)rsPurchaseInvoice2.GetField("FK_JOURNALENTRY").Value);

            if (!rsPurchaseInvoice2.Fields["EXTERNALINVOICENUMBER"].Value.ToString().StartsWith(newJournalEntryNumber.ToString()))
            {
                var oldExternalInvoiceNumber = rsPurchaseInvoice2.GetField("EXTERNALINVOICENUMBER").Value.ToString();
                var oldExternalInvoiceNumberInfo = oldExternalInvoiceNumber.Split('-');

                if(oldExternalInvoiceNumberInfo.Length >= 2)
                {
                    var oldJournalEntryNumber = oldExternalInvoiceNumber.Split('-').First();

                    

                    var newExternalInvoiceNumber = oldExternalInvoiceNumber.Replace(oldJournalEntryNumber, newJournalEntryNumber.ToString());

                    var newExternalInvoiceNumberResult = newExternalInvoiceNumber.Length > 25
                        ? newExternalInvoiceNumber.Substring(0, 25)
                        : newExternalInvoiceNumber;

                    rsPurchaseInvoice2.Fields["EXTERNALINVOICENUMBER"].Value = newExternalInvoiceNumberResult;
                    rsPurchaseInvoice2.Fields["PAYMENTREFERENCE"].Value = newExternalInvoiceNumberResult;

                    var updateResult2 = rsPurchaseInvoice2.Update2();

                    if (updateResult2 != null && updateResult2.Any(x => x.HasError))
                    {
                        return $"Aanpassen extern factuurnummer mislukt, oorzaak: {updateResult2.First(x => x.HasError).GetResult()}";
                    }
                }
            }


            

            


            //Wijzgen inkoopfactuurregel divers
            var miscDeclarations = sdk.GetRecordsetColumns("R_CRMSETTINGS", "FK_MISCDECLARATIONS", "")
                .DataTable.AsEnumerable().First().Field<int?>("FK_MISCDECLARATIONS") ?? 0;

            if (miscDeclarations == 0)
            {
                return $"Geen divers declaraties aanwezig in de CRM-instellingen.";
            }

            var rsPurchaseInvoiceDetailMisc = sdk.GetRecordsetColumns("R_PURCHASEINVOICEDETAILMISC", "",
                $"FK_PURCHASEINVOICE = {createDeclaration.PURCHASEINVOICEID} AND FK_MISC = {miscDeclarations}");

            if(rsPurchaseInvoiceDetailMisc.RecordCount > 1)
            {
                return $"Meerdere declaratie regels gevonden. Administratie heeft de declaratie al verwerkt. Wijzigen niet meer mogelijk.";
            }

            rsPurchaseInvoiceDetailMisc.UseDataChanges = true;
            rsPurchaseInvoiceDetailMisc.UpdateWhenMoveRecord = false;

            rsPurchaseInvoiceDetailMisc.MoveFirst();

            rsPurchaseInvoiceDetailMisc.Fields["DESCRIPTION"].Value = createDeclaration.DESCRIPTION.Length > 80
                ? createDeclaration.DESCRIPTION.Substring(0, 80)
                : createDeclaration.DESCRIPTION;

            rsPurchaseInvoiceDetailMisc.Fields["QUANTITY"].Value = createDeclaration.QUANTITY;
            //rsPurchaseInvoiceDetailMisc.Fields["GROSSPURCHASEPRICE"].Value = createDeclaration.AMOUNT;
            rsPurchaseInvoiceDetailMisc.Fields["TOTALAMOUNT"].Value = createDeclaration.TOTALAMOUNT;

            if (!string.IsNullOrEmpty(createDeclaration.FK_ORDER))
            {
                if (!int.TryParse(createDeclaration.FK_ORDER, NumberStyles.Any, CultureInfo.CurrentCulture, out int iOrderId))
                {
                    return $"Order '{createDeclaration.FK_ORDER}' can't be parsed to a integer.";
                }

                if (iOrderId != 0)
                {
                    rsPurchaseInvoiceDetailMisc.Fields["DIRECTTOORDER"].Value = true;
                    rsPurchaseInvoiceDetailMisc.Fields["FK_ORDER"].Value = iOrderId;
                }

                if (!string.IsNullOrEmpty(createDeclaration.FK_JOBORDER))
                {
                    if (!int.TryParse(createDeclaration.FK_JOBORDER, NumberStyles.Any, CultureInfo.CurrentCulture, out int iJobOrderId))
                    {
                        return $"Job order '{createDeclaration.FK_JOBORDER}' can't be parsed to a integer.";
                    }

                    rsPurchaseInvoiceDetailMisc.Fields["FK_JOBORDER"].Value = iJobOrderId;
                }
                else
                {
                    rsPurchaseInvoiceDetailMisc.Fields["FK_JOBORDER"].Value = DBNull.Value;
                }
            }
            else
            {
                rsPurchaseInvoiceDetailMisc.Fields["DIRECTTOORDER"].Value = false;
                rsPurchaseInvoiceDetailMisc.Fields["FK_ORDER"].Value = DBNull.Value;
                rsPurchaseInvoiceDetailMisc.Fields["FK_JOBORDER"].Value = DBNull.Value;
            }

            var updateResult3 = rsPurchaseInvoiceDetailMisc.Update2();

            if (updateResult3 != null && updateResult3.Any(x => x.HasError))
            {
                return $"Wijzigen inkoopfactuurregel divers mislukt, oorzaak: {updateResult3.First(x => x.HasError).GetResult()}";
            }

            return string.Empty;
        }

        private int DetermineSupplier(SdkSession sdk, Employee employee, ref string logDetermineSupplier)
        {
            if (!employee.FK_RELATION.HasValue)
            {
                logDetermineSupplier =
                    $"Bij werknemer {sdk.Sdk.GetRecordTag("R_PERSON", employee.FK_PERSON)} was geen relatie bekend tbv declaraties. De relatie is automatisch aangemaakt. Advies om de gegevens nog te controleren.";

                var wfCreateDeclarationRelation = new Guid("0c808ff2-5052-41a1-aed4-8f84b6f968ee");
                var wfResult = sdk.Sdk.ExecuteWorkflowEvent("R_EMPLOYEE", employee.Id, wfCreateDeclarationRelation, null);

                if (wfResult.HasError)
                {
                    logDetermineSupplier =
                        $"Bij werknemer {sdk.Sdk.GetRecordTag("R_PERSON", employee.FK_PERSON)} was geen relatie bekend tbv declaraties, maar het maken van een relatie is mislukt (oorzaak: {wfResult.GetResult()}). De default declaratie relatie is gebruikt.";

                    return GetDefaultDeclarationSupplierFromCrmSettings(sdk);
                }

                var createdRelation = sdk.Find<Employee>(x => x.Id == employee.Id).First().FK_RELATION;

                if(createdRelation == 0)
                {
                    logDetermineSupplier =
                        $"Bij werknemer {sdk.Sdk.GetRecordTag("R_PERSON", employee.FK_PERSON)} was geen relatie bekend tbv declaraties, maar het maken van een relatie is mislukt. De default declaratie relatie is gebruikt.";

                    return GetDefaultDeclarationSupplierFromCrmSettings(sdk);
                }

                employee.FK_RELATION = createdRelation;
            }

            if (!RelationIsCreditor(sdk, employee.FK_RELATION.Value))
            {
                if(!CreateCreditor(sdk, employee.FK_RELATION.Value))
                {
                    logDetermineSupplier =
                        $"Bij werknemer {sdk.Sdk.GetRecordTag("R_PERSON", employee.FK_PERSON)} is de relatie nog geen crediteur. De default declaratie relatie is gebruikt.";

                    return GetDefaultDeclarationSupplierFromCrmSettings(sdk);
                }
            }

            return employee.FK_RELATION.Value;
        }

        private bool CreateCreditor(SdkSession sdk, int createdRelation)
        {
            var daybookTypePurchase = 1;

            var rsPurchaseDaybook = sdk.GetRecordsetColumns("R_DAYBOOK", "PK_R_DAYBOOK",
                $"DAYBOOKTYPE = {(int)daybookTypePurchase}");

            if (rsPurchaseDaybook.RecordCount == 0)
            {
                return false;
            }

            rsPurchaseDaybook.MoveFirst();

            var result = sdk.Sdk.EventsAndActions.CRM.Actions.CreateCreditor(createdRelation,
                    (int)rsPurchaseDaybook.Fields["PK_R_DAYBOOK"].Value);

            if (result.HasError)
            {
                return false;
            }

            return true;
        }

        private int GetDefaultDeclarationSupplierFromCrmSettings(SdkSession sdk)
        {
            return sdk.GetRecordsetColumns("R_CRMSETTINGS", "FK_DEFAULTDECLARATIONRELATION", "")
                                       .DataTable.AsEnumerable().First().Field<int?>("FK_DEFAULTDECLARATIONRELATION") ?? 0;
        }

        private string ValidateOrder(SdkSession sdk, string orderId, string jobOrderId)
        {
            if (string.IsNullOrEmpty(orderId))
            {
                return string.Empty; //Geen order meegegeven
            }

            if (!int.TryParse(orderId, NumberStyles.Any, CultureInfo.CurrentCulture, out int iOrderId))
            {
                return $"Order '{orderId}' can't be parsed to a integer.";
            }

            if (iOrderId == 0)
            {
                return string.Empty; //Geen order meegegeven
            }

            var order = sdk.Find<Order>(x => x.Id == iOrderId).FirstOrDefault();

            if (order == null)
            {
                return $"No order found with order id {orderId}.";
            }

            if (order.FINANCIALDONE)
            {
                return $"Order {order.ORDERNUMBER} already financial done.";
            }

            if (!string.IsNullOrEmpty(jobOrderId))
            {
                //Check bon 
                if (!int.TryParse(jobOrderId, NumberStyles.Any, CultureInfo.CurrentCulture, out int iJoborderId))
                {
                    return $"Job order '{jobOrderId}' can't be parsed to a integer.";
                }

                if (iJoborderId != 0)
                {
                    var jobOrder = sdk.Find<Joborder>(x => x.Id == iJoborderId).FirstOrDefault();

                    if (jobOrder == null)
                    {
                        return $"No job order found with id {iJoborderId}.";
                    }

                    if(jobOrder.FK_ORDER != iOrderId)
                    {
                        return $"Job order belongs to order id {jobOrder.FK_ORDER}, not to order id {iOrderId}. This is not allowed.";
                    }
                }
            }


            return string.Empty;
        }

        private int GetJournalEntryNumber(SdkSession sdk, int journalEntryId)
        {
            return sdk.GetRecordsetColumns("R_JOURNALENTRY", "JOURNALENTRYNUMBER", 
                    $"PK_R_JOURNALENTRY = {journalEntryId}")
                .DataTable.AsEnumerable().First().Field<int>("JOURNALENTRYNUMBER");
        }

        private int GetCurrentEmployee(SdkSession sdk)
        {
            return sdk.GetRecordsetColumns("R_USER", "FK_EMPLOYEE",
                    $"PK_R_USER = {sdk.Sdk.GetUserInfo().CurrentUserId}")
                .DataTable.AsEnumerable().First().Field<int?>("FK_EMPLOYEE") ?? 0;
        }

        private bool RelationIsCreditor(SdkSession sdk, int relationId)
        {
            return sdk.GetRecordsetColumns("R_CREDITOR", "PK_R_CREDITOR",
                $"FK_RELATION = {relationId}").RecordCount > 0;
        }


        private List<Declaration> MapPurchaseInvoiceInfoToDeclarations(List<PurchaseInvoice> purchaseInvoices,
            List<PurchaseInvoiceDetailMisc> purchaseInvoiceDetailsMisc, List<Employee> relationsFromEmployees)
        {
            var result = new List<Declaration>();

            foreach (PurchaseInvoice purchaseInvoice in purchaseInvoices)
            {
                var purchaseInvoiceDetailsMiscThisInvoice =
                    purchaseInvoiceDetailsMisc.Where(x => x.FK_PURCHASEINVOICE == purchaseInvoice.Id).ToList();

                var declaration = new Declaration();

                var employee = relationsFromEmployees.FirstOrDefault(x => x.FK_RELATION == purchaseInvoice.FK_SUPPLIER);

                if (employee != null)
                {
                    declaration.FK_EMPLOYEE = employee.Id;
                }

                //Vaak is dit 1.0
                //De administratie splitst de BTW welis ivm verschillende BTW. In dat geval gewoon 1.0 aanhouden
                var quantity = purchaseInvoiceDetailsMiscThisInvoice.First().QUANTITY;

                declaration.FK_ORDER = purchaseInvoiceDetailsMiscThisInvoice.FirstOrDefault()?.FK_ORDER.ToString();
                declaration.FK_JOBORDER = purchaseInvoiceDetailsMiscThisInvoice.FirstOrDefault()?.FK_JOBORDER.ToString();
                declaration.DECLARATIONTYPE = purchaseInvoice.DECLARATIONTYPE;
                declaration.DESCRIPTION = purchaseInvoice.EXTERNALINVOICENUMBER;
                declaration.QUANTITY = quantity;
                declaration.AMOUNT = purchaseInvoiceDetailsMiscThisInvoice.Sum(x => x.TOTALAMOUNT.Value) / quantity;
                declaration.TOTALAMOUNT = purchaseInvoiceDetailsMiscThisInvoice.Sum(x => x.TOTALAMOUNT.Value);
                declaration.DATE = purchaseInvoice.INVOICEDATE;

                declaration.STATE = DetermineState(purchaseInvoice.FK_WORKFLOWSTATE.Value);

                declaration.PURCHASEINVOICEID = purchaseInvoice.Id;

                result.Add(declaration);
            }

            return result;
        }

        private string DetermineState(Guid purchaseInvoiceWorkflowstate)
        {
            var wfStateNew = new Guid("466c4d1e-21ce-47bb-a2c6-8b96c6ec0d23");
            var wfStateBlocked = new Guid("23dd10a0-492d-4c1b-aae6-a661160d3ffe");

            if(purchaseInvoiceWorkflowstate == wfStateNew)
            {
                return "Open"; 
            }
            else if (purchaseInvoiceWorkflowstate == wfStateBlocked)
            {
                return "Afgekeurd";
            }
            else
            {
                return "Goedgekeurd";
            }
        }
    }
}
