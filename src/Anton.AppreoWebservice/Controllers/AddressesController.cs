﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Anton.AppreoWebservice.Framework;
using Anton.AppreoWebservice.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Anton.AppreoWebservice.Controllers
{
    [Route("api")]
    [ApiController]

    public class AddressesController : ControllerBase
    {
        private readonly ISdkSessionProvider _sessionProvider;

        public AddressesController(ISdkSessionProvider sessionProvider)
        {
            _sessionProvider = sessionProvider;
        }

        // GET: api/Addresses
        [HttpGet("{company}/[controller]")]
        public ActionResult<PagedResponse<Address>> Get(string company, [FromQuery] PagingOptions pagingOptions,
            DateTime? dateChanged = null)
        {
            if (string.IsNullOrEmpty(company))
            {
                return BadRequest("No company specified.");
            }

            var sdk = _sessionProvider.Get(company);
            if (sdk == null)
            {
                return BadRequest($"Company '{company}' not found.");
            }

            var filter = (dateChanged.HasValue)
                ? $"DATECHANGED >= '{dateChanged.Value:yyyyMMdd HH:mm:ss}'"
                : "";

            var addresses = sdk.FindPaged<Address>(pagingOptions.Page + 1, pagingOptions.PageSize, filter).ToList();
            var totalResults =
                sdk.Sdk.CreateRecordset("R_ADDRESS", "PK_R_ADDRESS", filter, "").RecordCount;

            return new PagedResponse<Address>().ViewModelsToPagedResponse(addresses, totalResults, pagingOptions);
        }
    }
}
