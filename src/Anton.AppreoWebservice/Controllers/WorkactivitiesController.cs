﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Anton.AppreoWebservice.Framework;
using Anton.AppreoWebservice.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Anton.AppreoWebservice.Controllers
{
    [Route("api")]
    [ApiController]

    public class WorkactivitiesController : ControllerBase
    {
        private readonly ISdkSessionProvider _sessionProvider;

        public WorkactivitiesController(ISdkSessionProvider sessionProvider)
        {
            _sessionProvider = sessionProvider;
        }

        // GET: api/Workactivities
        [HttpGet("{company}/[controller]")]
        public ActionResult<PagedResponse<Workactivity>> Get(string company, [FromQuery] PagingOptions pagingOptions,
            DateTime? dateChanged = null)
        {
            if (string.IsNullOrEmpty(company))
            {
                return BadRequest("No company specified.");
            }

            var sdk = _sessionProvider.Get(company);
            if (sdk == null)
            {
                return BadRequest($"Company '{company}' not found.");
            }

            var filter = (dateChanged.HasValue)
                ? $"DATECHANGED >= '{dateChanged.Value:yyyyMMdd HH:mm:ss}'"
                : "";

            var workactivities = sdk.FindPaged<Workactivity>(pagingOptions.Page + 1, pagingOptions.PageSize, filter).ToList();
            var totalResults =
                sdk.Sdk.CreateRecordset("R_WORKACTIVITY", "PK_R_WORKACTIVITY", filter, "").RecordCount;

            return new PagedResponse<Workactivity>().ViewModelsToPagedResponse(workactivities, totalResults, pagingOptions);
        }
    }
}
