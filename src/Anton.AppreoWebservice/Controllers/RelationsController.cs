﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using Anton.AppreoWebservice.Framework;
using Anton.AppreoWebservice.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Ridder.Client.SDK.Extensions;

namespace Anton.AppreoWebservice.Controllers
{
    [Route("api")]
    [ApiController]

    public class RelationsController : ControllerBase
    {
        private readonly ISdkSessionProvider _sessionProvider;

        public RelationsController(ISdkSessionProvider sessionProvider)
        {
            _sessionProvider = sessionProvider;
        }

        // GET: api/Relations
        [HttpGet("{company}/[controller]")]
        public ActionResult<PagedResponse<Relation>> Get(string company, [FromQuery]PagingOptions pagingOptions, DateTime? dateChanged = null)
        {
            if (string.IsNullOrEmpty(company))
            {
                return BadRequest("No company specified.");
            }

            var sdk = _sessionProvider.Get(company);
            if (sdk == null)
            {
                return BadRequest($"Company '{company}' not found.");
            }

            var customerRelationTypeIds = GetCustomerRelationTypes(sdk);

            var filter = (dateChanged.HasValue)
                ? $"DATECHANGED >= '{dateChanged.Value:yyyyMMdd HH:mm:ss}' AND FK_RELATIONTYPE IN ({string.Join(",", customerRelationTypeIds)})"
                : $"FK_RELATIONTYPE IN ({string.Join(",", customerRelationTypeIds)})";

            var relations = sdk.FindPaged<Relation>(pagingOptions.Page + 1, pagingOptions.PageSize, filter).ToList();
            var totalResults =
                sdk.Sdk.CreateRecordset("R_RELATION", "PK_R_RELATION", filter, "").RecordCount;

            var pagedRelations = new PagedResponse<Relation>().ViewModelsToPagedResponse(relations, totalResults, pagingOptions);

            JoinContactNameIntoPageResult(sdk, pagedRelations);

            return pagedRelations;
        }

        // GET: api/RelationsV2
        [HttpGet("v2/{company}/[controller]")]
        public ActionResult<PagedResponse<Relation>> GetV2(string company, [FromQuery] PagingOptions pagingOptions, DateTime? dateChanged = null)
        {
            if (string.IsNullOrEmpty(company))
            {
                return BadRequest("No company specified.");
            }

            var sdk = _sessionProvider.Get(company);
            if (sdk == null)
            {
                return BadRequest($"Company '{company}' not found.");
            }

            var filter = (dateChanged.HasValue)
                ? $"DATECHANGED >= '{dateChanged.Value:yyyyMMdd HH:mm:ss}'"
                : "";

            var relations = sdk.FindPaged<Relation>(pagingOptions.Page + 1, pagingOptions.PageSize, filter).ToList();
            var totalResults =
                sdk.Sdk.CreateRecordset("R_RELATION", "PK_R_RELATION", filter, "").RecordCount;

            var pagedRelations = new PagedResponse<Relation>().ViewModelsToPagedResponse(relations, totalResults, pagingOptions);

            JoinContactNameIntoPageResult(sdk, pagedRelations);

            return pagedRelations;
        }

        private List<int> GetCustomerRelationTypes(SdkSession sdk)
        {
            //OPDRG & OPDRG/LEV & PARTOPDRG

            var customerRelationTypeIds = sdk.GetRecordsetColumns("R_RELATIONTYPE", "PK_R_RELATIONTYPE",
                    "CODE LIKE '%OPDRG%'")
                .DataTable.AsEnumerable()
                .Select(x => x.Field<int>("PK_R_RELATIONTYPE")).ToList();

            if(!customerRelationTypeIds.Any())
            {
                return new List<int>() { 0 };
            }

            return customerRelationTypeIds;
        }

        private void JoinContactNameIntoPageResult(SdkSession sdk, PagedResponse<Relation> pagedRelations)
        {
            var mainContactIds = pagedRelations.Results.Where(x => x.MAINCONTACT.HasValue).Select(x => x.MAINCONTACT)
                .ToList();

            if (!mainContactIds.Any())
            {
                return;
            }

            var contacts = sdk.Find<Contact>($"PK_R_CONTACT IN ({string.Join(",", mainContactIds)})").ToList();
            
            foreach (Relation relation in pagedRelations.Results.Where(x => x.MAINCONTACT.HasValue))
            {
                var contact = contacts.First(x => x.Id == relation.MAINCONTACT.Value);
                relation.NAMEMAINCONTACT = contact.NAME;
            }
        }
    }
}
