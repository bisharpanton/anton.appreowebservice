﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Anton.AppreoWebservice.Framework;
using Anton.AppreoWebservice.Models;
using Anton.AppreoWebservice.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Ridder.Client.SDK.Extensions;

namespace Anton.AppreoWebservice.Controllers
{
    [Route("api")]
    [ApiController]

    public class WorkplaceInspectionsController : ControllerBase
    {
        private readonly ISdkSessionProvider _sessionProvider;

        public WorkplaceInspectionsController(ISdkSessionProvider sessionProvider)
        {
            _sessionProvider = sessionProvider;
        }

        // GET: api/WorkplaceInspections
        [HttpGet("{company}/[controller]")]
        public ActionResult<PagedResponse<WorkplaceInspection>> Get(string company, [FromQuery] PagingOptions pagingOptions,
            DateTime? dateChanged = null)
        {
            if (string.IsNullOrEmpty(company))
            {
                return BadRequest("No company specified.");
            }

            var sdk = _sessionProvider.Get(company);
            if (sdk == null)
            {
                return BadRequest($"Company '{company}' not found.");
            }

            var filter = (dateChanged.HasValue)
                ? $"DATECHANGED >= '{dateChanged.Value:yyyyMMdd HH:mm:ss}'"
                : "";

            var workplaceInspections = sdk.FindPaged<WorkplaceInspection>(pagingOptions.Page + 1, pagingOptions.PageSize, filter).ToList();
            var totalResults =
                sdk.Sdk.CreateRecordset("C_VGINSPECTIE", "PK_C_VGINSPECTIE", filter, "").RecordCount;

            JoinDetailtablesIntoPageResult(sdk, workplaceInspections);

            return new PagedResponse<WorkplaceInspection>().ViewModelsToPagedResponse(workplaceInspections, totalResults, pagingOptions);
        }
        
        // POST: api/WorkplaceInspections
        [HttpPost("{company}/[controller]")]
        public ActionResult<WorkplaceInspection> Post(string company, [FromBody] WorkplaceInspection workplaceInspection)
        {
            if (string.IsNullOrEmpty(company))
            {
                return BadRequest("No company specified.");
            }

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var sdk = _sessionProvider.Get(company);
            if (sdk == null)
            {
                return NotFound($"Company '{company}' not found.");
            }

            var validateResult = ValidateWorkplaceInspection(sdk, workplaceInspection);

            if (!validateResult.Result)
            {
                return BadRequest(validateResult.Message);
            }

            workplaceInspection.FK_ORDER =  workplaceInspection.FK_ORDER == 0 ? null : workplaceInspection.FK_ORDER;
            workplaceInspection.FK_TODO = workplaceInspection.FK_TODO == 0 ? null : workplaceInspection.FK_TODO;

            //Initialize columns
            var wfStateNew = new Guid("3e84bfe8-85b4-46c8-b263-fbdd6c0fee93");
            workplaceInspection.FK_WORKFLOWSTATE = wfStateNew;

            try
            {
                sdk.Insert(workplaceInspection);
            }
            catch (Exception e)
            {
                return StatusCode(500, $"Aanmaken werkplekinspectie mislukt, oorzaak: {e.Message}");
            }

            var errorMessage = string.Empty;

            errorMessage = CreateWorkplaceinspectionEmployees(workplaceInspection, sdk);

            if (!string.IsNullOrEmpty(errorMessage))
                return StatusCode(500, errorMessage);

            errorMessage = CreateWorkplaceinspectionIntercompanyRelations(workplaceInspection, sdk);

            if (!string.IsNullOrEmpty(errorMessage))
                return StatusCode(500, errorMessage);

            errorMessage = CreateWorkplaceinspectionSubcontractors(workplaceInspection, sdk);

            if (!string.IsNullOrEmpty(errorMessage))
                return StatusCode(500, errorMessage);

            errorMessage = CreateWorkplaceinspectionObservationRounds(workplaceInspection, sdk);

            if (!string.IsNullOrEmpty(errorMessage))
                return StatusCode(500, errorMessage);

            errorMessage = CreateWorkplaceinspectionImprovements(workplaceInspection, sdk);

            if (!string.IsNullOrEmpty(errorMessage))
                return StatusCode(500, errorMessage);

            return Ok(workplaceInspection);
        }

        // PUT: api/WorkplaceInspections
        [HttpPut("{company}/[controller]")]
        public ActionResult<WorkplaceInspection> Put(string company, [FromBody] WorkplaceInspection workplaceInspection)
        {
            if (string.IsNullOrEmpty(company))
            {
                return BadRequest("No company specified.");
            }

            var sdk = _sessionProvider.Get(company);
            if (sdk == null)
            {
                return BadRequest($"Company '{company}' not found.");
            }

            var existingWorkplaceInspection = sdk.Find<WorkplaceInspection>($"PK_C_VGINSPECTIE = {workplaceInspection.Id}")
                .FirstOrDefault();

            if (existingWorkplaceInspection == null)
            {
                return BadRequest($"No workplace inspection found with id {workplaceInspection.Id}.");
            }

            if (existingWorkplaceInspection.ISDONE)
            {
                return BadRequest($"Workplace inspection is already done.");
            }

            var validateResult = ValidateWorkplaceInspection(sdk, workplaceInspection);

            if (!validateResult.Result)
            {
                return BadRequest(validateResult.Message);
            }

            workplaceInspection.FK_ORDER =
                workplaceInspection.FK_ORDER == 0 ? null : workplaceInspection.FK_ORDER;

            //Bij een wijzigings actie mogen deze velden niet gewijzigd worden.
            workplaceInspection.FK_WORKFLOWSTATE = existingWorkplaceInspection.FK_WORKFLOWSTATE;

            try
            {
                sdk.Update(workplaceInspection);
            }
            catch (Exception e)
            {
                return StatusCode(500, $"Bijwerken werkplekinspectie mislukt, oorzaak: {e.Message}");
            }

            var errorMessage = string.Empty;

            var existingEmployeeIds = sdk.Find<WorkplaceInspectionEmployee>($"FK_WORKPLACEINSPECTION == {workplaceInspection.Id}").Select(x => x.FK_EMPLOYEE.Value).ToList();
            var newEmployeeIds = workplaceInspection.EmployeeIds == null ? new List<int>() : workplaceInspection.EmployeeIds.ToList();

            if (existingEmployeeIds.Count != newEmployeeIds.Count || existingEmployeeIds.Except(newEmployeeIds).Any())
            {
                errorMessage = GlobalFunctions.DeleteRecordsOfDetailTable(sdk, "U_WORKPLACEINSPECTIONEMPLOYEE", $"FK_WORKPLACEINSPECTION = {workplaceInspection.Id}");

                if (!string.IsNullOrEmpty(errorMessage))
                    return StatusCode(500, errorMessage);

                errorMessage = CreateWorkplaceinspectionEmployees(workplaceInspection, sdk);

                if (!string.IsNullOrEmpty(errorMessage))
                    return StatusCode(500, errorMessage);
            }

            var existingIntercompanyRelationIds = sdk.Find<WorkplaceInspectionIntercompanyRelation>($"FK_WORKPLACEINSPECTION == {workplaceInspection.Id}").Select(x => x.FK_RELATION.Value).ToList();
            var newIntercompanyRelationIds = workplaceInspection.IntercompanyRelationIds == null ? new List<int>() : workplaceInspection.IntercompanyRelationIds.ToList();

            if (existingIntercompanyRelationIds.Count != newIntercompanyRelationIds.Count || existingIntercompanyRelationIds.Except(newIntercompanyRelationIds).Any())
            {
                errorMessage = GlobalFunctions.DeleteRecordsOfDetailTable(sdk, "U_WORKPLACEINSPECTIONINTERCOMPANYRELATION", $"FK_WORKPLACEINSPECTION = {workplaceInspection.Id}");

                if (!string.IsNullOrEmpty(errorMessage))
                    return StatusCode(500, errorMessage);

                errorMessage = CreateWorkplaceinspectionIntercompanyRelations(workplaceInspection, sdk);

                if (!string.IsNullOrEmpty(errorMessage))
                    return StatusCode(500, errorMessage);
            }

            var existingSubcontractorIds = sdk.Find<WorkplaceInspectionSubcontractor>($"FK_WORKPLACEINSPECTION == {workplaceInspection.Id}").Select(x => x.FK_RELATION.Value).ToList();
            var newSubcontractIds = workplaceInspection.SubcontractorIds == null ? new List<int>() : workplaceInspection.SubcontractorIds.ToList();

            if (existingSubcontractorIds.Count != newSubcontractIds.Count || existingSubcontractorIds.Except(newSubcontractIds).Any())
            {
                errorMessage = GlobalFunctions.DeleteRecordsOfDetailTable(sdk, "U_WORKPLACEINSPECTIONSUBCONTRACTOR", $"FK_WORKPLACEINSPECTION = {workplaceInspection.Id}");

                if (!string.IsNullOrEmpty(errorMessage))
                    return StatusCode(500, errorMessage);

                errorMessage = CreateWorkplaceinspectionSubcontractors(workplaceInspection, sdk);

                if (!string.IsNullOrEmpty(errorMessage))
                    return StatusCode(500, errorMessage);
            }

            var existingObservationRoundsIds = sdk.Find<WorkplaceInspectionObservationRound>($"FK_WORKPLACEINSPECTION == {workplaceInspection.Id}").Select(x => x.FK_OBSERVATIONROUND.Value).ToList();
            var newObservationRoundsIds = workplaceInspection.ObservationRoundIds == null ? new List<int>() : workplaceInspection.ObservationRoundIds.ToList();

            if (existingObservationRoundsIds.Count != newObservationRoundsIds.Count || existingObservationRoundsIds.Except(newObservationRoundsIds).Any())
            {
                errorMessage = GlobalFunctions.DeleteRecordsOfDetailTable(sdk, "U_WORKPLACEINSPECTIONOBSERVATIONROUND", $"FK_WORKPLACEINSPECTION = {workplaceInspection.Id}");

                if (!string.IsNullOrEmpty(errorMessage))
                    return StatusCode(500, errorMessage);

                errorMessage = CreateWorkplaceinspectionObservationRounds(workplaceInspection, sdk);

                if (!string.IsNullOrEmpty(errorMessage))
                    return StatusCode(500, errorMessage);
            }

            var existingImprovementIds = sdk.Find<WorkplaceInspectionImprovement>($"FK_WORKPLACEINSPECTION == {workplaceInspection.Id}").Select(x => x.FK_IMPROVEMENTS.Value).ToList();
            var newImprovementIds = workplaceInspection.ImprovementIds == null ? new List<int>() : workplaceInspection.ImprovementIds.ToList();

            if (existingImprovementIds.Count != newImprovementIds.Count || existingImprovementIds.Except(newImprovementIds).Any())
            {
                errorMessage = GlobalFunctions.DeleteRecordsOfDetailTable(sdk, "U_WORKPLACEINSPECTIONIMPROVEMENTS", $"FK_WORKPLACEINSPECTION = {workplaceInspection.Id}");

                if (!string.IsNullOrEmpty(errorMessage))
                    return StatusCode(500, errorMessage);

                errorMessage = CreateWorkplaceinspectionImprovements(workplaceInspection, sdk);

                if (!string.IsNullOrEmpty(errorMessage))
                    return StatusCode(500, errorMessage);
            }

            return Ok(workplaceInspection);
        }

        // DELETE api/WorkplaceInspections/5
        [HttpDelete("{company}/[controller]/{id}")]
        public ActionResult Delete(string company, int id)
        {
            if (string.IsNullOrEmpty(company))
            {
                return BadRequest("No company specified.");
            }

            var sdk = _sessionProvider.Get(company);
            if (sdk == null)
            {
                return BadRequest($"Company '{company}' not found.");
            }

            var rsWorkplaceInspection = sdk.GetRecordsetColumns("C_VGINSPECTIE", "",
                $"PK_C_VGINSPECTIE = {id}");

            if (rsWorkplaceInspection.RecordCount == 0)
            {
                return BadRequest($"No workplace inspection found with id {id}.");
            }

            rsWorkplaceInspection.UpdateWhenMoveRecord = false;
            rsWorkplaceInspection.MoveFirst();
            rsWorkplaceInspection.Delete();

            var updateResult = rsWorkplaceInspection.Update2();

            if (updateResult.Any(x => x.HasError))
            {
                return BadRequest(
                    $"Deleting workplace inspection failed, cause: {updateResult.First(x => x.HasError).GetResult()}");
            }

            return Ok();
        }

        private void JoinDetailtablesIntoPageResult(SdkSession sdk, List<WorkplaceInspection> workplaceInspections)
        {
            var workplaceInspectionsIds = workplaceInspections.Select(x => x.Id).ToList();

            var workplaceInspectionEmployees = sdk.Find<WorkplaceInspectionEmployee>($"FK_WORKPLACEINSPECTION IN ({string.Join(",", workplaceInspectionsIds)})").ToList();
            var workplaceInspectionIntercompanyRelations = sdk.Find<WorkplaceInspectionIntercompanyRelation>($"FK_WORKPLACEINSPECTION IN ({string.Join(",", workplaceInspectionsIds)})").ToList();
            var workplaceInspectionSubcontractors = sdk.Find<WorkplaceInspectionSubcontractor>($"FK_WORKPLACEINSPECTION IN ({string.Join(",", workplaceInspectionsIds)})").ToList();
            var workplaceInspectionObservationsRounds = sdk.Find<WorkplaceInspectionObservationRound>($"FK_WORKPLACEINSPECTION IN ({string.Join(",", workplaceInspectionsIds)})").ToList();
            var workplaceInspectionImprovements = sdk.Find<WorkplaceInspectionImprovement>($"FK_WORKPLACEINSPECTION IN ({string.Join(",", workplaceInspectionsIds)})").ToList();

            foreach (var workplaceInspection in workplaceInspections)
            {
                workplaceInspection.EmployeeIds = workplaceInspectionEmployees.Where(x => x.FK_WORKPLACEINSPECTION == workplaceInspection.Id && x.FK_EMPLOYEE.HasValue).Select(x => x.FK_EMPLOYEE.Value).ToList();
                workplaceInspection.IntercompanyRelationIds = workplaceInspectionIntercompanyRelations.Where(x => x.FK_WORKPLACEINSPECTION == workplaceInspection.Id && x.FK_RELATION.HasValue).Select(x => x.FK_RELATION.Value).ToList();
                workplaceInspection.SubcontractorIds = workplaceInspectionSubcontractors.Where(x => x.FK_WORKPLACEINSPECTION == workplaceInspection.Id && x.FK_RELATION.HasValue).Select(x => x.FK_RELATION.Value).ToList();
                workplaceInspection.ObservationRoundIds = workplaceInspectionObservationsRounds.Where(x => x.FK_WORKPLACEINSPECTION == workplaceInspection.Id && x.FK_OBSERVATIONROUND.HasValue).Select(x => x.FK_OBSERVATIONROUND.Value).ToList();
                workplaceInspection.ImprovementIds = workplaceInspectionImprovements.Where(x => x.FK_WORKPLACEINSPECTION == workplaceInspection.Id && x.FK_IMPROVEMENTS.HasValue).Select(x => x.FK_IMPROVEMENTS.Value).ToList();
            }
        }

        private string CreateWorkplaceinspectionEmployees(WorkplaceInspection workplaceInspection, SdkSession sdk)
        {
            if(workplaceInspection.EmployeeIds == null)
            {
                return string.Empty;
            }

            var employeeIds = workplaceInspection.EmployeeIds.Where(x => x != 0).ToList();

            if (!employeeIds.Any())
            {
                return string.Empty;
            }

            try
            {
                var workplaceInspectionEmployees = employeeIds
                    .Select(x => new WorkplaceInspectionEmployee()
                    {
                        FK_EMPLOYEE = x,
                        FK_WORKPLACEINSPECTION = workplaceInspection.Id,
                    }).ToList();

                sdk.InsertBulk(workplaceInspectionEmployees);
            }
            catch (Exception e)
            {
                return $"Aanmaken werkplekinspectie betrokken werknemers mislukt, oorzaak: {e.Message}";
            }

            return string.Empty;
        }

        private string CreateWorkplaceinspectionIntercompanyRelations(WorkplaceInspection workplaceInspection, SdkSession sdk)
        {
            if (workplaceInspection.IntercompanyRelationIds == null)
            {
                return string.Empty;
            }

            var relationIds = workplaceInspection.IntercompanyRelationIds.Where(x => x != 0).ToList();

            if (!relationIds.Any())
            {
                return string.Empty;
            }

            try
            {
                var newRecords = relationIds
                    .Where(x => x != 0)
                    .Select(x => new WorkplaceInspectionIntercompanyRelation()
                    {
                        FK_RELATION = x,
                        FK_WORKPLACEINSPECTION = workplaceInspection.Id,
                    }).ToList();

                sdk.InsertBulk(newRecords);
            }
            catch (Exception e)
            {
                return $"Aanmaken werkplekinspectie zusterbedrijven mislukt, oorzaak: {e.Message}";
            }

            return string.Empty;
        }

        private string CreateWorkplaceinspectionSubcontractors(WorkplaceInspection workplaceInspection, SdkSession sdk)
        {
            if (workplaceInspection.SubcontractorIds == null)
            {
                return string.Empty;
            }

            var subcontractorIds = workplaceInspection.SubcontractorIds.Where(x => x != 0).ToList();

            if (!subcontractorIds.Any())
            {
                return string.Empty;
            }

            try
            {
                var newRecords = subcontractorIds
                    .Where(x => x != 0)
                    .Select(x => new WorkplaceInspectionSubcontractor()
                    {
                        FK_RELATION = x,
                        FK_WORKPLACEINSPECTION = workplaceInspection.Id,
                    }).ToList();

                sdk.InsertBulk(newRecords);
            }
            catch (Exception e)
            {
                return $"Aanmaken werkplekinspectie onderaannemers mislukt, oorzaak: {e.Message}";
            }

            return string.Empty;
        }

        private string CreateWorkplaceinspectionObservationRounds(WorkplaceInspection workplaceInspection, SdkSession sdk)
        {
            if (workplaceInspection.ObservationRoundIds == null)
            {
                return string.Empty;
            }

            var observationRoundIds = workplaceInspection.ObservationRoundIds.Where(x => x != 0).ToList();

            if (!observationRoundIds.Any())
            {
                return string.Empty;
            }

            try
            {
                var newRecords = observationRoundIds
                    .Where(x => x != 0)
                    .Select(x => new WorkplaceInspectionObservationRound()
                    {
                        FK_OBSERVATIONROUND = x,
                        FK_WORKPLACEINSPECTION = workplaceInspection.Id,
                    }).ToList();

                sdk.InsertBulk(newRecords);
            }
            catch (Exception e)
            {
                return $"Aanmaken werkplekinspectie observatierondes mislukt, oorzaak: {e.Message}";
            }

            return string.Empty;
        }

        private string CreateWorkplaceinspectionImprovements(WorkplaceInspection workplaceInspection, SdkSession sdk)
        {
            if (workplaceInspection.ImprovementIds == null)
            {
                return string.Empty;
            }

            var improvementIds = workplaceInspection.ImprovementIds.Where(x => x != 0).ToList();

            if (!improvementIds.Any())
            {
                return string.Empty;
            }

            try
            {
                var newRecords = improvementIds
                    .Where(x => x != 0)
                    .Select(x => new WorkplaceInspectionImprovement()
                    {
                        FK_IMPROVEMENTS = x,
                        FK_WORKPLACEINSPECTION = workplaceInspection.Id,
                    }).ToList();

                sdk.InsertBulk(newRecords);
            }
            catch (Exception e)
            {
                return $"Aanmaken werkplekinspectie verbetervoorstellen mislukt, oorzaak: {e.Message}";
            }

            return string.Empty;
        }

        private ValidateResult ValidateWorkplaceInspection(SdkSession sdk, WorkplaceInspection workplaceInspection)
        {
            var employee = sdk.Find<Employee>(x => x.Id == workplaceInspection.FK_INSPECTER).FirstOrDefault();

            if (employee == null)
            {
                return new ValidateResult()
                {
                    Result = false, Message = $"Column inspecter: no employee found with id {workplaceInspection.FK_INSPECTER}"
                };
            }

            if ((workplaceInspection.FK_TODO ?? 0) != 0)
            {
                var todo = sdk.Find<Todo>(x => x.Id == workplaceInspection.FK_TODO).FirstOrDefault();

                if (todo == null)
                {
                    return new ValidateResult()
                    {
                        Result = false,
                        Message = $"No todo found with id {workplaceInspection.FK_TODO}"
                    };
                }
            }

            var resultChoiceColumns = CheckAllChoiceColumns(sdk, workplaceInspection);

            if (!resultChoiceColumns.Result)
            {
                return resultChoiceColumns;
            }

            return new ValidateResult() {Result = true, Message = ""};
        }

        private ValidateResult CheckAllChoiceColumns(SdkSession sdk, WorkplaceInspection workplaceInspection)
        {
            //Deze check zit ook op het savescript van de tabel C_VGINSPECTIE, alleen daar is deze dynamisch geprogrammeerd

            if (workplaceInspection.AFSPRAKENHYGIENE1 == VGInspectie.Onvoldoende &&
                string.IsNullOrEmpty(workplaceInspection.AFSPRAKENHYGIENE2))
            {
                return new ValidateResult()
                {
                    Result = false, Message = "At column 'AFSPRAKENHYGIENE' is the score insufficient, but there is no explanation."
                };
            }

            if (workplaceInspection.AFVALINZAMELING1 == VGInspectie.Onvoldoende &&
                string.IsNullOrEmpty(workplaceInspection.AFVALINZAMELING2))
            {
                return new ValidateResult()
                {
                    Result = false, Message = "At column 'AFVALINZAMELING' is the score insufficient, but there is no explanation."
                };
            }
            
            if (workplaceInspection.BRANDRISICOS1 == VGInspectie.Onvoldoende &&
                string.IsNullOrEmpty(workplaceInspection.BRANDRISICOS2))
            {
                return new ValidateResult()
                {
                    Result = false, Message = "At column 'BRANDRISICOS' is the score insufficient, but there is no explanation."
                };
            }

            if (workplaceInspection.FOLLOWUPINCIDENTEN1 == VGInspectie.Onvoldoende &&
                string.IsNullOrEmpty(workplaceInspection.FOLLOWUPINCIDENTEN2))
            {
                return new ValidateResult()
                {
                    Result = false, Message = "At column 'FOLLOWUPINCIDENTEN' is the score insufficient, but there is no explanation."
                };
            }

            if (workplaceInspection.GEBRUIKGEREEDSCHAP1 == VGInspectie.Onvoldoende &&
                string.IsNullOrEmpty(workplaceInspection.GEBRUIKGEREEDSCHAP2))
            {
                return new ValidateResult()
                {
                    Result = false, Message = "At column 'GEBRUIKGEREEDSCHAP' is the score insufficient, but there is no explanation."
                };
            }

            if (workplaceInspection.GEBRUIKHIJSMATERIAAL1 == VGInspectie.Onvoldoende &&
                string.IsNullOrEmpty(workplaceInspection.GEBRUIKHIJSMATERIAAL2))
            {
                return new ValidateResult()
                {
                    Result = false, Message = "At column 'GEBRUIKHIJSMATERIAAL' is the score insufficient, but there is no explanation."
                };
            }

            if (workplaceInspection.GEVAARLIJKESTOFFEN1 == VGInspectie.Onvoldoende &&
                string.IsNullOrEmpty(workplaceInspection.GEVAARLIJKESTOFFEN2))
            {
                return new ValidateResult()
                {
                    Result = false, Message = "At column 'GEVAARLIJKESTOFFEN' is the score insufficient, but there is no explanation."
                };
            }

            if (workplaceInspection.KEURINGMATERIEEL1 == VGInspectie.Onvoldoende &&
                string.IsNullOrEmpty(workplaceInspection.KEURINGMATERIEEL2))
            {
                return new ValidateResult()
                {
                    Result = false, Message = "At column 'KEURINGMATERIEEL' is the score insufficient, but there is no explanation."
                };
            }

            if (workplaceInspection.LMRA1 == VGInspectie.Onvoldoende &&
                string.IsNullOrEmpty(workplaceInspection.LMRA2))
            {
                return new ValidateResult()
                {
                    Result = false, Message = "At column 'LMRA' is the score insufficient, but there is no explanation."
                };
            }

            if (workplaceInspection.ORDERENNETHEID1 == VGInspectie.Onvoldoende &&
                string.IsNullOrEmpty(workplaceInspection.ORDERENNETHEID2))
            {
                return new ValidateResult()
                {
                    Result = false, Message = "At column 'ORDERENNETHEID' is the score insufficient, but there is no explanation."
                };
            }

            if (workplaceInspection.PBM1 == VGInspectie.Onvoldoende &&
                string.IsNullOrEmpty(workplaceInspection.PBM2))
            {
                return new ValidateResult()
                {
                    Result = false, Message = "At column 'PBM' is the score insufficient, but there is no explanation."
                };
            }

            if (workplaceInspection.VEILIGEWERKMETHODEN1 == VGInspectie.Onvoldoende &&
                string.IsNullOrEmpty(workplaceInspection.VEILIGEWERKMETHODEN2))
            {
                return new ValidateResult()
                {
                    Result = false, Message = "At column 'VEILIGEWERKMETHODEN' is the score insufficient, but there is no explanation."
                };
            }

            if (workplaceInspection.VGM1 == VGInspectie.Onvoldoende &&
                string.IsNullOrEmpty(workplaceInspection.VGM2))
            {
                return new ValidateResult()
                {
                    Result = false, Message = "At column 'VGM' is the score insufficient, but there is no explanation."
                };
            }

            if (workplaceInspection.VLUCHTROUTE1 == VGInspectie.Onvoldoende &&
                string.IsNullOrEmpty(workplaceInspection.VLUCHTROUTE2))
            {
                return new ValidateResult()
                {
                    Result = false, Message = "At column 'VLUCHTROUTE' is the score insufficient, but there is no explanation."
                };
            }

            if (workplaceInspection.BEKENDHEIDMETRISICOSWERKPLEK1 == VGInspectie.Onvoldoende &&
                string.IsNullOrEmpty(workplaceInspection.BEKENDHEIDMETRISICOSWERKPLEK2))
            {
                return new ValidateResult()
                {
                    Result = false, Message = "At column 'BEKENDHEIDMETRISICOSWERKPLEK' is the score insufficient, but there is no explanation."
                };
            }

            if (workplaceInspection.TRAUITGEVOERD1 == VGInspectie.Onvoldoende &&
                string.IsNullOrEmpty(workplaceInspection.TRAUITGEVOERD2))
            {
                return new ValidateResult()
                {
                    Result = false, Message = "At column 'TRAUITGEVOERD' is the score insufficient, but there is no explanation."
                };
            }
            
            return new ValidateResult() {Result = true, Message = ""};
        }
    }
}
