﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Anton.AppreoWebservice.Framework;
using Anton.AppreoWebservice.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Anton.AppreoWebservice.Controllers
{
    [Route("api")]
    [ApiController]

    public class AllEmployeesController : ControllerBase
    {
        private readonly ISdkSessionProvider _sessionProvider;

        public AllEmployeesController(ISdkSessionProvider sessionProvider)
        {
            _sessionProvider = sessionProvider;
        }

        // GET: api/AllEmployees
        [HttpGet("{company}/[controller]")]
        public ActionResult<PagedResponse<AllEmployee>> Get(string company, [FromQuery] PagingOptions pagingOptions)
        {
            if (string.IsNullOrEmpty(company))
            {
                return BadRequest("No company specified.");
            }

            if (company != "AIO")
            {
                return BadRequest("All employees only available in company 'AIO / Anton Internal Operations'.");
            }

            var sdk = _sessionProvider.Get(company);
            if (sdk == null)
            {
                return BadRequest($"Company '{company}' not found.");
            }

            var allEmployees = sdk.FindPaged<AllEmployee>(pagingOptions.Page + 1, pagingOptions.PageSize, "").ToList();
            var totalResults = sdk.Sdk.CreateRecordset("C_ALLEMPLOYEES", "PK_C_ALLEMPLOYEES", "", "").RecordCount;

            return new PagedResponse<AllEmployee>().ViewModelsToPagedResponse(allEmployees, totalResults, pagingOptions);
        }

        // GET: api/AllEmployees/GetPicture/{id}
        [HttpGet("{company}/[controller]/GetPicture/{uniqueEmployeeIndex}")]
        public ActionResult<string> GetPicture(string company, string uniqueEmployeeIndex)
        {
            if (string.IsNullOrEmpty(company))
            {
                return BadRequest("No company specified.");
            }

            if (company != "AIO")
            {
                return BadRequest("All employees only available in company 'AIO / Anton Internal Operations'.");
            }

            var sdk = _sessionProvider.Get(company);
            if (sdk == null)
            {
                return BadRequest($"Company '{company}' not found.");
            }

            var rsEmployee = sdk.GetRecordsetColumns("C_ALLEMPLOYEES", "PICTURE",
                $"UNIQUEEMPLOYEEINDEX = '{uniqueEmployeeIndex}'");

            if (rsEmployee.RecordCount == 0)
            {
                return BadRequest($"Employee with unique employee index '{uniqueEmployeeIndex}' not found.");
            }

            var picture = rsEmployee.DataTable.AsEnumerable().First().Field<byte[]>("PICTURE");

            if (picture == null)
            {
                return NotFound($"No picture found at employee with unique employee index {uniqueEmployeeIndex}");
            }

            return Convert.ToBase64String(picture);
        }
    }
}
