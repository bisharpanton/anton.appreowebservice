﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Anton.AppreoWebservice.Framework;
using Anton.AppreoWebservice.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Ridder.Client.SDK.Extensions;

namespace Anton.AppreoWebservice.Controllers
{
    [Route("api")]
    [ApiController]

    public class EmployeesController : ControllerBase
    {
        private readonly ISdkSessionProvider _sessionProvider;

        public EmployeesController(ISdkSessionProvider sessionProvider)
        {
            _sessionProvider = sessionProvider;
        }

        // GET: api/Employees
        [HttpGet("{company}/[controller]")]
        public ActionResult<PagedResponse<Employee>> Get(string company, [FromQuery]PagingOptions pagingOptions, DateTime? dateChanged = null)
        {
            if (string.IsNullOrEmpty(company))
            {
                return BadRequest("No company specified.");
            }

            var sdk = _sessionProvider.Get(company);
            if (sdk == null)
            {
                return BadRequest($"Company '{company}' not found.");
            }

            var filter = (dateChanged.HasValue)
                ? $"DATECHANGED >= '{dateChanged.Value:yyyyMMdd HH:mm:ss}'"
                : "";

            var employees = sdk.FindPaged<Employee>(pagingOptions.Page + 1, pagingOptions.PageSize, filter).ToList();
            var totalResults =
                sdk.Sdk.CreateRecordset("R_EMPLOYEE", "PK_R_EMPLOYEE", filter, "").RecordCount;


            FillEmployeePrivateData(sdk, employees);

            FillTimeSchedule(sdk, employees);

            var users = !employees.Any()
                ? new List<User>()
                : sdk.Find<User>($"FK_EMPLOYEE IN ({string.Join(",", employees.Select(x => x.Id))})").ToList();
            employees.ForEach(x => x.ACTIVEDIRECTORYACCOUNTLINKED = !string.IsNullOrEmpty(users.FirstOrDefault(y => y.FK_EMPLOYEE == x.Id)?.ADUSERLOGONNAME));


            return new PagedResponse<Employee>().ViewModelsToPagedResponse(employees, totalResults, pagingOptions);
        }

        // GET: api/Employees/GetPicture/{id}
        [HttpGet("{company}/[controller]/GetPicture/{id}")]
        public ActionResult<string> GetPicture(string company, string id)
        {
            if (string.IsNullOrEmpty(company))
            {
                return BadRequest("No company specified.");
            }

            var sdk = _sessionProvider.Get(company);
            if (sdk == null)
            {
                return BadRequest($"Company '{company}' not found.");
            }

            var employee = sdk.Find<Employee>($"PK_R_EMPLOYEE = {id}").FirstOrDefault();

            if (employee == null)
            {
                return BadRequest($"Employee with id {id} not found.");
            }

            var rsEmployeePrivateDate = sdk.GetRecordsetColumns("R_EMPLOYEEPRIVATEDATA", "PICTURE",
                $"FK_EMPLOYEE = {employee.Id}");

            var picture = rsEmployeePrivateDate.DataTable.AsEnumerable().First().Field<byte[]>("PICTURE");

            if (picture == null)
            {
                return NotFound($"No picture found at employee id {id}");
            }

            return Convert.ToBase64String(picture);
        }

        private void FillEmployeePrivateData(SdkSession sdk, List<Employee> employees)
        {
            var employeeIds = employees.Select(y => y.Id).ToList();

            var employeePrivateData = sdk.Find<EmployeePrivateData>(x => employeeIds.Contains(x.FK_EMPLOYEE))
                .ToList();

            foreach (Employee employee in employees)
            {
                var epd = employeePrivateData.FirstOrDefault(x => x.FK_EMPLOYEE == employee.Id);

                if (epd == null)
                {
                    continue;
                }

                employee.DATEOFBIRTH = epd.DATEOFBIRTH;
                employee.FK_ADDRESS = epd.FK_ADDRESS;
                employee.PERSONALMOBILEPHONENUMBER = epd.PHONE1;
                employee.PERSONALEMERGENCYNUMBER = epd.PHONE2;
                employee.PERSONALEMAIL = epd.EMAIL;
                employee.SPEEDDIAL = epd.SPEEDDIAL;

                employee.DATEPICTURECHANGED = epd.DATEPICTURECHANGED;
            }
        }

        private void FillTimeSchedule(SdkSession sdk, List<Employee> employees)
        {
            var employeeIds = employees.Select(y => y.Id).ToList();
            var timeTableOffsets = sdk
                .Find<TimeTableOffset>(x =>
                    employeeIds.Contains(x.FK_EMPLOYEE))
                .Where(x => x.STARTDATE.Date <= DateTime.Now.Date)
                .ToList();

            if(!timeTableOffsets.Any())
            {
                return;
            }

            var timeTableIds = timeTableOffsets.Select(y => y.FK_TIMETABLE).ToList();
            var timeTableWorkdays = sdk.Find<TimeTableWorkday>(x =>
                timeTableIds.Contains(x.FK_TIMETABLE)).ToList();

            var workdayIds = timeTableWorkdays.Select(y => y.FK_WORKDAY).ToList();
            var workdays = sdk.Find<Workday>(x => workdayIds.Contains(x.Id)).ToList();

            foreach (Employee employee in employees)
            {
                var timeTableOffset = timeTableOffsets
                    .Where(x => x.FK_EMPLOYEE == employee.Id)
                    .OrderByDescending(x => x.STARTDATE).FirstOrDefault();

                if (timeTableOffset == null)
                {
                    continue;
                }

                var timeTableWorkdaysThisEmployee =
                    timeTableWorkdays.Where(x => x.FK_TIMETABLE == timeTableOffset.FK_TIMETABLE).ToList();

                if(timeTableWorkdaysThisEmployee.Count == 1)
                {
                    var defaultHoursWorkday = workdays.First(x => x.Id == timeTableWorkdaysThisEmployee.First().FK_WORKDAY).DEFAULTHOURS;
                    var defaultHoursWorkdayInHours = TimeSpan.FromTicks(defaultHoursWorkday).TotalHours;

                    employee.TIMEMONDAY = defaultHoursWorkdayInHours;
                    employee.TIMETUESDAY = defaultHoursWorkdayInHours;
                    employee.TIMEWEDNESDAY = defaultHoursWorkdayInHours;
                    employee.TIMETHURSDAY = defaultHoursWorkdayInHours;
                    employee.TIMEFRIDAY = defaultHoursWorkdayInHours;
                    employee.TIMESATURDAY = defaultHoursWorkdayInHours;
                    employee.TIMESUNDAY = defaultHoursWorkdayInHours;
                }
                else if(timeTableWorkdaysThisEmployee.Count == 7)
                {
                    var workdayIdMonday = timeTableWorkdaysThisEmployee.First(x => x.SEQUENCENR == 1).FK_WORKDAY;
                    employee.TIMEMONDAY = TimeSpan.FromTicks(workdays.First(x => x.Id == workdayIdMonday).DEFAULTHOURS).TotalHours;

                    var workdayIdTuesday = timeTableWorkdaysThisEmployee.First(x => x.SEQUENCENR == 2).FK_WORKDAY;
                    employee.TIMETUESDAY = TimeSpan.FromTicks(workdays.First(x => x.Id == workdayIdTuesday).DEFAULTHOURS).TotalHours;

                    var workdayIdWednesday = timeTableWorkdaysThisEmployee.First(x => x.SEQUENCENR == 3).FK_WORKDAY;
                    employee.TIMEWEDNESDAY = TimeSpan.FromTicks(workdays.First(x => x.Id == workdayIdWednesday).DEFAULTHOURS).TotalHours;

                    var workdayIdThursday = timeTableWorkdaysThisEmployee.First(x => x.SEQUENCENR == 4).FK_WORKDAY;
                    employee.TIMETHURSDAY = TimeSpan.FromTicks(workdays.First(x => x.Id == workdayIdThursday).DEFAULTHOURS).TotalHours;

                    var workdayIdFriday = timeTableWorkdaysThisEmployee.First(x => x.SEQUENCENR == 5).FK_WORKDAY;
                    employee.TIMEFRIDAY = TimeSpan.FromTicks(workdays.First(x => x.Id == workdayIdFriday).DEFAULTHOURS).TotalHours;

                    var workdayIdSaturday = timeTableWorkdaysThisEmployee.First(x => x.SEQUENCENR == 6).FK_WORKDAY;
                    employee.TIMESATURDAY = TimeSpan.FromTicks(workdays.First(x => x.Id == workdayIdSaturday).DEFAULTHOURS).TotalHours;

                    var workdayIdSunday = timeTableWorkdaysThisEmployee.First(x => x.SEQUENCENR == 7).FK_WORKDAY;
                    employee.TIMESUNDAY = TimeSpan.FromTicks(workdays.First(x => x.Id == workdayIdSunday).DEFAULTHOURS).TotalHours;
                }
            }
        }
    }
}
