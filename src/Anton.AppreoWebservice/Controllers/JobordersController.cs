﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Anton.AppreoWebservice.Framework;
using Anton.AppreoWebservice.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Ridder.Client.SDK.Extensions;

namespace Anton.AppreoWebservice.Controllers
{
    [Route("api")]
    [ApiController]
    public class JobordersController : ControllerBase
    {
        private readonly ISdkSessionProvider _sessionProvider;

        public JobordersController(ISdkSessionProvider sessionProvider)
        {
            _sessionProvider = sessionProvider;
        }

        // GET: api/Joborders
        [HttpGet("{company}/[controller]")]
        public ActionResult<PagedResponse<Joborder>> Get(string company, [FromQuery] PagingOptions pagingOptions,
            DateTime? dateChanged = null)
        {
            if (string.IsNullOrEmpty(company))
            {
                return BadRequest("No company specified.");
            }

            var sdk = _sessionProvider.Get(company);
            if (sdk == null)
            {
                return BadRequest($"Company '{company}' not found.");
            }

            var filter = (dateChanged.HasValue)
                ? $"DATECHANGED >= '{dateChanged.Value:yyyyMMdd HH:mm:ss}'"
                : "";

            var joborders = sdk.FindPaged<Joborder>(pagingOptions.Page + 1, pagingOptions.PageSize, filter).ToList();
            var totalResults =
                sdk.Sdk.CreateRecordset("R_JOBORDER", "PK_R_JOBORDER", filter, "").RecordCount;

            //Service bonnen komen niet op de status 'Vrijgegeven', in Appreo worden default de vrijgegeven bonnen getoond.
            //Daarom tweaken we hier dat service bonnen met status nieuw de status vrijgegeven krijgt.
            joborders.Where(x => x.PLANSETTING == PlanSetting.Service && x.FK_WORKFLOWSTATE.Value.Equals(JobOrderWorkflowStates.New)).ToList()
                .ForEach(x => x.FK_WORKFLOWSTATE = JobOrderWorkflowStates.ReleasedForProduction);

            return new PagedResponse<Joborder>().ViewModelsToPagedResponse(joborders, totalResults, pagingOptions);
        }

        // PUT: api/Joborders/ReleaseForProduction/5
        [HttpPut("{company}/[controller]/ReleaseForProduction/{id}")]
        public ActionResult ReleaseForProduction(string company, int id)
        {
            if (string.IsNullOrEmpty(company))
            {
                return BadRequest("No company specified.");
            }

            var sdk = _sessionProvider.Get(company);
            if (sdk == null)
            {
                return BadRequest($"Company '{company}' not found.");
            }

            var joborder = sdk.Find<Joborder>($"PK_R_JOBORDER = {id}").FirstOrDefault();

            if (joborder == null)
            {
                return BadRequest($"No job order found with id {id}.");
            }

            if (joborder.FK_WORKFLOWSTATE == JobOrderWorkflowStates.ReleasedForProduction)
            {
                return Ok(joborder);
            }

            if (joborder.FK_WORKFLOWSTATE == JobOrderWorkflowStates.Done)
            {
                return BadRequest("Job order is already done.");
            }

            if (joborder.FK_WORKFLOWSTATE == JobOrderWorkflowStates.Planned)
            {
                return BadRequest("Job order has state 'Planned'. Release for production is not available.");
            }

            var wfReleaseForProduction = new Guid("feeed11e-1086-452c-a0c2-0ce8499f0e4c");
            var wfResult = sdk.Sdk.ExecuteWorkflowEvent("R_JOBORDER", joborder.Id, wfReleaseForProduction, null);

            if (wfResult.HasError)
            {
                return BadRequest($"Release for production failed; cause: {wfResult.GetResult()}.");
            }

            //We halen het object opnieuw op, om het veld Workflowstatus bij te werken
            var updatedJoborder = sdk.Find<Joborder>($"PK_R_JOBORDER = {id}").First();

            return Ok(updatedJoborder);
        }

        // PUT: api/Joborders/SetJoborderDone/5
        [HttpPut("{company}/[controller]/SetJoborderDone/{id}")]
        public ActionResult SetJoborderDone(string company, int id)
        {
            if (string.IsNullOrEmpty(company))
            {
                return BadRequest("No company specified.");
            }

            var sdk = _sessionProvider.Get(company);
            if (sdk == null)
            {
                return BadRequest($"Company '{company}' not found.");
            }

            var joborder = sdk.Find<Joborder>($"PK_R_JOBORDER = {id}").FirstOrDefault();

            if (joborder == null)
            {
                return BadRequest($"No job order found with id {id}.");
            }

            if (joborder.FK_WORKFLOWSTATE == JobOrderWorkflowStates.Done)
            {
                return Ok(joborder);
            }

            if (joborder.FK_WORKFLOWSTATE == JobOrderWorkflowStates.New && joborder.PLANSETTING == PlanSetting.Production)
            {
                return BadRequest("Job order has state 'New'. Set job order done is not available. Release for production first.");
            }

            if (joborder.FK_WORKFLOWSTATE == JobOrderWorkflowStates.Planned && joborder.PLANSETTING == PlanSetting.Production)
            {
                return BadRequest("Job order has state 'Planned'. Release for production is not available.");
            }

            if(joborder.PLANSETTING == PlanSetting.Production)
            {
                var alreadyQuantityProduced = sdk.GetRecordsetColumns("R_JOBORDERQUANTITYPRODUCED", "QUANTITYPRODUCED",
                    $"FK_JOBORDER = {joborder.Id}").DataTable.AsEnumerable().Sum(x => x.Field<double>("QUANTITYPRODUCED"));
                var quantityProduced = alreadyQuantityProduced > joborder.QUANTITYPLANNED.Value
                    ? 0
                    : joborder.QUANTITYPLANNED.Value - alreadyQuantityProduced;

                var resultSetProductionDone =
                    sdk.Sdk.EventsAndActions.Production.Actions.SetJobOrderProductionDoneWithQuantity(joborder.Id, quantityProduced);

                if (resultSetProductionDone.HasError)
                {
                    return BadRequest($"Set production done failed; cause: {resultSetProductionDone.GetResult()}.");
                }
            }
            else if(joborder.PLANSETTING == PlanSetting.Service)
            {
                //TODO: Een custom workflow maken die de action SetJobOrderProductionDoneAction uitvoert en de status naar Gereed zet indien Appreo service bonnen gereed wilt melden.
                /*
                var resultSetProductionDone =
                    sdk.Sdk.EventsAndActions.Production.Actions.SetJobOrderProductionDoneWithQuantity(joborder.Id, 1.0);

                if (resultSetProductionDone.HasError)
                {
                    return BadRequest($"Set production done failed; cause: {resultSetProductionDone.GetResult()}.");
                }*/
            }


            //We halen het object opnieuw op, om het veld Workflowstatus bij te werken
            var updatedJoborder = sdk.Find<Joborder>($"PK_R_JOBORDER = {id}").First();

            return Ok(updatedJoborder);
        }

        // PUT: api/Joborders/RevertProductionDone/5
        [HttpPut("{company}/[controller]/RevertProductionDone/{id}")]
        public ActionResult RevertProductionDone(string company, int id)
        {
            if (string.IsNullOrEmpty(company))
            {
                return BadRequest("No company specified.");
            }

            var sdk = _sessionProvider.Get(company);
            if (sdk == null)
            {
                return BadRequest($"Company '{company}' not found.");
            }

            var joborder = sdk.Find<Joborder>($"PK_R_JOBORDER = {id}").FirstOrDefault();

            if (joborder == null)
            {
                return BadRequest($"No job order found with id {id}.");
            }

            if (joborder.FK_WORKFLOWSTATE == JobOrderWorkflowStates.ReleasedForProduction)
            {
                return Ok(joborder);
            }

            if (joborder.FK_WORKFLOWSTATE == JobOrderWorkflowStates.New)
            {
                return BadRequest("Job order has state 'New'. Revert production done is not available.");
            }

            if (joborder.FK_WORKFLOWSTATE == JobOrderWorkflowStates.Planned)
            {
                return BadRequest("Job order has state 'Planned'. Revert production done is not available.");
            }

            var wfRevertProductionDone = new Guid("9a950996-dc03-4f79-b847-d4982a296430");
            var wfResult = sdk.Sdk.ExecuteWorkflowEvent("R_JOBORDER", joborder.Id, wfRevertProductionDone, null);

            if (wfResult.HasError)
            {
                return BadRequest($"Revert production done failed; cause: {wfResult.GetResult()}.");
            }

            //We halen het object opnieuw op, om het veld Workflowstatus bij te werken
            var updatedJoborder = sdk.Find<Joborder>($"PK_R_JOBORDER = {id}").First();

            return Ok(updatedJoborder);
        }

        // PUT: api/Joborders/UpdatePlanInfo/5
        [HttpPut("{company}/[controller]/UpdatePlanInfo/{id}")]
        public ActionResult UpdatePlanInfo(string company, int id, DateTime? plannedStartDate, DateTime? plannedEndDate)
        {
            if (string.IsNullOrEmpty(company))
            {
                return BadRequest("No company specified.");
            }

            var sdk = _sessionProvider.Get(company);
            if (sdk == null)
            {
                return BadRequest($"Company '{company}' not found.");
            }

            if (!plannedStartDate.HasValue || !plannedEndDate.HasValue)
            {
                return BadRequest($"No planned start date or planned end date found.");
            }

            if(plannedEndDate.Value.Date < plannedStartDate.Value.Date)
            {
                return BadRequest("Geplande startdatum mag niet voor geplande einddatum liggen.");
            }

            var joborder = sdk.Find<Joborder>($"PK_R_JOBORDER = {id}").FirstOrDefault();

            if (joborder == null)
            {
                return BadRequest($"No job order found with id {id}.");
            }

            //Ik wil geen andere kolommen wijzigen, daarom voor de zekerheid data updaten via recordsset
            var rsJoborder = sdk.GetRecordsetColumns("R_JOBORDER", "PLANNEDPRODUCTIONSTARTDATE, PLANNEDPRODUCTIONENDDATE, ISPLANNEDBYAPPREO",
                $"PK_R_JOBORDER = {id}");

            rsJoborder.UseDataChanges = false;

            rsJoborder.MoveFirst();

            rsJoborder.SetFieldValue("PLANNEDPRODUCTIONSTARTDATE", plannedStartDate);
            rsJoborder.SetFieldValue("PLANNEDPRODUCTIONENDDATE", plannedEndDate);
            
            if(!(bool)rsJoborder.GetField("ISPLANNEDBYAPPREO").Value)
            {
                rsJoborder.SetFieldValue("ISPLANNEDBYAPPREO", true);
            }

            var updateResult = rsJoborder.Update2();

            if (updateResult != null && updateResult.Any(x => x.HasError))
            {
                return StatusCode(500, $"Bijwerken planinfo bon is mislukt, oorzaak: {updateResult.First(x => x.HasError).GetResult()}");
            }

            joborder.PLANNEDPRODUCTIONSTARTDATE = plannedStartDate;
            joborder.PLANNEDPRODUCTIONENDDATE = plannedEndDate;

            return Ok(joborder);
        }
    }
}
