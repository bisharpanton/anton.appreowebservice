﻿using System;
using System.Linq;
using Anton.AppreoWebservice.Framework;
using Anton.AppreoWebservice.Models;
using Anton.AppreoWebservice.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Ridder.Client.SDK.Extensions;

namespace Anton.AppreoWebservice.Controllers
{
    [Route("api")]
    [ApiController]
    public class ImprovementsController : ControllerBase
    {
        private readonly ISdkSessionProvider _sessionProvider;

        public ImprovementsController(ISdkSessionProvider sessionProvider)
        {
            _sessionProvider = sessionProvider;
        }

        // GET: api/improvements
        [HttpGet("{company}/[controller]")]
        public ActionResult<PagedResponse<Improvements>> Get(string company, [FromQuery] PagingOptions pagingOptions,
            DateTime? dateChanged = null)
        {
            if (string.IsNullOrEmpty(company))
            {
                return BadRequest("No company specified.");
            }

            var sdk = _sessionProvider.Get(company);
            if (sdk == null)
            {
                return BadRequest($"Company '{company}' not found.");
            }

            var filter = (dateChanged.HasValue)
                ? $"DATECHANGED >= '{dateChanged.Value:yyyyMMdd HH:mm:ss}'"
                : "";

            var improvements = sdk.FindPaged<Improvements>(pagingOptions.Page + 1, pagingOptions.PageSize, filter).ToList();
            var totalResults =
                sdk.Sdk.CreateRecordset("U_IMPROVEMENTS", "PK_U_IMPROVEMENTS", filter, "").RecordCount;

            return new PagedResponse<Improvements>().ViewModelsToPagedResponse(improvements, totalResults, pagingOptions);
        }

        // POST: api/improvements
        [HttpPost("{company}/[controller]")]
        public ActionResult<Improvements> Post(string company, [FromBody] Improvements improvements)
        {
            if(string.IsNullOrEmpty(company))
            {
                return BadRequest("No company specified.");
            }

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var sdk = _sessionProvider.Get(company);
            if (sdk == null)
            {
                return BadRequest($"Company '{company}' not found.");
            }

            var validateResult = ValidateImprovements(sdk, improvements);

            if (!validateResult.Result)
            {
                return BadRequest(validateResult.Message);
            }

            improvements.FK_EMPLOYEE = improvements.FK_EMPLOYEE == 0 ? null : improvements.FK_EMPLOYEE;
            improvements.FK_KIND = improvements.FK_KIND == 0 ? null : improvements.FK_KIND;

            sdk.Insert(improvements);

            return Ok(improvements);
        }

        // PUT: api/Improvements
        [HttpPut("{company}/[controller]")]
        public ActionResult<Improvements> Put(string company, [FromBody] Improvements improvements)
        {
            if (string.IsNullOrEmpty(company))
            {
                return BadRequest("No company specified.");
            }

            var sdk = _sessionProvider.Get(company);
            if (sdk == null)
            {
                return BadRequest($"Company '{company}' not found.");
            }

            var existingImprovements = sdk.Find<Improvements>($"PK_U_IMPROVEMENTS = {improvements.Id}")
                .FirstOrDefault();

            if (existingImprovements == null)
            {
                return BadRequest($"No improvement found with id {improvements.Id}");
            }

            var validateResult = ValidateImprovements(sdk, improvements);

            if (!validateResult.Result)
            {
                return BadRequest(validateResult.Message);
            }

            improvements.FK_EMPLOYEE = improvements.FK_EMPLOYEE == 0 ? null : improvements.FK_EMPLOYEE;
            improvements.FK_KIND = improvements.FK_KIND == 0 ? null : improvements.FK_KIND;

            sdk.Update(improvements);

            return Ok(improvements);
        }

        // DELETE api/Improvements/5
        [HttpDelete("{company}/[controller]/{id}")]
        public ActionResult Delete(string company, int id)
        {
            if (string.IsNullOrEmpty(company))
            {
                return BadRequest("No company specified.");
            }

            var sdk = _sessionProvider.Get(company);
            if (sdk == null)
            {
                return BadRequest($"Company '{company}' not found.");
            }

            var rsImprovements = sdk.GetRecordsetColumns("U_IMPROVEMENTS", "",
                $"PK_U_IMPROVEMENTS = {id}");

            if (rsImprovements.RecordCount == 0)
            {
                return BadRequest($"No improvement found with id {id}.");
            }

            rsImprovements.UpdateWhenMoveRecord = false;
            rsImprovements.MoveFirst();
            rsImprovements.Delete();

            var updateResult = rsImprovements.Update2();

            if (updateResult.Any(x => x.HasError))
            {
                return BadRequest(
                    $"Deleting improvement failed, cause: {updateResult.First(x => x.HasError).GetResult()}");
            }

            return Ok();
        }

        private ValidateResult ValidateImprovements(SdkSession sdk, Improvements improvements)
        {
            var employee = sdk.Find<Employee>(x => x.Id == improvements.FK_EMPLOYEE).FirstOrDefault();

            if((improvements.FK_EMPLOYEE ?? 0) != 0 && employee == null)
            {
                return new ValidateResult()
                {
                    Result = false,
                    Message = $"No employee found with id {improvements.FK_EMPLOYEE}"
                };
            }

            var kind = sdk.Find<SafetyReportKind>(x => x.Id == improvements.FK_KIND).FirstOrDefault();
            if ((improvements.FK_KIND ?? 0) != 0 && kind == null)
            {
                return new ValidateResult()
                {
                    Result = false, Message = $"No kind found with id {improvements.FK_KIND}"
                };
            }

            return new ValidateResult() { Result = true, Message = "" };
        }
    }
}
