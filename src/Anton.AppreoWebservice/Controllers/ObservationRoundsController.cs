﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Anton.AppreoWebservice.Framework;
using Anton.AppreoWebservice.Models;
using Anton.AppreoWebservice.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Ridder.Client.SDK.Extensions;

namespace Anton.AppreoWebservice.Controllers
{
    [Route("api")]
    [ApiController]

    public class ObservationRoundsController : ControllerBase
    {
        private readonly ISdkSessionProvider _sessionProvider;

        public ObservationRoundsController(ISdkSessionProvider sessionProvider)
        {
            _sessionProvider = sessionProvider;
        }

        // GET: api/ObservationRounds
        [HttpGet("{company}/[controller]")]
        public ActionResult<PagedResponse<ObservationRound>> Get(string company, [FromQuery] PagingOptions pagingOptions,
            DateTime? dateChanged = null)
        {
            if (string.IsNullOrEmpty(company))
            {
                return BadRequest("No company specified.");
            }

            var sdk = _sessionProvider.Get(company);
            if (sdk == null)
            {
                return BadRequest($"Company '{company}' not found.");
            }

            var filter = (dateChanged.HasValue)
                ? $"DATECHANGED >= '{dateChanged.Value:yyyyMMdd HH:mm:ss}'"
                : "";

            var observationRounds = sdk.FindPaged<ObservationRound>(pagingOptions.Page + 1, pagingOptions.PageSize, filter).ToList();
            var totalResults =
                sdk.Sdk.CreateRecordset("C_VGINSPECTIEGEDRAGSPUNTEN", "PK_C_VGINSPECTIEGEDRAGSPUNTEN", filter, "").RecordCount;
            
            return new PagedResponse<ObservationRound>().ViewModelsToPagedResponse(observationRounds, totalResults, pagingOptions);
        }

        // GET: api/ObservationRoundsTopics
        [HttpGet("{company}/[controller]/GetObservationRoundTopics")]
        public ActionResult<PagedResponse<ObservationRoundTopic>> GetObservationRoundTopics(string company, [FromQuery] PagingOptions pagingOptions,
            DateTime? dateChanged = null)
        {
            if (string.IsNullOrEmpty(company))
            {
                return BadRequest("No company specified.");
            }

            var sdk = _sessionProvider.Get(company);
            if (sdk == null)
            {
                return BadRequest($"Company '{company}' not found.");
            }

            var filter = (dateChanged.HasValue)
                ? $"DATECHANGED >= '{dateChanged.Value:yyyyMMdd HH:mm:ss}'"
                : "";

            var observationRoundTopics = sdk.FindPaged<ObservationRoundTopic>(pagingOptions.Page + 1, pagingOptions.PageSize, filter).ToList();
            var totalResults =
                sdk.Sdk.CreateRecordset("C_GEDRAGSONDERWERPEN", "PK_C_GEDRAGSONDERWERPEN", filter, "").RecordCount;

            return new PagedResponse<ObservationRoundTopic>().ViewModelsToPagedResponse(observationRoundTopics, totalResults, pagingOptions);
        }

        // POST: api/ObservationRounds
        [HttpPost("{company}/[controller]")]
        public ActionResult<ObservationRound> Post(string company, [FromBody] ObservationRound observationRound)
        {
            if (string.IsNullOrEmpty(company))
            {
                return BadRequest("No company specified.");
            }

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var sdk = _sessionProvider.Get(company);
            if (sdk == null)
            {
                return NotFound($"Company '{company}' not found.");
            }

            var validateResult = ValidateObservationRound(sdk, observationRound);

            if (!validateResult.Result)
            {
                return BadRequest(validateResult.Message);
            }

            observationRound.FK_ORDER = observationRound.FK_ORDER == 0 ? null : observationRound.FK_ORDER;
            observationRound.FK_GEDRAGSONDERWERP = observationRound.FK_GEDRAGSONDERWERP == 0 ? null : observationRound.FK_GEDRAGSONDERWERP;

            observationRound.STATE = GedragspuntStatus.Openstaand;

            sdk.Insert(observationRound);

            return Ok(observationRound);
        }

        // PUT: api/ObservationRounds
        [HttpPut("{company}/[controller]")]
        public ActionResult<ObservationRound> Put(string company, [FromBody] ObservationRound observationRound)
        {
            if (string.IsNullOrEmpty(company))
            {
                return BadRequest("No company specified.");
            }

            var sdk = _sessionProvider.Get(company);
            if (sdk == null)
            {
                return BadRequest($"Company '{company}' not found.");
            }

            var existingObservationRound = sdk.Find<ObservationRound>($"PK_C_VGINSPECTIEGEDRAGSPUNTEN = {observationRound.Id}")
                .FirstOrDefault();

            if (existingObservationRound == null)
            {
                return BadRequest($"No observation round found with id {observationRound.Id}.");
            }

            var validateResult = ValidateObservationRound(sdk, observationRound);

            if (!validateResult.Result)
            {
                return BadRequest(validateResult.Message);
            }

            observationRound.FK_ORDER = observationRound.FK_ORDER == 0 ? null : observationRound.FK_ORDER;
            observationRound.FK_GEDRAGSONDERWERP = observationRound.FK_GEDRAGSONDERWERP == 0 ? null : observationRound.FK_GEDRAGSONDERWERP;


            sdk.Update(observationRound);

            return Ok(observationRound);
        }

        // DELETE api/ObservationRounds/5
        [HttpDelete("{company}/[controller]/{id}")]
        public ActionResult Delete(string company, int id)
        {
            if (string.IsNullOrEmpty(company))
            {
                return BadRequest("No company specified.");
            }

            var sdk = _sessionProvider.Get(company);
            if (sdk == null)
            {
                return BadRequest($"Company '{company}' not found.");
            }

            var rsObservationRounds = sdk.GetRecordsetColumns("C_VGINSPECTIEGEDRAGSPUNTEN", "",
                $"PK_C_VGINSPECTIEGEDRAGSPUNTEN = {id}");

            if (rsObservationRounds.RecordCount == 0)
            {
                return BadRequest($"No observation round found with id {id}.");
            }

            rsObservationRounds.UpdateWhenMoveRecord = false;
            rsObservationRounds.MoveFirst();
            rsObservationRounds.Delete();

            var updateResult = rsObservationRounds.Update2();

            if (updateResult.Any(x => x.HasError))
            {
                return BadRequest(
                    $"Deleting observation round failed, cause: {updateResult.First(x => x.HasError).GetResult()}");
            }

            return Ok();
        }

        private ValidateResult ValidateObservationRound(SdkSession sdk, ObservationRound observationRound)
        {
            var employee = sdk.Find<Employee>(x => x.Id == observationRound.FK_EMPLOYEE).FirstOrDefault();

            if (employee == null)
            {
                return new ValidateResult()
                {
                    Result = false, Message = $"No employee found with id {observationRound.FK_EMPLOYEE}"
                };
            }

            if((observationRound.FK_TODO ?? 0) != 0)
            {
                var todo = sdk.Find<Todo>(x => x.Id == observationRound.FK_TODO).FirstOrDefault();

                if(todo == null)
                {
                    return new ValidateResult()
                    {
                        Result = false,
                        Message = $"No todo found with id {observationRound.FK_TODO}"
                    };
                }
            }

            return new ValidateResult() {Result = true, Message = ""};
        }
    }
}
