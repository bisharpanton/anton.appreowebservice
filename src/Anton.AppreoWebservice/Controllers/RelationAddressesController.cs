﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Anton.AppreoWebservice.Framework;
using Anton.AppreoWebservice.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Anton.AppreoWebservice.Controllers
{
    [Route("api")]
    [ApiController]

    public class RelationAddressesController : ControllerBase
    {
        private readonly ISdkSessionProvider _sessionProvider;

        public RelationAddressesController(ISdkSessionProvider sessionProvider)
        {
            _sessionProvider = sessionProvider;
        }

        // GET: api/RelationAddresses
        [HttpGet("{company}/[controller]")]
        public ActionResult<PagedResponse<RelationDeliveryAddress>> Get(string company, [FromQuery] PagingOptions pagingOptions,
            DateTime? dateChanged = null)
        {
            if (string.IsNullOrEmpty(company))
            {
                return BadRequest("No company specified.");
            }

            var sdk = _sessionProvider.Get(company);
            if (sdk == null)
            {
                return BadRequest($"Company '{company}' not found.");
            }

            var filter = (dateChanged.HasValue)
                ? $"DATECHANGED >= '{dateChanged.Value:yyyyMMdd HH:mm:ss}'"
                : "";

            var relationAddresses = sdk.FindPaged<RelationDeliveryAddress>(pagingOptions.Page + 1, pagingOptions.PageSize, filter).ToList();
            var totalResults =
                sdk.Sdk.CreateRecordset("R_RELATIONDELIVERYADDRESS", "PK_R_RELATIONDELIVERYADDRESS", filter, "")
                    .RecordCount;

            return new PagedResponse<RelationDeliveryAddress>().ViewModelsToPagedResponse(relationAddresses, totalResults, pagingOptions);
        }
    }
}
