﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Anton.AppreoWebservice.Framework;
using Anton.AppreoWebservice.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Anton.AppreoWebservice.Controllers
{
    [Route("api")]
    [ApiController]

    public class OrdersController : ControllerBase
    {
        private readonly ISdkSessionProvider _sessionProvider;

        public OrdersController(ISdkSessionProvider sessionProvider)
        {
            _sessionProvider = sessionProvider;
        }

        // GET: api/Orders
        [HttpGet("{company}/[controller]")]
        public ActionResult<PagedResponse<Order>> Get(string company, [FromQuery] PagingOptions pagingOptions,
            DateTime? dateChanged = null)
        {
            if (string.IsNullOrEmpty(company))
            {
                return BadRequest("No company specified.");
            }

            var sdk = _sessionProvider.Get(company);
            if (sdk == null)
            {
                return BadRequest($"Company '{company}' not found.");
            }

            var filter = (dateChanged.HasValue)
                ? $"DATECHANGED >= '{dateChanged.Value:yyyyMMdd HH:mm:ss}'"
                : "";

            var orders = sdk.FindPaged<Order>(pagingOptions.Page + 1, pagingOptions.PageSize, filter).ToList();
            var totalResults =
                sdk.Sdk.CreateRecordset("R_ORDER", "PK_R_ORDER", filter, "").RecordCount;

            return new PagedResponse<Order>().ViewModelsToPagedResponse(orders, totalResults, pagingOptions);
        }
    }
}
