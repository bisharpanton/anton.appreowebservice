﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Anton.AppreoWebservice.Framework;
using Anton.AppreoWebservice.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Anton.AppreoWebservice.Controllers
{
    [Route("api")]
    [ApiController]
    public class OverTimeCodesController : ControllerBase
    {
        private readonly ISdkSessionProvider _sessionProvider;

        public OverTimeCodesController(ISdkSessionProvider sessionProvider)
        {
            _sessionProvider = sessionProvider;
        }

        // GET: api/OverTimeCodes
        [HttpGet("{company}/[controller]")]
        public ActionResult<PagedResponse<OverTimeCode>> Get(string company, [FromQuery] PagingOptions pagingOptions,
            DateTime? dateChanged = null)
        {
            if (string.IsNullOrEmpty(company))
            {
                return BadRequest("No company specified.");
            }

            var sdk = _sessionProvider.Get(company);
            if (sdk == null)
            {
                return BadRequest($"Company '{company}' not found.");
            }

            var filter = (dateChanged.HasValue)
                ? $"DATECHANGED >= '{dateChanged.Value:yyyyMMdd HH:mm:ss}'"
                : "";

            var overTimeCodes = sdk.FindPaged<OverTimeCode>(pagingOptions.Page + 1, pagingOptions.PageSize, filter).ToList();
            var totalResults =
                sdk.Sdk.CreateRecordset("R_TIMECODE", "PK_R_TIMECODE", filter, "").RecordCount;

            return new PagedResponse<OverTimeCode>().ViewModelsToPagedResponse(overTimeCodes, totalResults, pagingOptions);
        }
    }
}
