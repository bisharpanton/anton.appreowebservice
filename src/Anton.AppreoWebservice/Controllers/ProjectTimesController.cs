﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using ADODB;
using Anton.AppreoWebservice.Framework;
using Anton.AppreoWebservice.Models;
using Anton.AppreoWebservice.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Ridder.Client.SDK;
using Ridder.Client.SDK.Extensions;

namespace Anton.AppreoWebservice.Controllers
{
    [Route("api")]
    [ApiController]

    public class ProjectTimesController : ControllerBase
    {
        private readonly ISdkSessionProvider _sessionProvider;

        public ProjectTimesController(ISdkSessionProvider sessionProvider)
        {
            _sessionProvider = sessionProvider;
        }

        // GET: api/ProjectTimes
        [HttpGet("{company}/[controller]")]
        public ActionResult<PagedResponse<ProjectTime>> Get(string company, [FromQuery] PagingOptions pagingOptions,
            DateTime? dateChanged = null)
        {
            if (string.IsNullOrEmpty(company))
            {
                return BadRequest("No company specified.");
            }

            var sdk = _sessionProvider.Get(company);
            if (sdk == null)
            {
                return BadRequest($"Company '{company}' not found.");
            }

            var filter = (dateChanged.HasValue)
                ? $"DATECHANGED >= '{dateChanged.Value:yyyyMMdd HH:mm:ss}'"
                : "";

            var projectTimes = sdk.FindPaged<ProjectTime>(pagingOptions.Page + 1, pagingOptions.PageSize, filter).ToList();
            var totalResults =
                sdk.Sdk.CreateRecordset("R_PROJECTTIME", "PK_R_PROJECTTIME", filter, "").RecordCount;
            
            return new PagedResponse<ProjectTime>().ViewModelsToPagedResponse(projectTimes, totalResults, pagingOptions);
        }

        // POST: api/ProjectTimes
        [HttpPost("{company}/[controller]")]
        public ActionResult<ProjectTime> Post(string company, [FromBody] CreateProjectTime createProjectTime)
        {
            if (string.IsNullOrEmpty(company))
            {
                return BadRequest("No company specified.");
            }

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (createProjectTime.Employee == 0 || createProjectTime.Workactivity == 0)
            {
                return BadRequest("Employee and workactivity are required.");
            }

            var sdk = _sessionProvider.Get(company);
            if (sdk == null)
            {
                return NotFound($"Company '{company}' not found.");
            }

            var message = ValidateCreateProjectTime(sdk, createProjectTime);
            if (!string.IsNullOrEmpty(message))
            {
                return BadRequest(message);
            }

            var workactivity = sdk.Find<Workactivity>(x => x.Id == createProjectTime.Workactivity).First();

            if (workactivity.WORKACTIVITYTYPE == WorkActivityType.Direct_hours &&
                (!createProjectTime.Order.HasValue || !createProjectTime.Joborder.HasValue))
            {
                return BadRequest(
                    $"Workactivity {workactivity.DESCRIPTION} is a direct workactivity. Order and job order are required.");
            }

            var rsProjectTime = sdk.GetRecordsetColumns("R_PROJECTTIME", "", "PK_R_PROJECTTIME = -1");
            rsProjectTime.UseDataChanges = true;
            rsProjectTime.UpdateWhenMoveRecord = false;

            rsProjectTime.AddNew();

            var updateResult = MapAndSaveProjecttime(rsProjectTime, createProjectTime);
            
            if(updateResult.Any(x => x.HasError))
            {
                return BadRequest(
                    $"Saving project times failed, cause: {updateResult.First(x => x.HasError).GetResult()}");
            }

            var createdProjectTimeId = (int)updateResult.First().PrimaryKey;
            var createdProjectTimes = sdk.Find<ProjectTime>(x =>
                x.Id == createdProjectTimeId || x.ORIGINPROJECTTIMEID == createdProjectTimeId);

            return Ok(createdProjectTimes);
        }

        // PUT: api/ProjectTimes
        [HttpPut("{company}/[controller]/{id}")]
        public ActionResult Put(string company, int id, [FromBody] CreateProjectTime createProjectTime)
        {
            if (string.IsNullOrEmpty(company))
            {
                return BadRequest("No company specified.");
            }

            if (createProjectTime.Employee == 0 || createProjectTime.Workactivity == 0)
            {
                return BadRequest("Employee and workactivity are required.");
            }

            var sdk = _sessionProvider.Get(company);
            if (sdk == null)
            {
                return BadRequest($"Company '{company}' not found.");
            }

            var message = ValidateCreateProjectTime(sdk, createProjectTime);
            if (!string.IsNullOrEmpty(message))
            {
                return BadRequest(message);
            }

            var rsProjectTime = sdk.GetRecordsetColumns("R_PROJECTTIME", "",
                $"PK_R_PROJECTTIME = {id}");

            if (rsProjectTime.RecordCount == 0)
            {
                return BadRequest($"No project time found with id {id}.");
            }

            rsProjectTime.MoveFirst();

            //Appreo mag verlofboekingen al direct aanmaken met status 'Afgesloten'. Door onderstaande lukt het dan niet meer om de urenboeking te verwijderen. Check daarom
            //of het een verlofboeking is die door Appreo is aangemaakt die is afgesloten. Ze dan eerst het vinkje 'Afgesloten' uit.

            var holidayWorkactivities = GetHolidayWorkactivities(sdk);

            if((bool)rsProjectTime.GetField("CLOSED").Value && rsProjectTime.GetField("CREATOR").Value.ToString().Equals("SDK_APPREOWEBSERVICE") 
                && holidayWorkactivities.Contains((int)rsProjectTime.GetField("FK_WORKACTIVITY").Value) && holidayWorkactivities.Contains((int)createProjectTime.Workactivity))
            {
                rsProjectTime.MoveFirst();
                rsProjectTime.SetFieldValue("CLOSED", false);
                var updateResult2 = rsProjectTime.Update2();

                if (updateResult2.Any(x => x.HasError))
                {
                    return BadRequest(
                        $"Undo bool 'Closed' project times failed, cause: {updateResult2.First(x => x.HasError).GetResult()}");
                }
            }

            //Bij het aanmaken van een urenboeking is intelligentie voor het splitsen van uren als er overuren van toepassing zijn.
            //Die intelligentie wordt niet getriggerd bij het wijzigen van een urenboeking. 
            //Daarom verwijderen we de urenboeking en maken deze opnieuw aan.
            rsProjectTime.UpdateWhenMoveRecord = false;
            rsProjectTime.MoveFirst();
            rsProjectTime.Delete();

            var updateResult = rsProjectTime.Update2();

            if (updateResult.Any(x => x.HasError))
            {
                return BadRequest(
                    $"Deleting project times failed, cause: {updateResult.First(x => x.HasError).GetResult()}");
            }

            var rsProjectTimeAddNew = sdk.GetRecordsetColumns("R_PROJECTTIME", "", "PK_R_PROJECTTIME = -1");
            rsProjectTimeAddNew.UseDataChanges = true;
            rsProjectTimeAddNew.UpdateWhenMoveRecord = false;

            rsProjectTimeAddNew.AddNew();

            var updateResultInsert = MapAndSaveProjecttime(rsProjectTimeAddNew, createProjectTime);

            if (updateResultInsert.Any(x => x.HasError))
            {
                return BadRequest(
                    $"Saving project times failed, cause: {updateResultInsert.First(x => x.HasError).GetResult()}");
            }

            var createdProjectTimeId = (int)updateResultInsert.First().PrimaryKey;
            var createdProjectTimes = sdk.Find<ProjectTime>(x =>
                x.Id == createdProjectTimeId || x.ORIGINPROJECTTIMEID == createdProjectTimeId);

            return Ok(createdProjectTimes);
        }


        // DELETE api/values/5
        [HttpDelete("{company}/[controller]/{id}")]
        public ActionResult Delete(string company, int id)
        {
            if (string.IsNullOrEmpty(company))
            {
                return BadRequest("No company specified.");
            }

            var sdk = _sessionProvider.Get(company);
            if (sdk == null)
            {
                return BadRequest($"Company '{company}' not found.");
            }

            var rsProjectTime = sdk.GetRecordsetColumns("R_PROJECTTIME", "",
                $"PK_R_PROJECTTIME = {id}");

            if (rsProjectTime.RecordCount == 0)
            {
                return BadRequest($"No project time found with id {id}.");
            }

            //Appreo mag verlofboekingen al direct aanmaken met status 'Afgesloten'. Door onderstaande lukt het dan niet meer om de urenboeking te verwijderen. Check daarom
            //of het een verlofboeking is die door Appreo is aangemaakt die is afgesloten. Ze dan eerst het vinkje 'Afgesloten' uit.

            var holidayWorkactivities = GetHolidayWorkactivities(sdk);

            if ((bool)rsProjectTime.GetField("CLOSED").Value && rsProjectTime.GetField("CREATOR").Value.ToString().Equals("SDK_APPREOWEBSERVICE")
                && holidayWorkactivities.Contains((int)rsProjectTime.GetField("FK_WORKACTIVITY").Value))
            {
                rsProjectTime.MoveFirst();
                rsProjectTime.SetFieldValue("CLOSED", false);
                var updateResult2 = rsProjectTime.Update2();

                if (updateResult2.Any(x => x.HasError))
                {
                    return BadRequest(
                        $"Undo bool 'Closed' project times failed, cause: {updateResult2.First(x => x.HasError).GetResult()}");
                }
            }

            rsProjectTime.UpdateWhenMoveRecord = false;
            rsProjectTime.MoveFirst();
            rsProjectTime.Delete();

            var updateResult = rsProjectTime.Update2();

            if (updateResult.Any(x => x.HasError))
            {
                return BadRequest(
                    $"Deleting project times failed, cause: {updateResult.First(x => x.HasError).GetResult()}");
            }

            return Ok();
        }

        private List<int> GetHolidayWorkactivities(SdkSession sdk)
        {
            return sdk.GetRecordsetColumns("R_WORKACTIVITY", "PK_R_WORKACTIVITY", "WORKACTIVITYTYPE = 10")
                .DataTable.AsEnumerable().Select(x => x.Field<int>("PK_R_WORKACTIVITY")).ToList();
        }


        private ISDKResult[] MapAndSaveProjecttime(ISdkRecordset rsProjectTime, CreateProjectTime createProjectTime)
        {
            rsProjectTime.Fields["DATE"].Value = createProjectTime.Date.Date;
            rsProjectTime.Fields["FK_EMPLOYEE"].Value = createProjectTime.Employee;
            rsProjectTime.Fields["FK_WORKACTIVITY"].Value = createProjectTime.Workactivity;

            if (createProjectTime.Order.HasValue && createProjectTime.Order.Value > 0)
            {
                rsProjectTime.Fields["FK_ORDER"].Value = createProjectTime.Order.Value;
                rsProjectTime.Fields["FK_JOBORDER"].Value = createProjectTime.Joborder.Value;
            }

            if(createProjectTime.JoborderDetailWorkactivity.HasValue && createProjectTime.JoborderDetailWorkactivity.Value > 0)
            {
                rsProjectTime.Fields["FK_JOBORDERDETAILWORKACTIVITY"].Value =
                    createProjectTime.JoborderDetailWorkactivity.Value;
            }

            if(createProjectTime.OverTime.HasValue && createProjectTime.OverTime.Value > 0)
            {
                rsProjectTime.Fields["FK_OVERTIMECODE"].Value = createProjectTime.OverTime.Value;
            }

            rsProjectTime.Fields["STARTTIME"].Value = createProjectTime.StartTime;
            rsProjectTime.Fields["ENDTIME"].Value = createProjectTime.EndTime;

            rsProjectTime.Fields["TIMEEMPLOYEE"].Value = TimeSpan.FromHours(createProjectTime.TimeEmployeeInHours).Ticks;
            rsProjectTime.Fields["TIMEDEVICE"].Value = TimeSpan.FromHours(createProjectTime.TimeEmployeeInHours).Ticks;

            rsProjectTime.Fields["VERKORTEMEMO"].Value = createProjectTime.Memo;

            if(createProjectTime.Closed)
            {
                rsProjectTime.SetFieldValue("CLOSED", true);
            }

            return rsProjectTime.Update2();
        }

        private string ValidateCreateProjectTime(SdkSession sdk, CreateProjectTime createProjectTime)
        {
            var existingEmployee = sdk.Find<Employee>(x => x.Id == createProjectTime.Employee).FirstOrDefault();

            if (existingEmployee == null)
            {
                return $"No employee found with id '{createProjectTime.Employee}'";
            }

            var existingWorkactivity = sdk.Find<Workactivity>(x => x.Id == createProjectTime.Workactivity).FirstOrDefault();

            if (existingWorkactivity == null)
            {
                return $"No workactivity found with id '{createProjectTime.Workactivity}'";
            }

            if ((createProjectTime.Order ?? 0) != 0)
            {
                var existingOrder = sdk.Find<Order>(x => x.Id == createProjectTime.Order).FirstOrDefault();

                if(existingOrder == null)
                {
                    return $"No order found with id '{createProjectTime.Order}'";
                }
            }

            if ((createProjectTime.Joborder ?? 0) != 0)
            {
                var existingJobOrder = sdk.Find<Joborder>(x => x.Id == createProjectTime.Joborder).FirstOrDefault();

                if (existingJobOrder == null)
                {
                    return $"No job order found with id '{createProjectTime.Joborder}'";
                }
            }

            if ((createProjectTime.JoborderDetailWorkactivity ?? 0) != 0)
            {
                var existingJobOrderDetail = sdk.Find<JoborderDetailWorkactivity>(x => x.Id == createProjectTime.JoborderDetailWorkactivity).FirstOrDefault();

                if (existingJobOrderDetail == null)
                {
                    return $"No job order detail workactivity found with id '{createProjectTime.JoborderDetailWorkactivity}'";
                }
            }

            if ((createProjectTime.OverTime ?? 0) != 0)
            {
                var existingOverTime = sdk.Find<OverTimeCode>(x => x.Id == createProjectTime.OverTime).FirstOrDefault();

                if (existingOverTime == null)
                {
                    return $"No overtime found with id '{createProjectTime.OverTime}'";
                }
            }

            return string.Empty;
        }
    }
}
