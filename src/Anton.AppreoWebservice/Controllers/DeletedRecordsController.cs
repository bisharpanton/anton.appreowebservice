﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Anton.AppreoWebservice.Framework;
using Anton.AppreoWebservice.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Anton.AppreoWebservice.Controllers
{
    [Route("api")]
    [ApiController]

    public class DeletedRecordsController : ControllerBase
    {
        private readonly ISdkSessionProvider _sessionProvider;

        public DeletedRecordsController(ISdkSessionProvider sessionProvider)
        {
            _sessionProvider = sessionProvider;
        }

        // GET: api/DeletedRecords
        [HttpGet("{company}/[controller]")]
        public ActionResult<PagedResponse<DeletedRecord>> Get(string company, [FromQuery] PagingOptions pagingOptions,
            DateTime? dateDeleted = null)
        {
            if (string.IsNullOrEmpty(company))
            {
                return BadRequest("No company specified.");
            }

            var sdk = _sessionProvider.Get(company);
            if (sdk == null)
            {
                return BadRequest($"Company '{company}' not found.");
            }

            var filter = (dateDeleted.HasValue)
                ? $"DATECREATED >= '{dateDeleted.Value:yyyyMMdd HH:mm:ss}'"
                : "";

            var deletedRecords = sdk.FindPaged<DeletedRecord>(pagingOptions.Page + 1, pagingOptions.PageSize, filter).ToList();
            var totalResults =
                sdk.Sdk.CreateRecordset("C_DELETEDRECORDS", "PK_C_DELETEDRECORDS", filter, "").RecordCount;

            return new PagedResponse<DeletedRecord>().ViewModelsToPagedResponse(deletedRecords, totalResults, pagingOptions);
        }
    }
}
