﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Anton.AppreoWebservice.Framework;
using Anton.AppreoWebservice.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Anton.AppreoWebservice.Controllers
{
    [Route("api")]
    [ApiController]

    public class ToolsController : ControllerBase
    {
        private readonly ISdkSessionProvider _sessionProvider;

        public ToolsController(ISdkSessionProvider sessionProvider)
        {
            _sessionProvider = sessionProvider;
        }

        // GET: api/Tools
        [HttpGet("{company}/[controller]")]
        public ActionResult<PagedResponse<Tool>> Get(string company, [FromQuery]PagingOptions pagingOptions, DateTime? dateChanged = null)
        {
            if (string.IsNullOrEmpty(company))
            {
                return BadRequest("No company specified.");
            }

            var sdk = _sessionProvider.Get(company);
            if (sdk == null)
            {
                return BadRequest($"Company '{company}' not found.");
            }

            var filter = (dateChanged.HasValue)
                ? $"DATECHANGED >= '{dateChanged.Value:yyyyMMdd HH:mm:ss}'"
                : "";

            var tools = sdk.FindPaged<Tool>(pagingOptions.Page + 1, pagingOptions.PageSize, filter).ToList();
            var totalResults =
                sdk.Sdk.CreateRecordset("R_TOOL", "PK_R_TOOL", filter, "").RecordCount;

            return new PagedResponse<Tool>().ViewModelsToPagedResponse(tools, totalResults, pagingOptions);
        }

        // GET: api/Tools/GetPicture/{id}
        [HttpGet("{company}/[controller]/GetPicture/{id}")]
        public ActionResult<string> GetPicture(string company, string id)
        {
            if (string.IsNullOrEmpty(company))
            {
                return BadRequest("No company specified.");
            }

            var sdk = _sessionProvider.Get(company);
            if (sdk == null)
            {
                return BadRequest($"Company '{company}' not found.");
            }

            var rsTools = sdk.GetRecordsetColumns("R_TOOL", "IMAGE",
                $"PK_R_TOOL = {id}");

            if (rsTools.RecordCount == 0)
            {
                return BadRequest($"Tool with id {id} not found.");
            }

            var picture = rsTools.DataTable.AsEnumerable().First().Field<byte[]>("IMAGE");

            if (picture == null)
            {
                return NotFound($"No picture found at tool id {id}");
            }

            return Convert.ToBase64String(picture);
        }
    }
}
