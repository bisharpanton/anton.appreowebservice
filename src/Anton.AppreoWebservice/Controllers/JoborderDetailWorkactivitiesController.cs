﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Anton.AppreoWebservice.Framework;
using Anton.AppreoWebservice.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Ridder.Client.SDK.Extensions;

namespace Anton.AppreoWebservice.Controllers
{
    [Route("api")]
    [ApiController]
    public class JoborderDetailWorkactivitiesController : ControllerBase
    {
        private readonly ISdkSessionProvider _sessionProvider;

        public JoborderDetailWorkactivitiesController(ISdkSessionProvider sessionProvider)
        {
            _sessionProvider = sessionProvider;
        }

        // GET: api/JoborderDetailWorkactivities
        [HttpGet("{company}/[controller]")]
        public ActionResult<PagedResponse<JoborderDetailWorkactivity>> Get(string company, [FromQuery] PagingOptions pagingOptions,
            DateTime? dateChanged = null)
        {
            if (string.IsNullOrEmpty(company))
            {
                return BadRequest("No company specified.");
            }

            var sdk = _sessionProvider.Get(company);
            if (sdk == null)
            {
                return BadRequest($"Company '{company}' not found.");
            }

            var filter = (dateChanged.HasValue)
                ? $"DATECHANGED >= '{dateChanged.Value:yyyyMMdd HH:mm:ss}'"
                : "";
            
            var joborderDetailWorkactivities = sdk.FindPaged<JoborderDetailWorkactivity>(
                pagingOptions.Page + 1, pagingOptions.PageSize, filter).ToList();
            var totalResults =
                sdk.Sdk.CreateRecordset("R_JOBORDERDETAILWORKACTIVITY", "PK_R_JOBORDERDETAILWORKACTIVITY", filter, "").RecordCount;


            //Service bonnen komen niet op de status 'Vrijgegeven', in Appreo worden default de vrijgegeven bonnen getoond.
            //Daarom tweaken we hier dat service bonnen met status nieuw de status vrijgegeven krijgt.
            foreach (var joborderDetailWorkactivity in joborderDetailWorkactivities.Where(x => x.Joborder.PLANSETTING == PlanSetting.Service && x.Joborder.FK_WORKFLOWSTATE.Value.Equals(JobOrderWorkflowStates.New)))
            {
                joborderDetailWorkactivity.FK_WORKFLOWSTATE = JoborderDetailWorkactivityWorkflowStates.ReleasedForProduction;
                joborderDetailWorkactivity.Joborder.FK_WORKFLOWSTATE = JobOrderWorkflowStates.ReleasedForProduction;
            }

            return new PagedResponse<JoborderDetailWorkactivity>().ViewModelsToPagedResponse(joborderDetailWorkactivities, totalResults, pagingOptions);
        }
        
        // PUT: api/JoborderDetailWorkactivities/ReleaseForProduction/5
        [HttpPut("{company}/[controller]/ReleaseForProduction/{id}")]
        public ActionResult ReleaseForProduction(string company, int id)
        {
            if (string.IsNullOrEmpty(company))
            {
                return BadRequest("No company specified.");
            }

            var sdk = _sessionProvider.Get(company);
            if (sdk == null)
            {
                return BadRequest($"Company '{company}' not found.");
            }

            var joborderDetailWorkactivity = sdk.Find<JoborderDetailWorkactivity>($"PK_R_JOBORDERDETAILWORKACTIVITY = {id}").FirstOrDefault();

            if (joborderDetailWorkactivity == null)
            {
                return BadRequest($"No job order detail workactivity found with id {id}.");
            }

            if (joborderDetailWorkactivity.FK_WORKFLOWSTATE == JoborderDetailWorkactivityWorkflowStates.ReleasedForProduction)
            {
                return Ok(joborderDetailWorkactivity);
            }

            if (joborderDetailWorkactivity.FK_WORKFLOWSTATE == JoborderDetailWorkactivityWorkflowStates.Done)
            {
                return BadRequest("Job order detail workactivity is already done.");
            }

            var wfReleaseForProduction = new Guid("4478ba2d-441c-4687-bd56-e9ce86ce961b");
            var wfResult = sdk.Sdk.ExecuteWorkflowEvent("R_JOBORDERDETAILWORKACTIVITY", joborderDetailWorkactivity.Id, wfReleaseForProduction, null);

            if (wfResult.HasError)
            {
                return BadRequest($"Release for production failed; cause: {wfResult.GetResult()}.");
            }

            //We halen het object opnieuw op, om het veld Workflowstatus bij te werken
            var updatedJoborderDetailWorkactivity = sdk.Find<JoborderDetailWorkactivity>($"PK_R_JOBORDERDETAILWORKACTIVITY = {id}").First();

            return Ok(updatedJoborderDetailWorkactivity);
        }

        
        // PUT: api/JoborderDetailWorkactivities/SetJoborderDone/5
        [HttpPut("{company}/[controller]/SetJoborderDetailWorkactivityDone/{id}")]
        public ActionResult SetJoborderDetailWorkactivityDone(string company, int id)
        {
            if (string.IsNullOrEmpty(company))
            {
                return BadRequest("No company specified.");
            }

            var sdk = _sessionProvider.Get(company);
            if (sdk == null)
            {
                return BadRequest($"Company '{company}' not found.");
            }

            var joborderDetailWorkactivity = sdk.Find<JoborderDetailWorkactivity>($"PK_R_JOBORDERDETAILWORKACTIVITY = {id}").FirstOrDefault();

            if (joborderDetailWorkactivity == null)
            {
                return BadRequest($"No job order detail workactivity found with id {id}.");
            }

            if (joborderDetailWorkactivity.FK_WORKFLOWSTATE == JoborderDetailWorkactivityWorkflowStates.Done)
            {
                return Ok(joborderDetailWorkactivity);
            }

            if (joborderDetailWorkactivity.FK_WORKFLOWSTATE == JoborderDetailWorkactivityWorkflowStates.New)
            {
                return BadRequest("Job order detail workactivity has state 'New'. Set production done is not available.");
            }

            var wfSetProductionDone = new Guid("d649fd98-719d-4ee9-bd22-12f56c747fb6");
            var wfResult = sdk.Sdk.ExecuteWorkflowEvent("R_JOBORDERDETAILWORKACTIVITY", joborderDetailWorkactivity.Id, wfSetProductionDone, null);

            if (wfResult.HasError)
            {
                return BadRequest($"Set production done failed; cause: {wfResult.GetResult()}.");
            }

            //We halen het object opnieuw op, om het veld Workflowstatus bij te werken
            var updatedJoborderDetailWorkactivity = sdk.Find<JoborderDetailWorkactivity>($"PK_R_JOBORDERDETAILWORKACTIVITY = {id}").First();

            return Ok(updatedJoborderDetailWorkactivity);
        }

        // PUT: api/JoborderDetailWorkactivities/RevertProductionDone/5
        [HttpPut("{company}/[controller]/RevertProductionDone/{id}")]
        public ActionResult RevertProductionDone(string company, int id)
        {
            if (string.IsNullOrEmpty(company))
            {
                return BadRequest("No company specified.");
            }

            var sdk = _sessionProvider.Get(company);
            if (sdk == null)
            {
                return BadRequest($"Company '{company}' not found.");
            }

            var joborderDetailWorkactivity = sdk.Find<JoborderDetailWorkactivity>($"PK_R_JOBORDERDETAILWORKACTIVITY = {id}")
                .FirstOrDefault();

            if (joborderDetailWorkactivity == null)
            {
                return BadRequest($"No job order detail workactivity found with id {id}.");
            }
            
            if (joborderDetailWorkactivity.FK_WORKFLOWSTATE == JoborderDetailWorkactivityWorkflowStates.ReleasedForProduction)
            {
                return Ok(joborderDetailWorkactivity);
            }

            if (joborderDetailWorkactivity.FK_WORKFLOWSTATE == JoborderDetailWorkactivityWorkflowStates.New)
            {
                return BadRequest("Job order detail workactivity has state 'New'. Revert production done is not available.");
            }

            var wfRevertProductionDone = new Guid("2b0fe6bb-3e51-4694-82c1-0574c4c626ec");
            var wfResult = sdk.Sdk.ExecuteWorkflowEvent("R_JOBORDERDETAILWORKACTIVITY", joborderDetailWorkactivity.Id, 
                wfRevertProductionDone, null);

            if (wfResult.HasError)
            {
                return BadRequest($"Revert production done failed; cause: {wfResult.GetResult()}.");
            }

            //We halen het object opnieuw op, om het veld Workflowstatus bij te werken
            var updatedJoborderDetailWorkactivity = sdk.Find<JoborderDetailWorkactivity>($"PK_R_JOBORDERDETAILWORKACTIVITY = {id}")
                .First();

            return Ok(updatedJoborderDetailWorkactivity);
        }

        // PUT: api/JoborderDetailWorkactivities/UpdatePlanInfo/5
        [HttpPut("{company}/[controller]/UpdatePlanInfo/{id}")]
        public ActionResult UpdatePlanInfo(string company, int id, DateTime? plannedStartDate, DateTime? plannedEndDate)
        {
            if (string.IsNullOrEmpty(company))
            {
                return BadRequest("No company specified.");
            }

            var sdk = _sessionProvider.Get(company);
            if (sdk == null)
            {
                return BadRequest($"Company '{company}' not found.");
            }

            if (!plannedStartDate.HasValue || !plannedEndDate.HasValue)
            {
                return BadRequest($"No planned start date or planned end date found.");
            }

            if (plannedEndDate.Value.Date < plannedStartDate.Value.Date)
            {
                return BadRequest("Geplande startdatum mag niet voor geplande einddatum liggen.");
            }

            var joborderDetailWorkactivity = sdk.Find<JoborderDetailWorkactivity>($"PK_R_JOBORDERDETAILWORKACTIVITY = {id}")
                .FirstOrDefault();

            if (joborderDetailWorkactivity == null)
            {
                return BadRequest($"No job order detail workactivity found with id {id}.");
            }

            //Ik wil geen andere kolommen wijzigen, daarom voor de zekerheid data updaten via recordsset
            var rsJoborderDetailWorkactivity = sdk.GetRecordsetColumns("R_JOBORDERDETAILWORKACTIVITY", "PLANNEDSTARTDATEPRODUCTION, PLANNEDFINISHDATEPRODUCTION, ISPLANNEDBYAPPREO",
                $"PK_R_JOBORDERDETAILWORKACTIVITY = {id}");

            rsJoborderDetailWorkactivity.UseDataChanges = false;

            rsJoborderDetailWorkactivity.MoveFirst();

            rsJoborderDetailWorkactivity.SetFieldValue("PLANNEDSTARTDATEPRODUCTION", plannedStartDate);
            rsJoborderDetailWorkactivity.SetFieldValue("PLANNEDFINISHDATEPRODUCTION", plannedEndDate);

            if (!(bool)rsJoborderDetailWorkactivity.GetField("ISPLANNEDBYAPPREO").Value)
            {
                rsJoborderDetailWorkactivity.SetFieldValue("ISPLANNEDBYAPPREO", true);
            }

            var updateResult = rsJoborderDetailWorkactivity.Update2();

            if (updateResult != null && updateResult.Any(x => x.HasError))
            {
                return StatusCode(500, $"Bijwerken plan info bonregel bewerking is mislukt, oorzaak: {updateResult.First(x => x.HasError).GetResult()}");
            }

            joborderDetailWorkactivity.PLANNEDSTARTDATEPRODUCTION = plannedStartDate;
            joborderDetailWorkactivity.PLANNEDFINISHDATEPRODUCTION = plannedEndDate;

            return Ok(joborderDetailWorkactivity);
        }
    }
}
