﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Anton.AppreoWebservice.Framework;
using Anton.AppreoWebservice.Models;
using Anton.AppreoWebservice.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Ridder.Client.SDK.Extensions;

namespace Anton.AppreoWebservice.Controllers
{
    [Route("api")]
    [ApiController]
    public class SafetyReportsController : ControllerBase
    {
        private readonly ISdkSessionProvider _sessionProvider;

        public SafetyReportsController(ISdkSessionProvider sessionProvider)
        {
            _sessionProvider = sessionProvider;
        }

        // GET: api/SafetyReports
        [HttpGet("{company}/[controller]")]
        public ActionResult<PagedResponse<SafetyReport>> Get(string company, [FromQuery] PagingOptions pagingOptions,
            DateTime? dateChanged = null)
        {
            if (string.IsNullOrEmpty(company))
            {
                return BadRequest("No company specified.");
            }

            var sdk = _sessionProvider.Get(company);
            if (sdk == null)
            {
                return BadRequest($"Company '{company}' not found.");
            }

            var filter = (dateChanged.HasValue)
                ? $"DATECHANGED >= '{dateChanged.Value:yyyyMMdd HH:mm:ss}'"
                : "";

            var safetyReports = sdk.FindPaged<SafetyReport>(pagingOptions.Page + 1, pagingOptions.PageSize, filter).ToList();
            var totalResults =
                sdk.Sdk.CreateRecordset("C_VEILIGHEIDSMELDINGEN", "PK_C_VEILIGHEIDSMELDINGEN", filter, "").RecordCount;

            JoinDetailtablesIntoPageResult(sdk, safetyReports);

            return new PagedResponse<SafetyReport>().ViewModelsToPagedResponse(safetyReports, totalResults, pagingOptions);
        }

        

        // GET: api/SafetyReportSources
        [HttpGet("{company}/[controller]/GetSafetyReportSources")]
        public ActionResult<PagedResponse<SafetyReportSource>> GetSafetyReportSources(string company, [FromQuery] PagingOptions pagingOptions,
            DateTime? dateChanged = null)
        {
            if (string.IsNullOrEmpty(company))
            {
                return BadRequest("No company specified.");
            }

            var sdk = _sessionProvider.Get(company);
            if (sdk == null)
            {
                return BadRequest($"Company '{company}' not found.");
            }

            var filter = (dateChanged.HasValue)
                ? $"DATECHANGED >= '{dateChanged.Value:yyyyMMdd HH:mm:ss}'"
                : "";

            var safetyReportSources = sdk.FindPaged<SafetyReportSource>(pagingOptions.Page + 1, pagingOptions.PageSize, filter).ToList();
            var totalResults =
                sdk.Sdk.CreateRecordset("C_BRONMELDINGEN", "PK_C_BRONMELDINGEN", filter, "").RecordCount;

            return new PagedResponse<SafetyReportSource>().ViewModelsToPagedResponse(safetyReportSources, totalResults, pagingOptions);
        }

        
        // GET: api/SafetyReportKinds
        [HttpGet("{company}/[controller]/GetSafetyReportKinds")]
        public ActionResult<PagedResponse<SafetyReportKind>> GetSafetyReportKinds(string company, [FromQuery] PagingOptions pagingOptions,
            DateTime? dateChanged = null)
        {
            if (string.IsNullOrEmpty(company))
            {
                return BadRequest("No company specified.");
            }

            var sdk = _sessionProvider.Get(company);
            if (sdk == null)
            {
                return BadRequest($"Company '{company}' not found.");
            }

            var filter = (dateChanged.HasValue)
                ? $"DATECHANGED >= '{dateChanged.Value:yyyyMMdd HH:mm:ss}'"
                : "";

            var safetyReportKinds = sdk.FindPaged<SafetyReportKind>(pagingOptions.Page + 1, pagingOptions.PageSize, filter).ToList();
            var totalResults =
                sdk.Sdk.CreateRecordset("C_MELDINGSOORTEN", "PK_C_MELDINGSOORTEN", filter, "").RecordCount;

            return new PagedResponse<SafetyReportKind>().ViewModelsToPagedResponse(safetyReportKinds, totalResults, pagingOptions);
        }

        // POST: api/SafetyReports
        [HttpPost("{company}/[controller]")]
        public ActionResult<SafetyReport> Post(string company, [FromBody] CreateSafetyReport createSafetyReport)
        {
            if (string.IsNullOrEmpty(company))
            {
                return BadRequest("No company specified.");
            }

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var sdk = _sessionProvider.Get(company);
            if (sdk == null)
            {
                return NotFound($"Company '{company}' not found.");
            }

            var safetyReport = new SafetyReport()
            {
                INCIDENTREPORTING = createSafetyReport.Incidentmelding,
                FK_BRONMELDINGEN = createSafetyReport.SafetyReportSource == 0 ? null : createSafetyReport.SafetyReportSource,
                FK_MELDER = createSafetyReport.Reporter == 0 ? null : createSafetyReport.Reporter,
                DESCRIPTION = createSafetyReport.Description,
                FK_MELDINGSOORTEN = createSafetyReport.SafetyReportKind == 0 ? null : createSafetyReport.SafetyReportKind,
                DATUMMELDING = createSafetyReport.DateSafetyReport,
                MEMO = createSafetyReport.Memo,
                INTERCOMPANYRELATION = createSafetyReport.IntercompanyRelation,
                FK_RELATION = createSafetyReport.RelationId == 0 ? null : createSafetyReport.RelationId,
                CAUSE = createSafetyReport.Cause,
                MEASURES = createSafetyReport.Measures,
                LONGTERMMEASURES = createSafetyReport.LongTermMeasures,
            };

            var validateResult = ValidateSafetyReport(sdk, safetyReport);

            if (!validateResult.Result)
            {
                return BadRequest(validateResult.Message);
            }

            var wfStateNew = new Guid("9d4f91d6-9dcf-4d41-a749-da1b6fb0727e");

            safetyReport.FK_WORKFLOWSTATE = wfStateNew; 
            safetyReport.CLASSIFICATION = Classification.N_;
            safetyReport.SAFETYAWARENESS = Veiligheidsbewustzijn.N_;

            try
            {
                sdk.Insert(safetyReport);
            }
            catch (Exception e)
            {
                return StatusCode(500, $"Aanmaken veiligheidsmelding mislukt, oorzaak: {e.Message}");
            }

            var errorMessage = string.Empty;

            errorMessage = CreateSafetyReportEmployees(createSafetyReport.EmployeeIds, safetyReport, sdk);

            if (!string.IsNullOrEmpty(errorMessage))
                return StatusCode(500, errorMessage);

            errorMessage = CreateSafetyReportIntercompanyRelations(createSafetyReport.IntercompanyRelationIds, safetyReport, sdk);

            if (!string.IsNullOrEmpty(errorMessage))
                return StatusCode(500, errorMessage);

            errorMessage = CreateSafetyReportSubcontractors(createSafetyReport.SubcontractorIds, safetyReport, sdk);

            if (!string.IsNullOrEmpty(errorMessage))
                return StatusCode(500, errorMessage);

            errorMessage = CreateSafetyReportObservationRounds(createSafetyReport.ObservationRoundIds, safetyReport, sdk);

            if (!string.IsNullOrEmpty(errorMessage))
                return StatusCode(500, errorMessage);

            errorMessage = CreateSafetyReportImprovements(createSafetyReport.ImprovementIds, safetyReport, sdk);

            if (!string.IsNullOrEmpty(errorMessage))
                return StatusCode(500, errorMessage);

            return Ok(safetyReport);
        }

        // PUT: api/SafetyReports
        [HttpPut("{company}/[controller]")]
        public ActionResult<SafetyReport> Put(string company, [FromBody] SafetyReport safetyReport)
        {
            if (string.IsNullOrEmpty(company))
            {
                return BadRequest("No company specified.");
            }

            var sdk = _sessionProvider.Get(company);
            if (sdk == null)
            {
                return BadRequest($"Company '{company}' not found.");
            }

            var existingSafetyReport = sdk.Find<SafetyReport>($"PK_C_VEILIGHEIDSMELDINGEN = {safetyReport.Id}")
                .FirstOrDefault();

            if (existingSafetyReport == null)
            {
                return BadRequest($"No safety report found with id {safetyReport.Id}.");
            }

            var validateResult = ValidateSafetyReport(sdk, safetyReport);

            if (!validateResult.Result)
            {
                return BadRequest(validateResult.Message);
            }

            safetyReport.FK_BRONMELDINGEN = safetyReport.FK_BRONMELDINGEN == 0 ? null : safetyReport.FK_BRONMELDINGEN;
            safetyReport.FK_MELDER = safetyReport.FK_MELDER == 0 ? null : safetyReport.FK_MELDER;
            safetyReport.FK_MELDINGSOORTEN = safetyReport.FK_MELDINGSOORTEN == 0 ? null : safetyReport.FK_MELDINGSOORTEN;

            //Bij een wijzigings actie mogen deze velden niet gewijzigd worden.
            safetyReport.FK_WORKFLOWSTATE = existingSafetyReport.FK_WORKFLOWSTATE;
            safetyReport.DATUMOPGELOST = existingSafetyReport.DATUMOPGELOST;
            safetyReport.CLASSIFICATION = existingSafetyReport.CLASSIFICATION;
            safetyReport.SAFETYAWARENESS = existingSafetyReport.SAFETYAWARENESS;


            try
            {
                sdk.Update(safetyReport);
            }
            catch (Exception e)
            {
                return StatusCode(500, $"Bijwerken veiligheidsmelding mislukt, oorzaak: {e.Message}");
            }

            var errorMessage = string.Empty;

            var existingEmployeeIds = sdk.Find<SafetyReportEmployee>($"FK_SAFETYREPORT == {safetyReport.Id}").Select(x => x.FK_EMPLOYEE.Value).ToList();
            var newEmployeeIds = safetyReport.EmployeeIds == null ? new List<int>() : safetyReport.EmployeeIds.ToList();

            if (existingEmployeeIds.Count != newEmployeeIds.Count || existingEmployeeIds.Except(newEmployeeIds).Any())
            {
                errorMessage = GlobalFunctions.DeleteRecordsOfDetailTable(sdk, "U_SAFETYREPORTEMPLOYEE", $"FK_SAFETYREPORT = {safetyReport.Id}");

                if (!string.IsNullOrEmpty(errorMessage))
                    return StatusCode(500, errorMessage);

                errorMessage = CreateSafetyReportEmployees(safetyReport.EmployeeIds, safetyReport, sdk);

                if (!string.IsNullOrEmpty(errorMessage))
                    return StatusCode(500, errorMessage);
            }

            var existingIntercompanyRelationIds = sdk.Find<SafetyReportIntercompanyRelation>($"FK_SAFETYREPORT == {safetyReport.Id}").Select(x => x.FK_RELATION.Value).ToList();
            var newIntercompanyRelationIds = safetyReport.IntercompanyRelationIds == null ? new List<int>() : safetyReport.IntercompanyRelationIds.ToList();

            if (existingIntercompanyRelationIds.Count != newIntercompanyRelationIds.Count || existingIntercompanyRelationIds.Except(newIntercompanyRelationIds).Any())
            {
                errorMessage = GlobalFunctions.DeleteRecordsOfDetailTable(sdk, "U_SAFETYREPORTINTERCOMPANYRELATION", $"FK_SAFETYREPORT = {safetyReport.Id}");

                if (!string.IsNullOrEmpty(errorMessage))
                    return StatusCode(500, errorMessage);

                errorMessage = CreateSafetyReportIntercompanyRelations(safetyReport.IntercompanyRelationIds, safetyReport, sdk);

                if (!string.IsNullOrEmpty(errorMessage))
                    return StatusCode(500, errorMessage);
            }

            var existingSubcontractorIds = sdk.Find<SafetyReportSubcontractor>($"FK_SAFETYREPORT == {safetyReport.Id}").Select(x => x.FK_RELATION.Value).ToList();
            var newSubcontractIds = safetyReport.SubcontractorIds == null ? new List<int>() : safetyReport.SubcontractorIds.ToList();

            if (existingSubcontractorIds.Count != newSubcontractIds.Count || existingSubcontractorIds.Except(newSubcontractIds).Any())
            {
                errorMessage = GlobalFunctions.DeleteRecordsOfDetailTable(sdk, "U_SAFETYREPORTSUBCONTRACTOR", $"FK_SAFETYREPORT = {safetyReport.Id}");

                if (!string.IsNullOrEmpty(errorMessage))
                    return StatusCode(500, errorMessage);

                errorMessage = CreateSafetyReportSubcontractors(safetyReport.SubcontractorIds, safetyReport, sdk);

                if (!string.IsNullOrEmpty(errorMessage))
                    return StatusCode(500, errorMessage);
            }

            var existingObservationRoundsIds = sdk.Find<SafetyReportObservationRound>($"FK_SAFETYREPORT == {safetyReport.Id}").Select(x => x.FK_OBSERVATIONROUND.Value).ToList();
            var newObservationRoundsIds = safetyReport.ObservationRoundIds == null ? new List<int>() : safetyReport.ObservationRoundIds.ToList();

            if (existingObservationRoundsIds.Count != newObservationRoundsIds.Count || existingObservationRoundsIds.Except(newObservationRoundsIds).Any())
            {
                errorMessage = GlobalFunctions.DeleteRecordsOfDetailTable(sdk, "U_SAFETYREPORTOBSERVATIONROUND", $"FK_SAFETYREPORT = {safetyReport.Id}");

                if (!string.IsNullOrEmpty(errorMessage))
                    return StatusCode(500, errorMessage);

                errorMessage = CreateSafetyReportObservationRounds(safetyReport.ObservationRoundIds, safetyReport, sdk);

                if (!string.IsNullOrEmpty(errorMessage))
                    return StatusCode(500, errorMessage);
            }

            var existingImprovementIds = sdk.Find<SafetyReportImprovement>($"FK_SAFETYREPORT == {safetyReport.Id}").Select(x => x.FK_IMPROVEMENTS.Value).ToList();
            var newImprovementIds = safetyReport.ImprovementIds == null ? new List<int>() : safetyReport.ImprovementIds.ToList();

            if (existingImprovementIds.Count != newImprovementIds.Count || existingImprovementIds.Except(newImprovementIds).Any())
            {
                errorMessage = GlobalFunctions.DeleteRecordsOfDetailTable(sdk, "U_SAFETYREPORTIMPROVEMENTS", $"FK_SAFETYREPORT = {safetyReport.Id}");

                if (!string.IsNullOrEmpty(errorMessage))
                    return StatusCode(500, errorMessage);

                errorMessage = CreateSafetyReportImprovements(safetyReport.ImprovementIds, safetyReport, sdk);

                if (!string.IsNullOrEmpty(errorMessage))
                    return StatusCode(500, errorMessage);
            }


            return Ok(safetyReport);
        }

        

        // DELETE api/SafetyReports/5
        [HttpDelete("{company}/[controller]/{id}")]
        public ActionResult Delete(string company, int id)
        {
            if (string.IsNullOrEmpty(company))
            {
                return BadRequest("No company specified.");
            }

            var sdk = _sessionProvider.Get(company);
            if (sdk == null)
            {
                return BadRequest($"Company '{company}' not found.");
            }

            var rsSafetyReport = sdk.GetRecordsetColumns("C_VEILIGHEIDSMELDINGEN", "",
                $"PK_C_VEILIGHEIDSMELDINGEN = {id}");

            if (rsSafetyReport.RecordCount == 0)
            {
                return BadRequest($"No safety report found with id {id}.");
            }

            rsSafetyReport.UpdateWhenMoveRecord = false;
            rsSafetyReport.MoveFirst();
            rsSafetyReport.Delete();

            var updateResult = rsSafetyReport.Update2();

            if (updateResult.Any(x => x.HasError))
            {
                return BadRequest(
                    $"Deleting safety report failed, cause: {updateResult.First(x => x.HasError).GetResult()}");
            }

            return Ok();
        }

        private ValidateResult ValidateSafetyReport(SdkSession sdk, SafetyReport safetyReport)
        {
            var employee = sdk.Find<Employee>(x => x.Id == safetyReport.FK_MELDER).FirstOrDefault();

            if ((safetyReport.FK_MELDER ?? 0) != 0  && employee == null)
            {
                return new ValidateResult()
                {
                    Result = false, Message = $"No employee found with id {safetyReport.FK_MELDER}"
                };
            }

            var safetyReportSource = sdk.Find<SafetyReportSource>(x => x.Id == safetyReport.FK_BRONMELDINGEN).FirstOrDefault();

            if ((safetyReport.FK_BRONMELDINGEN ?? 0) != 0 && safetyReportSource == null)
            {
                return new ValidateResult()
                {
                    Result = false, Message = $"No safety report source  found with id {safetyReport.FK_BRONMELDINGEN}"
                };
            }

            var safetyReportKind = sdk.Find<SafetyReportKind>(x => x.Id == safetyReport.FK_MELDINGSOORTEN).FirstOrDefault();

            if ((safetyReport.FK_MELDINGSOORTEN ?? 0) != 0 && safetyReportKind == null)
            {
                return new ValidateResult()
                {
                    Result = false, Message = $"No safety report kind  found with id {safetyReport.FK_MELDINGSOORTEN}"
                };
            }

            if (safetyReport.INCIDENTREPORTING == Incidentmelding.N_)
            {
                return new ValidateResult()
                {
                    Result = false, Message = $"Incident reporting is required."
                };
            }

            if((safetyReport.FK_RELATION ?? 0) != 0)
            {
                var relation = sdk.Find<Relation>(x => x.Id == safetyReport.FK_RELATION).FirstOrDefault();

                if (relation == null)
                {
                    return new ValidateResult()
                    {
                        Result = false,
                        Message = $"No relation found with id {safetyReport.FK_RELATION}"
                    };
                }
            }


            return new ValidateResult() {Result = true, Message = ""};
        }

        private void JoinDetailtablesIntoPageResult(SdkSession sdk, List<SafetyReport> safetyReports)
        {
            var safetyReportIds = safetyReports.Select(x => x.Id).ToList();

            var safetyReportEmployees = sdk.Find<SafetyReportEmployee>($"FK_SAFETYREPORT IN ({string.Join(",", safetyReportIds)})").ToList();
            var safetyReportIntercompanyRelations = sdk.Find<SafetyReportIntercompanyRelation>($"FK_SAFETYREPORT IN ({string.Join(",", safetyReportIds)})").ToList();
            var safetyReportSubcontractors = sdk.Find<SafetyReportSubcontractor>($"FK_SAFETYREPORT IN ({string.Join(",", safetyReportIds)})").ToList();
            var safetyReportObservationsRounds = sdk.Find<SafetyReportObservationRound>($"FK_SAFETYREPORT IN ({string.Join(",", safetyReportIds)})").ToList();
            var safetyReportImprovements = sdk.Find<SafetyReportImprovement>($"FK_SAFETYREPORT IN ({string.Join(",", safetyReportIds)})").ToList();

            foreach (var safetyReport in safetyReports)
            {
                safetyReport.EmployeeIds = safetyReportEmployees.Where(x => x.FK_SAFETYREPORT == safetyReport.Id && x.FK_EMPLOYEE.HasValue).Select(x => x.FK_EMPLOYEE.Value).ToList();
                safetyReport.IntercompanyRelationIds = safetyReportIntercompanyRelations.Where(x => x.FK_SAFETYREPORT == safetyReport.Id && x.FK_RELATION.HasValue).Select(x => x.FK_RELATION.Value).ToList();
                safetyReport.SubcontractorIds = safetyReportSubcontractors.Where(x => x.FK_SAFETYREPORT == safetyReport.Id && x.FK_RELATION.HasValue).Select(x => x.FK_RELATION.Value).ToList();
                safetyReport.ObservationRoundIds = safetyReportObservationsRounds.Where(x => x.FK_SAFETYREPORT == safetyReport.Id && x.FK_OBSERVATIONROUND.HasValue).Select(x => x.FK_OBSERVATIONROUND.Value).ToList();
                safetyReport.ImprovementIds = safetyReportImprovements.Where(x => x.FK_SAFETYREPORT == safetyReport.Id && x.FK_IMPROVEMENTS.HasValue).Select(x => x.FK_IMPROVEMENTS.Value).ToList();
            }
        }

        private string CreateSafetyReportEmployees(List<int> employeeIds, SafetyReport safetyReport, SdkSession sdk)
        {
            if(employeeIds == null)
            {
                return string.Empty;
            }

            employeeIds = employeeIds.Where(x => x != 0).ToList();

            if (!employeeIds.Any())
            {
                return string.Empty;
            }

            try
            {
                var safetyReportEmployees = employeeIds
                    .Select(x => new SafetyReportEmployee()
                    {
                        FK_EMPLOYEE = x,
                        FK_SAFETYREPORT = safetyReport.Id,
                    }).ToList();

                sdk.InsertBulk(safetyReportEmployees);
            }
            catch (Exception e)
            {
                return $"Aanmaken veiligheidsmelding betrokken werknemers mislukt, oorzaak: {e.Message}";
            }

            safetyReport.EmployeeIds = employeeIds;

            return string.Empty;
        }

        private string CreateSafetyReportIntercompanyRelations(List<int> intercompanyRelationIds, SafetyReport safetyReport, SdkSession sdk)
        {
            if (intercompanyRelationIds == null)
            {
                return string.Empty;
            }

            intercompanyRelationIds = intercompanyRelationIds.Where(x => x != 0).ToList();

            if (!intercompanyRelationIds.Any())
            {
                return string.Empty;
            }

            try
            {
                var newRecords = intercompanyRelationIds
                    .Where(x => x != 0)
                    .Select(x => new SafetyReportIntercompanyRelation()
                    {
                        FK_RELATION = x,
                        FK_SAFETYREPORT = safetyReport.Id,
                    }).ToList();

                sdk.InsertBulk(newRecords);
            }
            catch (Exception e)
            {
                return $"Aanmaken veiligheidsmeldingen zusterbedrijven mislukt, oorzaak: {e.Message}";
            }

            safetyReport.IntercompanyRelationIds = intercompanyRelationIds;

            return string.Empty;
        }

        private string CreateSafetyReportSubcontractors(List<int> subcontractorIds, SafetyReport safetyReport, SdkSession sdk)
        {
            if (subcontractorIds == null)
            {
                return string.Empty;
            }

            subcontractorIds = subcontractorIds.Where(x => x != 0).ToList();

            if (!subcontractorIds.Any())
            {
                return string.Empty;
            }

            try
            {
                var newRecords = subcontractorIds
                    .Where(x => x != 0)
                    .Select(x => new SafetyReportSubcontractor()
                    {
                        FK_RELATION = x,
                        FK_SAFETYREPORT = safetyReport.Id,
                    }).ToList();

                sdk.InsertBulk(newRecords);
            }
            catch (Exception e)
            {
                return $"Aanmaken veiligheidsmelding onderaannemers mislukt, oorzaak: {e.Message}";
            }

            safetyReport.SubcontractorIds = subcontractorIds;

            return string.Empty;
        }

        private string CreateSafetyReportObservationRounds(List<int> observationRoundIds, SafetyReport safetyReport, SdkSession sdk)
        {
            if (observationRoundIds == null)
            {
                return string.Empty;
            }

            observationRoundIds = observationRoundIds.Where(x => x != 0).ToList();

            if (!observationRoundIds.Any())
            {
                return string.Empty;
            }

            try
            {
                var newRecords = observationRoundIds
                    .Where(x => x != 0)
                    .Select(x => new SafetyReportObservationRound()
                    {
                        FK_OBSERVATIONROUND = x,
                        FK_SAFETYREPORT = safetyReport.Id,
                    }).ToList();

                sdk.InsertBulk(newRecords);
            }
            catch (Exception e)
            {
                return $"Aanmaken veiligheidsmelding observatierondes mislukt, oorzaak: {e.Message}";
            }

            safetyReport.ObservationRoundIds = observationRoundIds;

            return string.Empty;
        }

        private string CreateSafetyReportImprovements(List<int> improvementIds, SafetyReport safetyReport, SdkSession sdk)
        {
            if (improvementIds == null)
            {
                return string.Empty;
            }

            improvementIds = improvementIds.Where(x => x != 0).ToList();

            if (!improvementIds.Any())
            {
                return string.Empty;
            }

            try
            {
                var newRecords = improvementIds
                    .Where(x => x != 0)
                    .Select(x => new SafetyReportImprovement()
                    {
                        FK_IMPROVEMENTS = x,
                        FK_SAFETYREPORT = safetyReport.Id,
                    }).ToList();

                sdk.InsertBulk(newRecords);
            }
            catch (Exception e)
            {
                return $"Aanmaken veiligheidsmelding verbetervoorstellen mislukt, oorzaak: {e.Message}";
            }

            safetyReport.ImprovementIds = improvementIds;

            return string.Empty;
        }
    }
}
