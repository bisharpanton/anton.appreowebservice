﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using Anton.AppreoWebservice.Framework;
using Anton.AppreoWebservice.Models;
using Anton.AppreoWebservice.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Ridder.Client.SDK.Extensions;

namespace Anton.AppreoWebservice.Controllers
{
    [Route("api")]
    [ApiController]

    public class ItemController : ControllerBase
    {
        private readonly ISdkSessionProvider _sessionProvider;

        public ItemController(ISdkSessionProvider sessionProvider)
        {
            _sessionProvider = sessionProvider;
        }

        // GET: api/Items
        [HttpGet("{company}/[controller]")]
        public ActionResult<PagedResponse<Item>> Get(string company, [FromQuery]PagingOptions pagingOptions, DateTime? dateChanged = null)
        {
            if (string.IsNullOrEmpty(company))
            {
                return BadRequest("No company specified.");
            }

            var sdk = _sessionProvider.Get(company);
            if(sdk == null)
            {
                return BadRequest($"Company '{company}' not found");
            }

            var filter = (dateChanged.HasValue)
                ? $"DATECHANGED >= '{dateChanged.Value:yyyyMMdd HH:mm:ss}'"
                : "";

            var items = sdk.FindPaged<Item>(pagingOptions.Page + 1, pagingOptions.PageSize, filter).ToList();
            var totalResults =
                sdk.Sdk.CreateRecordset("R_ITEM", "PK_R_ITEM", filter, "").RecordCount;

            return new PagedResponse<Item>().ViewModelsToPagedResponse(items, totalResults, pagingOptions);
        }

        // GET: api/OrderWipDetailItem
        [HttpGet("{company}/[controller]/OrderWipDetailItem")]
        public ActionResult<PagedResponse<OrderWipDetailItem>> GetOrderWipDetailItem(string company, [FromQuery]PagingOptions pagingOptions, DateTime? dateChanged = null)
        {
            if (string.IsNullOrEmpty(company))
            {
                return BadRequest("No company specified.");
            }

            var sdk = _sessionProvider.Get(company);
            if (sdk == null)
            {
                return BadRequest($"Company '{company}' not found");
            }

            var filter = (dateChanged.HasValue)
                ? $"DATECHANGED >= '{dateChanged.Value:yyyyMMdd HH:mm:ss}'"
                : "";

            var items = sdk.FindPaged<OrderWipDetailItem>(pagingOptions.Page + 1, pagingOptions.PageSize, filter).ToList();
            var totalResults = 
                sdk.Sdk.CreateRecordset("R_ORDERWIPDETAILITEM", "PK_R_ORDERWIPDETAILITEM", filter, "").RecordCount;

            return new PagedResponse<OrderWipDetailItem>().ViewModelsToPagedResponse(items, totalResults, pagingOptions);
        }

        // POST: api/OrderWipDetailItem
        [HttpPost("{company}/[controller]/OrderWipDetailItem")]
        public ActionResult<OrderWipDetailItem> Post(string company, [FromBody] OrderWipDetailItem createOrderWipDetailItem)
        {
            if (string.IsNullOrEmpty(company))
            {
                return BadRequest("No company specified.");
            }

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (createOrderWipDetailItem.FK_ITEM == 0)
            {
                return BadRequest("Item is required to create a detailitem.");
            }

            if (createOrderWipDetailItem.FK_ORDER == 0)
            {
                return BadRequest("Order is required to create a detailitem.");
            }

            if (Math.Abs(createOrderWipDetailItem.QUANTITY) < 0.0001)
            {
                return BadRequest("Quantity cannot be zero.");
            }

            var sdk = _sessionProvider.Get(company);
            if(sdk == null)
            {
                return NotFound($"Company '{company}' not found.");
            }

            var item = sdk.Find<Item>(x => x.Id == createOrderWipDetailItem.FK_ITEM).FirstOrDefault();
            if (item == null)
            {
                return NotFound($"Item with id '{createOrderWipDetailItem.FK_ITEM}' not found.");
            }

            var order = sdk.Find<Order>(x => x.Id == createOrderWipDetailItem.FK_ORDER).FirstOrDefault();
            if (order == null)
            {
                return NotFound($"Order with id '{createOrderWipDetailItem.FK_ORDER}' not found");
            }

            if((createOrderWipDetailItem.FK_JOBORDER ?? 0) != 0)
            {
                var joborder = sdk.Find<Joborder>(x => x.Id == createOrderWipDetailItem.FK_JOBORDER).FirstOrDefault();
                if (joborder == null)
                {
                    return NotFound($"Job order with id '{createOrderWipDetailItem.FK_JOBORDER}' not found");
                }
            }

            var errorMessage = string.Empty;
            var createdOrderWipDetailItemId = CreateOrderWipDetailItem(sdk, createOrderWipDetailItem, ref errorMessage);
            if (!string.IsNullOrEmpty(errorMessage))
            {
                return BadRequest(errorMessage);
            }

            createOrderWipDetailItem.Id = createdOrderWipDetailItemId;

            return Ok(createOrderWipDetailItem);
        }

        // PUT: api/OrderWipDetailItem
        [HttpPut("{company}/[controller]/OrderWipDetailItem")]
        public ActionResult<OrderWipDetailItem> Put(string company, int id, [FromBody] OrderWipDetailItem orderWipDetailItem)
        {
            if (string.IsNullOrEmpty(company))
            {
                return BadRequest("No company specified.");
            }

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (orderWipDetailItem.FK_ITEM == 0)
            {
                return BadRequest("Item is required.");
            }

            if (orderWipDetailItem.FK_ORDER == 0)
            {
                return BadRequest("Order is required.");
            }

            if (Math.Abs(orderWipDetailItem.QUANTITY) < 0.0001)
            {
                return BadRequest("Quantity cannot be zero.");
            }

            var sdk = _sessionProvider.Get(company);
            if (sdk == null)
            {
                return NotFound($"Company '{company}' not found.");
            }

            var item = sdk.Find<Item>(x => x.Id == orderWipDetailItem.FK_ITEM).FirstOrDefault();
            if (item == null)
            {
                return NotFound($"Item with id '{orderWipDetailItem.FK_ITEM}' not found.");
            }

            var order = sdk.Find<Order>(x => x.Id == orderWipDetailItem.FK_ORDER).FirstOrDefault();
            if (order == null)
            {
                return NotFound($"Order with id '{orderWipDetailItem.FK_ORDER}' not found");
            }

            if ((orderWipDetailItem.FK_JOBORDER ?? 0) != 0)
            {
                var joborder = sdk.Find<Joborder>(x => x.Id == orderWipDetailItem.FK_JOBORDER).FirstOrDefault();
                if (joborder == null)
                {
                    return NotFound($"Job order with id '{orderWipDetailItem.FK_JOBORDER}' not found");
                }
            }

            var existingOrderWipDetail = sdk.Find<OrderWipDetailItem>(x => x.Id == id).FirstOrDefault();

            if(existingOrderWipDetail == null)
            {
                return NotFound($"No order wip detail found with id '{id}'");
            }

            var errorMessage = string.Empty;
            
            if(!UpdateOrderWipDetailItem(sdk, id, orderWipDetailItem, ref errorMessage))
            {
                return BadRequest(errorMessage);
            }


            return Ok(orderWipDetailItem);
        }


        // DELETE api/values/5
        [HttpDelete("{company}/[controller]/OrderWipDetailItem/{id}")]
        public ActionResult Delete(string company, int id)
        {
            if (string.IsNullOrEmpty(company))
            {
                return BadRequest("No company specified.");
            }

            var sdk = _sessionProvider.Get(company);
            if (sdk == null)
            {
                return NotFound($"Company '{company}' not found.");
            }

            var rsOrderWipDetailItem = sdk.GetRecordsetColumns("R_ORDERWIPDETAILITEM", "PK_R_ORDERWIPDETAILITEM",
                $"PK_R_ORDERWIPDETAILITEM = {id}");

            if (rsOrderWipDetailItem.RecordCount == 0)
            {
                return BadRequest($"No order wip detail item found with id {id}.");
            }

            rsOrderWipDetailItem.UpdateWhenMoveRecord = false;
            rsOrderWipDetailItem.MoveFirst();
            rsOrderWipDetailItem.Delete();

            var updateResult = rsOrderWipDetailItem.Update2();

            if (updateResult.Any(x => x.HasError))
            {
                return BadRequest(
                    $"Deleting order wip detail failed, cause: {updateResult.First(x => x.HasError).GetResult()}");
            }

            return Ok();
        }

        private int CreateOrderWipDetailItem(SdkSession sdk, OrderWipDetailItem createOrderWipDetailItem, ref string errorMessage)
        {
            var rsOrderWipDetailItem = sdk.GetRecordsetColumns("R_ORDERWIPDETAILITEM", "", "PK_R_ORDERWIPDETAILITEM = -1");
            rsOrderWipDetailItem.UseDataChanges = true;
            rsOrderWipDetailItem.UpdateWhenMoveRecord = false;
            rsOrderWipDetailItem.AddNew();

            rsOrderWipDetailItem.Fields["FK_ORDER"].Value = createOrderWipDetailItem.FK_ORDER;
            
            if ((createOrderWipDetailItem.FK_JOBORDER ?? 0) != 0)
            {
                rsOrderWipDetailItem.Fields["FK_JOBORDER"].Value = createOrderWipDetailItem.FK_JOBORDER;
            }
            else
            {
                rsOrderWipDetailItem.Fields["FK_JOBORDER"].Value = DBNull.Value;
            }

            rsOrderWipDetailItem.Fields["FK_ITEM"].Value = createOrderWipDetailItem.FK_ITEM;
            rsOrderWipDetailItem.Fields["QUANTITY"].Value = createOrderWipDetailItem.QUANTITY;
            rsOrderWipDetailItem.Fields["DATEINWIP"].Value = createOrderWipDetailItem.DATEINWIP;

            var updateResult = rsOrderWipDetailItem.Update2();

            if (updateResult != null && updateResult.Any(x => x.HasError))
            {
                errorMessage = $"Aanmaken orderwipdetailitem mislukt, oorzaak: {updateResult.First(x => x.HasError).GetResult()}";
                return 0;
            }

            return (int)updateResult.First().PrimaryKey;
        }

        private bool UpdateOrderWipDetailItem(SdkSession sdk, int id, OrderWipDetailItem createOrderWipDetailItem, ref string errorMessage)
        {
            var rsOrderWipDetailItem = sdk.GetRecordsetColumns("R_ORDERWIPDETAILITEM", "", $"PK_R_ORDERWIPDETAILITEM = {id}");
            rsOrderWipDetailItem.UseDataChanges = true;
            rsOrderWipDetailItem.UpdateWhenMoveRecord = false;
            rsOrderWipDetailItem.MoveFirst();

            rsOrderWipDetailItem.Fields["FK_ORDER"].Value = createOrderWipDetailItem.FK_ORDER;

            if ((createOrderWipDetailItem.FK_JOBORDER ?? 0) != 0)
            {
                rsOrderWipDetailItem.Fields["FK_JOBORDER"].Value = createOrderWipDetailItem.FK_JOBORDER;
            }
            else
            {
                rsOrderWipDetailItem.Fields["FK_JOBORDER"].Value = DBNull.Value;
            }

            rsOrderWipDetailItem.Fields["FK_ITEM"].Value = createOrderWipDetailItem.FK_ITEM;
            rsOrderWipDetailItem.Fields["QUANTITY"].Value = createOrderWipDetailItem.QUANTITY;
            rsOrderWipDetailItem.Fields["DATEINWIP"].Value = createOrderWipDetailItem.DATEINWIP;

            var updateResult = rsOrderWipDetailItem.Update2();

            if (updateResult != null && updateResult.Any(x => x.HasError))
            {
                errorMessage = $"Wijzigen orderwipdetailitem mislukt, oorzaak: {updateResult.First(x => x.HasError).GetResult()}";
                return false;
            }

            return true;
        }
    }
}
