﻿using System;
using System.Collections.Generic;
using System.Linq;
using Anton.AppreoWebservice.Framework;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

namespace Anton.AppreoWebservice.Controllers
{
    [Route("api/[controller]")]
    [ApiController]

    public class CompaniesController : ControllerBase
    {
        private readonly SdkCompaniesOptions _options;

        public CompaniesController(IOptions<SdkCompaniesOptions> options)
        {
            _options = options?.Value ?? throw new ArgumentNullException(nameof(options));
        }

        [HttpGet]
        public ActionResult<List<string>> Get()
        {
            return _options.Companies.Select(x => x.Key).ToList();
        }
    }
}