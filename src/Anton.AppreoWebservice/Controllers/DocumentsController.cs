﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Anton.AppreoWebservice.Framework;
using Anton.AppreoWebservice.Models;
using Anton.AppreoWebservice.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Ridder.Client.SDK.Extensions;

namespace Anton.AppreoWebservice.Controllers
{
    [Route("api")]
    [ApiController]

    public class DocumentsController : ControllerBase
    {
        private readonly ISdkSessionProvider _sessionProvider;

        public DocumentsController(ISdkSessionProvider sessionProvider)
        {
            _sessionProvider = sessionProvider;
        }

        // GET: api/Documents
        [HttpGet("{company}/[controller]")]
        public ActionResult<PagedResponse<Document>> Get(string company, [FromQuery] PagingOptions pagingOptions, DateTime? dateChanged = null, string tableName = null)
        {
            if (string.IsNullOrEmpty(company))
            {
                return BadRequest("No company specified.");
            }

            var sdk = _sessionProvider.Get(company);
            if (sdk == null)
            {
                return BadRequest($"Company '{company}' not found.");
            }

            var documents = GetDocumentInfoFromLinkTables(sdk, dateChanged, tableName);

            if (!documents.Any())
            {
                return new PagedResponse<Document>()
                {
                    Page = pagingOptions?.Page,
                    PageSize = pagingOptions?.PageSize,
                    TotalResults = 0,
                    Results = new List<Document>(),
                };
            }

            //Maak paging actief
            var pagedDocuments = GetDocumentsPaged(documents.OrderBy(x => x.DocumentId).ToList(), pagingOptions);

            var allDocumentIds = pagedDocuments.Select(x => x.DocumentId).ToList();
            var iQDocuments = sdk.Find<iQDocument>(x => allDocumentIds.Contains(x.Id)).ToList();

            foreach (Document pagedDocument in pagedDocuments)
            {
                if(pagedDocument.IsDeleted)
                {
                    continue;
                }

                var iqDocument = iQDocuments.First(x => x.Id == pagedDocument.DocumentId);

                pagedDocument.DateChanged = iqDocument.DATECHANGED.Value;

                if (iqDocument.STORAGESYSTEM == "Database")
                {
                    pagedDocument.FileName = $"{iqDocument.DESCRIPTION}{iqDocument.EXTENSION}";
                    pagedDocument.DocumentInBase64String = Convert.ToBase64String(iqDocument.DOCUMENTDATA);
                }
                else if (iqDocument.STORAGESYSTEM == "Verwijzing" && System.IO.File.Exists(iqDocument.DOCUMENTLOCATION))
                {
                    pagedDocument.FileName = Path.GetFileName(iqDocument.DOCUMENTLOCATION);
                    pagedDocument.DocumentInBase64String =
                        Convert.ToBase64String(System.IO.File.ReadAllBytes(iqDocument.DOCUMENTLOCATION));
                }
            }

            return new PagedResponse<Document>()
            {
                Page = pagingOptions?.Page,
                PageSize = pagingOptions?.PageSize,
                TotalResults = documents.Count,
                Results = pagedDocuments,
            };
        }

        private static List<Document> GetDocumentInfoFromLinkTables(SdkSession sdk, DateTime? dateChanged, string tableName)
        {
            var result = new List<Document>();

            //Het opvragen van alle documenten zonder dateChanged duurt veel te lang. Daarom kappen we dat af op het laatste jaar.
            if(!dateChanged.HasValue)
            {
                dateChanged = DateTime.Now.Date.AddYears(-1);
            }
            
            var iqDocuments = sdk.Find<iQDocument>(x => x.DATECHANGED.Value >= dateChanged.Value).ToList();
            var iQDocumentIds = iqDocuments.Select(x => x.Id).ToList();

            if (string.IsNullOrEmpty(tableName) || tableName.Equals("R_EMPLOYEE"))
            {
                result.AddRange(sdk.Find<EmployeeDocument>(x => iQDocumentIds.Contains(x.FK_DOCUMENT)).Select(x =>
                            new Document() { DocumentId = x.FK_DOCUMENT, TableName = "R_EMPLOYEE", RecordId = x.FK_EMPLOYEE }).ToList());
            }

            if (string.IsNullOrEmpty(tableName) || tableName.Equals("C_VGINSPECTIEGEDRAGSPUNTEN"))
            {
                result.AddRange(sdk.Find<ObservationRoundDocument>(x => iQDocumentIds.Contains(x.FK_DOCUMENT)).Select(x =>
                        new Document() { DocumentId = x.FK_DOCUMENT, TableName = "C_VGINSPECTIEGEDRAGSPUNTEN", RecordId = x.FK_VGINSPECTIEGEDRAGSPUNTEN }));
            }

            if (string.IsNullOrEmpty(tableName) || tableName.Equals("R_ORDER") || tableName.Equals("R_JOBORDERDETAILWORKACTIVITY"))
            {
                var orderDocuments = sdk.Find<OrderDocument>(x => iQDocumentIds.Contains(x.FK_DOCUMENT)).ToList();
                var allWorkactivityOrderDocuments = !orderDocuments.Any()
                    ? new List<WorkactivityOrderDocument>()
                    : sdk.Find<WorkactivityOrderDocument>($"FK_ORDERDOCUMENT IN ({string.Join(",", orderDocuments.Select(x => x.Id))})").ToList();

                foreach (var orderDocument in orderDocuments)
                {
                    var workactivityOrderDocumentsThisOrderDocument = allWorkactivityOrderDocuments.Where(x => x.FK_ORDERDOCUMENT == orderDocument.Id).ToList();

                    if (workactivityOrderDocumentsThisOrderDocument.Any() && (string.IsNullOrEmpty(tableName) || tableName.Equals("R_JOBORDERDETAILWORKACTIVITY")))
                    {
                        var joborderDetailsWorkactivity = sdk
                            .Find<JoborderDetailWorkactivity>($"FK_ORDER = {orderDocument.FK_ORDER} AND FK_WORKACTIVITY IN ({string.Join(",", workactivityOrderDocumentsThisOrderDocument.Select(y => y.FK_WORKACTIVITY))})").ToList();

                        result.AddRange(joborderDetailsWorkactivity.Select(x => new Document()
                        {
                            DocumentId = orderDocument.FK_DOCUMENT,
                            TableName = "R_JOBORDERDETAILWORKACTIVITY",
                            RecordId = x.Id,
                        }).ToList());
                    }

                    if (!workactivityOrderDocumentsThisOrderDocument.Any() && (string.IsNullOrEmpty(tableName) || tableName.Equals("R_ORDER")))
                    {
                        result.Add(new Document() { DocumentId = orderDocument.FK_DOCUMENT, TableName = "R_ORDER", RecordId = orderDocument.FK_ORDER });
                    }
                }

                if (string.IsNullOrEmpty(tableName) || tableName.Equals("R_JOBORDERDETAILWORKACTIVITY"))
                {
                    //Add deleted documents
                    var deletedDocuments = sdk.Find<DeletedDocument>(x => x.DATECREATED.Value >= dateChanged.Value).ToList();

                    foreach (var deletedDocument in deletedDocuments)
                    {
                        result.Add(new Document()
                        {
                            DocumentId = deletedDocument.DOCUMENTID.Value,
                            TableName = deletedDocument.TABLENAME,
                            RecordId = deletedDocument.RECORDID.Value,
                            DateChanged = deletedDocument.DATECREATED.Value,
                            IsDeleted = true,
                        });
                    }
                }
            }

            if (string.IsNullOrEmpty(tableName) || tableName.Equals("C_VEILIGHEIDSMELDINGEN"))
            {
                result.AddRange(sdk.Find<SafetyReportDocument>(x => iQDocumentIds.Contains(x.FK_DOCUMENT)).Select(x =>
                        new Document() { DocumentId = x.FK_DOCUMENT, TableName = "C_VEILIGHEIDSMELDINGEN", RecordId = x.FK_VEILIGHEIDSMELDINGEN }));
            }

            if (string.IsNullOrEmpty(tableName) || tableName.Equals("R_TODO"))
            {
                result.AddRange(sdk.Find<TodoDocument>(x => iQDocumentIds.Contains(x.FK_DOCUMENT)).Select(x =>
                         new Document() { DocumentId = x.FK_DOCUMENT, TableName = "R_TODO", RecordId = x.FK_TODO }));
            }

            if (string.IsNullOrEmpty(tableName) || tableName.Equals("C_VGINSPECTIE"))
            {
                result.AddRange(sdk.Find<WorkplaceInspectionDocument>(x => iQDocumentIds.Contains(x.FK_DOCUMENT)).Select(x =>
                        new Document() { DocumentId = x.FK_DOCUMENT, TableName = "C_VGINSPECTIE", RecordId = x.FK_VGINSPECTIE }));
            }

            return result;
        }



        private List<Document> GetDocumentsPaged(List<Document> documents, PagingOptions pagingOptions)
        {
            var batches = documents.Batch(pagingOptions.PageSize).ToArray();

            if (pagingOptions.Page > batches.Length - 1)
            {
                return new List<Document>();
            }

            return batches[pagingOptions.Page].OrderBy(x => x.DocumentId).ToList();
        }

        // POST: api/Documents/AddDocument
        [HttpPost("{company}/[controller]/AddDocument")]
        public ActionResult AddDocument(string company, string tableName, int recordId, IFormFile file)
        {
            if (string.IsNullOrEmpty(company))
            {
                return BadRequest("No company specified.");
            }

            var sdk = _sessionProvider.Get(company);
            if (sdk == null)
            {
                return BadRequest($"Company '{company}' not found.");
            }

            var validateResult =
                ValidateParameters(sdk, tableName, recordId, file);

            if (!validateResult.Result)
            {
                return BadRequest(validateResult.Message);
            }

            var fullLocationCreatedFile = ""; //Wordt gevuld door de methode CreateFileOnAppreoDocumentLocation
            var resultCreateFile = CreateFileOnArchiveLocation(sdk, file, tableName, recordId, ref fullLocationCreatedFile);

            if (!resultCreateFile.Result)
            {
                return BadRequest(resultCreateFile.Message);
            }

            var resultProcessToRidderiQ = ProcessDocumentToRidderiQ(sdk, tableName, recordId, fullLocationCreatedFile);

            if (!resultProcessToRidderiQ.Result)
            {
                return BadRequest(resultProcessToRidderiQ.Message);
            }

            return Ok("Document correct added to Ridder iQ");
        }

        private ValidateResult ProcessDocumentToRidderiQ(SdkSession sdk, string tableName, int recordId, string fullLocationCreatedFile)
        {
            if (tableName == "R_ORDER")
            {
                //Bij aanmaken document is document direct gearchiveerd in de ordermap
                return new ValidateResult() { Result = true, Message = "" };
            }
            else
            {
                var fileNameWithoutExtension = Path.GetFileNameWithoutExtension(fullLocationCreatedFile);

                //Veiligheidsmeldingen worden gedistribueerd over meerdere bedrijven. Daarom slaan we deze op in de database.
                //Bij andere tabellen gewoon als verwijzing opslaan
                var saveInDatabase = tableName.Equals("C_VEILIGHEIDSMELDINGEN");
                var saveAsLocation = !saveInDatabase;

                var result = sdk.Sdk.EventsAndActions.CRM.Actions.AddDocument(fullLocationCreatedFile, "0", saveAsLocation,
                    tableName, recordId, fileNameWithoutExtension);

                if (result.HasError)
                {
                    return new ValidateResult() { Result = false, Message = $"Adding document in Ridder iQ failed; cause: {result.GetResult()}." };
                }
            }

            return new ValidateResult() { Result = true, Message = "" };
        }

        private ValidateResult CreateFileOnArchiveLocation(SdkSession sdk, IFormFile file,
            string tableName, int recordId, ref string fullLocationCreatedFile)
        {
            var archiveInOrderDirectoryInfo = DetermineToArchiveInOrderDirectory(sdk, tableName, recordId);
            var targetDirectory = DetermineTargetDirectory(sdk, archiveInOrderDirectoryInfo);

            if (targetDirectory.StartsWith("ERROR_"))
            {
                return new ValidateResult() { Result = false, Message = targetDirectory };
            }

            if (!Directory.Exists(targetDirectory))
            {
                Directory.CreateDirectory(targetDirectory);
            }

            var extension = Path.GetExtension(file.FileName);
            var fileNameWithoutExtension = $"{archiveInOrderDirectoryInfo.TableRecordLabel}_{DateTime.Now:d-M-yyyy HH.mm.ss}";
            var existingFiles = Directory.GetFiles(targetDirectory,
                fileNameWithoutExtension + "*" + extension, SearchOption.TopDirectoryOnly)
                .Select(x => Path.GetFileName(x)).ToList();

            var uniqueFileName = !existingFiles.Any()
                ? $"{fileNameWithoutExtension}{extension}"
                : $"{fileNameWithoutExtension} ({existingFiles.Count + 1}){extension}";

            fullLocationCreatedFile = Path.Combine(targetDirectory, uniqueFileName);

            using (var stream = new FileStream(fullLocationCreatedFile, FileMode.Create))
            {
                file.CopyTo(stream);
            }

            return new ValidateResult() { Result = true, Message = "" };
        }

        private string DetermineTargetDirectory(SdkSession sdk, ArchiveInOrderDirectoryInfo archiveInOrderDirectoryInfo)
        {
            if(archiveInOrderDirectoryInfo.OrderId.HasValue)
            {
                var orderDirectory = DetermineLocationOrderDirectory(sdk, archiveInOrderDirectoryInfo.OrderId.Value);

                if (string.IsNullOrEmpty(orderDirectory))
                {
                    return $"ERROR_No order directory found for this order.";
                }

                return Path.Combine(orderDirectory, "Appreo app documenten", archiveInOrderDirectoryInfo.TableRecordLabel);
            }
            else
            {
                var generalLocationAppreoAppDocuments = sdk.GetRecordsetColumns("R_CRMSETTINGS",
                    "LOCATIONAPPREOAPPDOCUMENTS", "")
                .DataTable.AsEnumerable().First().Field<string>("LOCATIONAPPREOAPPDOCUMENTS");

                if (string.IsNullOrEmpty(generalLocationAppreoAppDocuments))
                {
                    return $"ERROR_No location Appreo app documents found in the Ridder CRM-settings.";

                }

                return Path.Combine(generalLocationAppreoAppDocuments, archiveInOrderDirectoryInfo.TableRecordLabel);                
            }
        }

        private ArchiveInOrderDirectoryInfo DetermineToArchiveInOrderDirectory(SdkSession sdk, string tableName, int recordId)
        {
            if (tableName.Equals("R_ORDER"))
            {
                return new ArchiveInOrderDirectoryInfo()
                {
                    OrderId = recordId,
                    TableRecordLabel = "Order documenten",
                };
            }

            if(tableName.Equals("R_PURCHASEINVOICE"))
            {
                return new ArchiveInOrderDirectoryInfo()
                {
                    OrderId = sdk.Find<PurchaseInvoiceDetailMisc>(x => x.FK_PURCHASEINVOICE == recordId).FirstOrDefault()?.FK_ORDER,
                    TableRecordLabel = "Declaraties",
                };
            }

            var tableInfo = sdk.GetRecordsetColumns("M_TABLEINFO", "PK_M_TABLEINFO, DESCRIPTION", $"TABLENAME = '{tableName}'", "")
                .DataTable.AsEnumerable().FirstOrDefault();

            if (tableInfo == null)
            {
                return new ArchiveInOrderDirectoryInfo()
                {
                    OrderId = null,
                    TableRecordLabel = tableInfo.Field<string>("DESCRIPTION"),
                };
            }

            var tableHasFkOrder = sdk.GetRecordsetColumns("M_COLUMNINFO", "PK_M_COLUMNINFO",
                $"FK_TABLEINFO = '{tableInfo.Field<Guid>("PK_M_TABLEINFO")}' AND COLUMNNAME = 'FK_ORDER'").RecordCount > 0;

            if(!tableHasFkOrder)
            {
                return new ArchiveInOrderDirectoryInfo()
                {
                    OrderId = null,
                    TableRecordLabel = tableInfo.Field<string>("DESCRIPTION"),
                };
            }

            var orderId = sdk.GetRecordsetColumns(tableName, "FK_ORDER", $"PK_{tableName} = {recordId}")
                .DataTable.AsEnumerable().First().Field<int?>("FK_ORDER");

            return new ArchiveInOrderDirectoryInfo()
            {
                OrderId = orderId,
                TableRecordLabel = tableInfo.Field<string>("DESCRIPTION"),
            };
        }

        private ValidateResult ValidateParameters(SdkSession sdk, string tableName, int recordId, IFormFile file)
        {
            if (string.IsNullOrEmpty(tableName))
            {
                return new ValidateResult() { Result = false, Message = "No table name specified." };
            }

            if (file == null)
            {
                return new ValidateResult() { Result = false, Message = "No file specified." };
            }

            if (!CheckTableName(sdk, tableName))
            {
                return new ValidateResult() { Result = false, Message = $"Table name '{tableName}' does not exixts in Ridder iQ." };
            }

            if (!CheckTableHasDocumentManagement(sdk, tableName))
            {
                return new ValidateResult() { Result = false, Message = $"At table '{tableName}' is it not possible to add documents." };
            }

            if (!CheckRecordId(sdk, tableName, recordId))
            {
                return new ValidateResult() { Result = false, Message = $"Record id {recordId} not found in table '{tableName}'." };
            }

            return new ValidateResult() { Result = true, Message = "" };
        }

        private bool CheckTableHasDocumentManagement(SdkSession sdk, string tableName)
        {
            return sdk.GetRecordsetColumns("M_TABLEINFO", "PK_M_TABLEINFO",
                $"TABLENAME = '{tableName}DOCUMENT'").RecordCount > 0;
        }

        private bool CheckRecordId(SdkSession sdk, string tableName, int recordId)
        {
            return sdk.GetRecordsetColumns(tableName, $"PK_{tableName}",
                $"PK_{tableName} = {recordId}").RecordCount > 0;
        }

        private bool CheckTableName(SdkSession sdk, string tableName)
        {
            return sdk.GetRecordsetColumns("M_TABLEINFO", "PK_M_TABLEINFO",
                $"TABLENAME = '{tableName}'").RecordCount > 0;
        }

        private string DetermineLocationOrderDirectory(SdkSession sdk, int orderId)
        {
            var order = sdk.Find<Order>(x => x.Id == orderId).FirstOrDefault();

            if (order == null)
            {
                return string.Empty;
            }

            var locationOrderDirectories = sdk.GetRecordsetColumns("R_CRMSETTINGS",
                    "ORDERLOCATION", "").DataTable.AsEnumerable()
                .First().Field<string>("ORDERLOCATION");

            if (string.IsNullOrEmpty(locationOrderDirectories))
            {
                return string.Empty;
            }

            return Path.Combine(locationOrderDirectories, order.ORDERNUMBER.ToString());
        }

        class ArchiveInOrderDirectoryInfo
        {
            public string TableRecordLabel { get; set; }
            public int? OrderId { get; set; }
        }
    }
}
